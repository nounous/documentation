# Contribution

Merci de contribuer à la documentation ! Même si le déploiement/la
maintenance des différents services du Crans peut paraître plus intéressante
que faire de la documentation, il s'agit au même titre d'une étape cruciale du
bon fonctionnement du Crans : on ne peut pas faire fonctionner tout un parc
informatique si on ne sait pas ce qu'il y a dedans.

## Contribuer

Toute contribution est la bienvenue ! Surtout n'hésitez pas à demander de
l'aide aux vieilles nounous si vous n'êtes pas certain⋅e de ce que vous
faites.

On distingue ici deux types de contributions :

* les contributions directes, c'est-à-dire un ajout d'une documentation sur
  un ou plusieurs sujet, ou une modification d'une documentation déjà existante
  pour la corriger, la mettre à jour, ...

* les demandes de contribution, c'est-à-dire les demandes d'ajout de
  documentation sur un sujet que vous trouvez mal expliqué et que vous ne
  comprenez pas bien (sinon vous pourriez le faire vous-même ^^), ou les
  demandes d'ajout de tutoriels dans le dossier [`howto`](howto).

Pour les demandes de contribution, vous pouvez directement demander à une
nounou, mais pour ne pas oublier il vaut mieux dans tous les cas ouvrir une
issue sur le dépôt gitlab <https://gitlab.crans.org/nounous/documentation>.
Une nounou ou un⋅e apprenti⋅e se chargera alors (lorsqu'iel aura le temps)
d'écrire cela.

Pour les contributions directes, il n'y a aucun problème à directement mettre
vos modifications sur ce dépôt, à partir du moment où les
modifications/ajouts respectent l'organisation décrite dans [la section
Organisation du README.md](README.md#organisation) ainsi que les conventions de
rédactions décrites ci-dessous. Si vous avez effectué ou prévoyez beaucoup
de modifications, il peut alors être préférable de placer tous les
changements sur une autre branche puis de faire une merge request sur gitlab
pour qu'une autre nounou vérifie vos changements. De plus, si besoin, vous
pouvez vous aider des fichiers `TEMPLATE.md` pour avoir une idée de comment
structurer votre documentation.

## Conventions de rédaction

### Structuration

Tous les fichiers doivent être écrits en
[markdown](https://fr.wikipedia.org/wiki/Markdown). Cela permet de lire toute
la documentation dans toutes circonstances avec des mises en forme
standardisées. Vous pouvez trouver de la documentation markdown sur [ce
site](https://www.markdownguide.org/) si nécessaire. Un linter est configuré
dans ce dépôt : [markdownlint](https://github.com/markdownlint/markdownlint).
Aucun fichier ne doit renvoyer d'avertissement après exécution de la commande
`mdl .` à la racine du dépôt.

**Tout dossier doit contenir un fichier `README.md`**, même si celui-ci est
court. Il doit contenir le rôle de ce que contient le dossier avec une brêve
description de tous les fichiers et sous-dossiers présents.

Tout dossier peut également contenir en plus, si nécessaire et approprié, un
fichier `TEMPLATE.md` mettant à disposition un template pour les futures
documentations.

De plus, mis à part les fichiers `README.md` et `TEMPLATE.md`, et les fichiers
uniques [`CONTRIBUTING.md`](CONTRIBUTING.md), [`CRITICAL.md`](CRITICAL.md) et
[`LICENSE.md`](LICENSE.md), tous les noms de fichiers doivent être **clairs**
et en minuscules. Il vaut mieux un nom long qu'une abrévation peu
compréhensible. Dans le cas d'un nom de fichiers de plusieurs mots, on les
séparera par des tirets bas (underscores).  Par ailleurs, on évite autant que
possible de créer des dossiers contenant 50 fichiers : on préfère avoir des
sous-dossiers thématiques plus navigables.

### Mise en page

Il n'y a jamais assez d'hyperliens : n'hésitez pas à mettre des références
vers de la documentation officielle ou à rediriger vers d'autres fichiers de
la documentation du Crans si cela est possible ! Cela permet de naviguer
facilement sans se perdre dans les sous-dossiers.

De plus, pour faciliter la lisibilité, que ce soit dans le terminal ou sur
internet, on limite la taille des lignes à 80 caractères en comptant les
caractères non affichés sur des rendus graphiques (comme les hyperliens ou
les étoiles pour les mots en gras/italique/...). Si besoin, vous pouvez
facilement configurer un formatter dans votre éditeur de texte (vim, helix,
...) avec simplement la commande `fold -s -w80`.

### Langue

* Il serait préférable que toute la documentation soit (au minimum) être
  écrite en français. Cela ne signifie pas qu'il ne peut pas y avoir de
  traduction en anglais ou de termes informatiques anglais, mais la langue de
  rédaction devrait rester le français pour être compris par tous et toutes
  sans difficulté.

* On privilégie le vouvoiement au tutoiement.

* L'usage de l'écriture inclusive est recommandé, que ce soit par
  l'utilisation prioritaire de termes épicènes, par la coordination (ex: "tous
  et toutes") ou par l'emploi du point médian.
