# Comment devenir nounou ?

Bonjour jeune apprenti⋅e, tu regardes sûrement avec des yeux pleins d'étoiles
tes ainé⋅es qui actuellement disposent des droits suprêmes sur l'infrastructure
du Crans. Ce petit document est là pour t'expliquer comment toi aussi un jour tu
pourras devenir l'un⋅e d'elleux. Et quand il sera temps et que tu commenceras à
t'encrouter, comment tu pourras rendre tes droits.

Déjà commençons par une question un peu conne mais néanmoins légitime « Qui
"mérite" d'être nounou ? » Premièrement, devenir nounou **N'EST PAS** une
question de compétence. Pas besoin de connaître l'infrastructure par cœur, tous
les fichiers de conf, être un pro d'apt, d'ansible et de la couche 2. Tout ce
qu'on attend d'une nouvelle nounou c'est d'être capable de ne pas faire
n'importe quoi avec ces droits. Devenir nounou ce n'est pas avoir fini son
apprentissage mais bien la deuxième étape de celui-ci où on commence à mettre
les mains dans le camboui. Et ça ça présuppose seulement qu'on va pas causer du
tort en utilisant ces droits.

Détaillons-les un peu ces droits, ce qu'il permette de faire et qu'est ce qu'on
entend par « causer du tort ». En devenant nounou, vous débloquez les accès à
toute l'infrastructure. Techniquement, il n'y a plus de fichiers que vous ne
pouvez pas voir, modifier ou supprimer et ce sur toutes les machines du Crans.
De même, on chiffre pour vous tous les mots de passe qui ont trait au technique
du Crans. Ce sont des droits très extensifs. D'où la possibilité de faire une
erreur de manipulation. Normalement tout est fait dans l'infrastructure pour
limiter la casse possible. On a de la redondance où c'est possible et
nécessaire, on fait des backups quotidiennes à la fois des données utilisateur⋅rices
mais aussi des données d'administration. Cependant, tout ça ne change pas le
fait qu'une mauvaise commande rentrée dans votre shell peut rendre le
Crans hors ligne pour quelques heures ou supprimer de manière irreversible des
données. Quand vous devenez nounou, on vous fait confiance pour éviter ce genre
de choses au maximum. Après les erreurs ça arrive toujours. La personne qui
écrit ces lignes en a fait un certain nombre avec des conséquences diverses.
Ce qui compte ce n'est pas de ne pas en faire du tout, c'est d'en faire peu,
d'apprendre de celles-ci et de s'assurer qu'elles ne sont pas irréversibles.

Il y a aussi quelque chose qu'il faut souligner. Et normalement quand vous
deviendrez nounou vous allez le voir assez fréquement (à chaque fois que vous
faîtes un sudo pour la première fois sur une machine) :

```txt
We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.
```

Ce court texte résume bien ce que je veux aborder ici. Les droits que vous vous
voyez confier sont une intrusion phénoménale dans la vie privée des gens. Si
vous voulez une métaphore, vous pouvez voir ça comme si les adhérent⋅es du Crans
vous avait donné la clé de leur maison pour que vous puissiez réparer quand il y
a une fuite d'eau, mais pas pour que vous fouillez dans le bureau pour voir leurs
papiers. Beaucoup de nos adhérent⋅es utilisent les outils qu'on met à leur
disposition pour organiser une partie de leur vie, que ce soit d'utiliser
l'adresse mail qui leur est fournie ou stocker des données dans leur Owncloud,
et nous en sommes très heureux⋅ses. La charte que vous avez signé en devenant
apprenti⋅e mentionne déjà ces problématiques mais il est important que ce soit
très clair. **Vous ne pouvez consulter ou diffuser les données personnelles
d'un⋅e adhérent⋅e qu'avec son consentement explicite.** On a parfois tendance à
l'ENS à faire des abus de droits une blague, ce n'est pas le cas au Crans !

Bon maintenant que ces clarifications et rappels ont été faits, on va pour
passer aux choses plus intéréssantes : comment on se donne ces droits (et dans la
marge, comment on se les retire) ?

## Rentre dans le cercle

Le groupe des superadministrateur⋅rices au Crans est le groupe nounou. On va donc
faire un petit tour dans le ldap et sous ou=groups,cn=_nounou on rajoute les
uids correspondant au⋅x personne⋅s qu'on souhaite ajouter. Voilà, vous avez
normalement:tm: les accès root sur toutes les machines du Crans. Cependant,
vous allez peut-être constater qu'il y a certaines machines où cela ne
fonctionne pas. C'est sûrement dû à ces ~~conneries~~ de replicat ldap. Il faut
à ce moment là aller faire un tour dessus pour forcer la resynchro (c.f.
`tools/ldap.md`).

## Give me the password

Vous avez *50* nouveaux mot de passes. Il est maintenant nécessaire de
rechiffrer le pass pour vous. C'est un peu chiant mais on s'y fait. Voilà la
procédure:

  1. On se souvient du hash de commit courant :
  `HASH=$(git rev-parse HEAD)`
  1. On ajoute toutes les personnes concernées dans le groupe nounou dans le
   fichier `.groups.yml` du store et on commit ces changements `git commit -m
  toto1234`.
  1. On rechiffre tous les fichiers à rechiffrer
  `cat .last_group.json | jq -r '. | with_entries( select(.value | any( .==
  "nounou"))) | keys[]' | xargs pass rans reencrypt`
  1. On supprime tous les commits inutiles :
  `git reset --soft $HASH; git commit -m 'Coucou les ami⋅es'`
  1. On vérifie que ça marche et on push :)

Petit point pour quand on retire des droits au gens. Là où pour tous les autres
droits, quand vous les retirez à quelqu'un⋅e bah iel les a plus, ce n'est pas le
cas pour les mots de passe. Il reste dans l'historique de git un moment où la
personne qui perds ces droits à toujours les mots de passes chiffrées pour elle.
On choisit généralement de s'en tamponner l'oreille avec une babouche.
Cependant, il peut y avoir certains moments où on veut effectivement retirer
complètement à quelqu'un⋅e la possibilité d'utiliser un mot de passe. Dans ce
cas il faut le changer :(

## Thank you for your services

Au Crans on a **plein** de services. Certains sont gentils et écoutent
directement le ldap pour savoir qui a des droits, d'autres s'en foutent et ils
faut se rajouter dans les admins à la main. Petite liste ci dessous.

### Gitlab

Il faut aller dans la zone d'administration, puis dans la liste
d'utilisateur⋅ices et changez le profil de l'utilisateur⋅ice de regular à admin.

### Mailman

Il faut se connecter normalement au logiciel, puis allez dans la section admin
<https://lists.crans.org/admin>, dans la sous page utilisateur⋅rices, on peut
rechercher les personnes concernées pour leur donner les deux statuts admin
(accès à la zone d'administration) et superutilisateur (bypass tous les checks
de droits).

### Owncloud

Il faut aller dans la zone d'administration et rajouter les utilisateur⋅ices au
groupe d'administration.

### Re2o

Oh lad, j'espère que ça aussi ça existe plus. Il faut aller dans la section
gestion des groupes et rajouter les personnes aux groupes nounous et au groupe
superutilisateur⋅rice.

### Kiwi

On édite le fichier de conf du wiki sous `/etc/moin/mywiki.py` et on rajoute son
compte wiki dans la liste des administrateur⋅rices.

### IRC

On édite le fichier `/etc/inspircd/opers.conf` pour rajouter un bloc suivant

```txt
<oper
    name=<nick>
    password="<pass-hash>"
    hash="sha256"
    host="*"
    type="Deity"
    fingerprint="<fingerprint certificat>"
    autologin="on"
>
```

Où le hash du mot de passe peut être obtenu en envoyant
`/quote mkpasswd hmac-sha256 <password>`. Pour rendre les changements effectifs
on reload le service inspircd (tips, la manière la plus rapide de perdre ses
droits est de restart le serveur plutôt que de le reload ^^).

### Mails

Il faut maintenant vous rajouter en alias de root@crans.org. C'est le début du
spam. Pour ce faire, rendez-vous sur le dépôt Gitlab `mail-aliases`, et ajoutez
votre pseudo Crans sur la bonne ligne dans les bons fichiers, en face de `root:`.
Pensez ensuite à mettre à jour le dépôt pour le service `mail`, et enfin mettez
bien à jour `redisdead`, `sputnik` et surtout `zamok` dans le dossier
`/var/local/services/mail`. Enfin, pensez bien à filtrer les mails arrivant à
l'adresse `root@crans.org` sur votre client.

Voilà, tu as maintenant gagné le droit de t'ajouter ou de te retirer de
<https://wiki.crans.org/CransNounous> !
