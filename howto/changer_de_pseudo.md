# Changement de pseudo

Le pseudo est l'identifiant unique permettant de s'authentifier sur l'ensemble
des services du Crans. Pour diverses raisons dont il n'est pas question de
juger, un⋅e utilisateur⋅rice peut vouloir changer de pseudo. C'est pénible
mais faisable : respecter la volonté de l'utilisateur⋅rice dans ce cas est
important. Ce petit guide est là pour rappeler tout ce qu'il ne faut pas
oublier.

## Re2o

Il faut bien un début, c'est sur l'intranet qu'on commence par renommer
quelqu'un⋅e.  Il suffit d'éditer le profil et de changer le pseudo.

Ne pas oublier d'aller contrôler le LDAP sur `yson-partou` pour s'assurer que
l'ancien compte a bien été supprimé. Pour cela, faire un `sudo shelldap` sur
`yson-partou`, puis taper `cn=Utilisateurs` et s'assurer que
`cat cn=ancienpseudo` ne renvoie rien.

## Zamok

Le plus évident, il faut déplacer le home ainsi que les mails.

Si le pseudo vient à l'instant d'être modifié, il suffit de faire un
`sudo mv /home/{ancienpseudo,nouveaupseudo}`, sinon avec l'accord de la
personne on peut faire un `rsync -ar /home/{ancienpseudo,nouveaupseudo}/`, ce
qui va copier le contenu de l'ancien home dans le nouveau, mais attention au
remplacement de données. L'ancien home peut ensuite être supprimé.

De la même manière, il faut renommer `/var/mail/ancienpseudo`
en`/var/mail/nouveaupseudo`, ou déplacer le contenu et supprimer l'ancien
dossier.

## Backups

Étape non pressante, mais on peut se rendre sur `backup-ft` et `backup-thot`
pour déplacer `/backup/borg-adh/home/ancienpseudo` en
`/backup/borg-adh/home/nouveaupseudo`, si ce n'est pas trop tard. Si le nouveau
dossier est déjà créé, alors on peut attendre quelques jours voire quelques
mois au cas où pour supprimer l'ancien dossier inutile.

À faire avec précaution. Mais à faire puisqu'on ne garde pas des backups
éternellement. Pas pour des raisons de place mais pour des raisons de
non-conservation de données personnelles.

## Gitlab

S'il n'y a pas d'ancien compte, il n'y a rien à faire.

S'il y a un ancien compte ET un nouveau compte, c'est pénible. Il faut
transférer les appartenances de projet d'un compte à l'autre. Ce cas ne
s'étant pour l'instant pas présenté, il sera à détailler à terme.

On se place dans l'hypothèse où l'on veut renommer un ancien compte. On
commence par se rendre dans l'interface d'administration de Gitlab, section
utilisateurs :
[https://gitlab.crans.org/admin/users](https://gitlab.crans.org/admin/users).

Si la personne s'est reconnectée avec son nouveau pseudo SANS AUCUNE
CONTRIBUTION, alors on peut supprimer librement ce nouveau compte. On se rend
ensuite dans les paramètres de l'ancien compte, on clique sur « Éditer » et
on remplace le  pseudo. Inutile de remplacer nom et adresse mail, ces champs
sont importés par LDAP.

Ensuite, on se rend dans l'onglet « Identités », et on met à jour
l'identifiant LDAP. Normalement, la personne peut désormais se connecter en
n'ayant perdu ni projets ni commits, et son adresse mail ainsi que son nom
seront mis à jour à sa prochaine connexion.

## Owncloud

Deux cas à considérer : l'utilisateur⋅rice s'est servi⋅e des agendas
Owncloud ou non.  Dans le premier cas, c'est pénible et cette documentation
sera mise à jour afin de détailler comment exporter et réimporter les
calendriers.

Si l'utilisateurice n'a pas utilisé d'application externe et uniquement le
téléversement de fichiers, il suffit simplement de se rendre dans l'interface
d'administration, section utilisateur⋅rices :
[https://owncloud.crans.org/settings/users](https://owncloud.crans.org/settings/users),
puis de chercher l'ancien compte et de simplement le supprimer. Les données
étant montées sur Zamok, seul le home local inutile sera supprimé.

## Mailman

On commence par se rendre dans [Django-Admin](https://lists.crans.org/admin/).

S'il n'y a pas d'ancien compte, aucune question à se poser.

S'il y a un nouveau compte inutile, on le supprime.

S'il n'y a pas de nouveau compte, on peut changer le pseudo Mailman si
souhaité, mais l'important est surtout d'aller dans les connexions sociales et
de changer le pseudo utilisé pour lier le compte Mailman.

## Wiki

Si la connexion par le CAS est configurée, il faut penser à
`mv /var/local/wiki/assowiki/{ancienpseudo,nouveaupseudo}`. Concerne a priori
peu de monde.

## The Lounge

Si on tient à ne pas perdre sa connexion WebIRC (si concernée), on peut aller
sur Zamok et `mv /etc/thelounge/users/{ancienpseudo,nouveaupseudo}.json`. Il y
a aussi des fichiers de d'historiques des messages que l'on peut supprimer
`rm /etc/thelounge/logs/{pseudo}` et `rm /etc/thelounge/logs/{pseudo}.sqlite3`
ou remommer.
