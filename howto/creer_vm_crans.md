# Guide pour la création de VM interne au Crans

## Étape préliminaire

Savoir ce que l'on veut et regarder les besoins du service que l'on souhaite
installer

## Création de la VM

### Se connecter à Proxmox

Proxmox est un logiciel de virtualisation utilisé au Crans, plus d'info ici
[Proxmox](/outils/os/proxmox.md)

Pour se connecter au Proxmox :

```bash
ssh -NL 8006:localhost:8006 sam.adm.crans.org
```

Puis connectez-vous `https://localhost:8006/` dans votre navigateur open source
préféré (Et connectez-vous avec vos identifiants nounous)

Trouvez un identifiant de VM qui n'ai pas déjà pris.

### Se connecter au bon LDAP

Se connecter au LDAP à l'aide de shelldap par exemple :

- `wall-e` pour `adm`, (serveur de service)
- `flirt` pour `adh`   (service de service)

`shelldap -f shelldap_wall-e.rc`

Avec le fichier `shelldap_wall-e.rc` (évidemment modifiez `_ton_pseudo` avec
votre pseudo nounou) :

```yaml
# Fichier de configuration shelldap admin
# de bleizi/shirenn adapté
server: ldaps://172.16.10.100:636
binddn: uid=_ton_pseudo,ou=passwd,dc=crans,dc=org
basedn: dc=crans,dc=org
promptpass: yes
tls: yes
tls_cacert: /etc/ldap/ldap.pem
```

"Bind password :" -> Entrez votre mot de passe nounous

### Créer l'entrée dans LDAP

#### Base de navigation dans le LDAP

Les éléments commencent par ```d``` sont les dossiers, on peut donc "entrer
dans ces dossiers".

Ici le dossier qui nous intéresse est le dossier ```ou=hosts``` qui contient
les configurations des différentes VM.

Ce sont dans "ces fichiers" de configuration qu'est décrit les la VM.
On peut utiliser ```cat``` pour regarder leurs contenues, ```mv``` pour changer
leurs noms ou les déplacer. Pour les éditer la commande est ```edit```.
Voir : [doc ldap](/infrastructure/services/ldap.md) pour en savoir plus à
propos du LDAP et de shelldap.

#### Configuration réseau

Convention du Crans (plus ou moins respecté), si votre id est XYZ (avec X, Y, Z
des chiffres):

- L'adresse IPv4 est alors : `172.16.[vlan].XYZ`
- L'adresse IPv6 est alors : `fd00::10:0:ff:fe0X:YZ[vlan]`
- L'adresse MAC est alors : `02:00:00:0X:YZ:[vlan]`

> Recommendation : pour que ce soit plus simple vous pouvez vous inspirer de la
> configuration d'une VM pré-existante.

### Création de la VM sur Proxmox

> :bulb: Pour créer une VM sous nixos référez vous au repos
> [nixos](https://gitlab.crans.org/nounous/nixos) du Crans

Créer une VM dans Proxmox avec l'ISO Debian.
Mettez l'id que vous avez sélectionné précédemment.
Et le nom que vous souhaitez donner à la machine (un nom avec une ref obscure
ou un nom qui reprends le nom du service qui sera hébergé afin de facilement
savoir à quoi sert la VM)

> - Ne pas modifier d'autre option si inconnue
> - Bien spécifier dans l'option Disks où sera stocké la VM : serveur
>   adhérent avec 10TB
> - Entrer la bonne adresse MAC dans le networking
> - Préciser dans Network les réseaux qui vont être utilisés par la VM.
>   Pour cela, regardez cette [documentation](/infrastructure/reseaux/plan.md)
> - Ajouter dans la section "hardware" le "Serial Port" avec le numéro 0

## Installation de Debian

### Config réseaux

Configurez l'interface réseau de la VM (pas de DHCP):

l'IP du serveur lui-même est à entrer manuellement (pas d'autoconf)

```config
Gateway : ipv4 : 172.16.10.101 (bizarre, fournit par routeur sam)

romanesco : ipv4 : 172.16.10.128 (DNS récursif)

hostname : le_nom_de_la machine
```

Pour plus d'information, regardez cette
[documentation](/infrastructure/reseaux/plan.md).
En cas de problème, il est possible de le configurer après installation dans
`/etc/network/interfaces`.

Nom de domaine pour les machines adm : `adm.crans.org`

```txt
FQDN : `[nom du service].adm.crans.org`
```

> - Pour le gestionnaire de paquet, il est possible d'utiliser le mirroir du
>   crans `http://mirror.adm.crans.org/debian/`. En cas de problème, il est
>   possible de le configurer après installation dans `/etc/apt/sources.list`.

__Installer openssh__ si pas fait dans l'installateur

Copiez votre clé publique ssh sur la VM fraichement créer
via la terminal disponible sur Proxmox (`~/.ssh/authorized_keys`)

## Setup initial de la VM

Pour faciliter le déploiement des VM, le Crans opte pour l'utilisation
d'Ansible. Cette partie ne fait qu'effleurer le fonctionnement d'Ansible dans
l'objectif de terminer l'installation. Pour aller plus loin, vous pouvez
regarder cette [documentation](/outils/logiciels/ansible.md).

### SSH

Avant toutes choses, vérifiez que vous avez bien configuré ssh dans
`~/.ssh/config`. Pour cela, faites un tour sur la documentation
d'[Ansible du crans](https://gitlab.crans.org/nounous/ansible) section
`Configurer la connexion au vlan adm`.

La VM étant nouvellement configurer, il n'est possible de se ssh qu'en tant
que root. Ainsi, il est nécessaire de copiez votre clé SSH (public) sur le
compte root : `ssh-copy-id root@[nom_serveur].adm.crans.org`

> - À noter : il faut autoriser les connexions ssh en root et interdisez cela
dès que cela est fait.

### Installation d'Ansible

Installez Ansible sur votre machine ainsi que `python3-ldap`. En effet,
Ansible permet la gestion de toutes les VM depuis son ordinateur.

Téléchargez le [dépôt git](https://gitlab.crans.org/nounous/ansible) avec les
fichiers Ansible du Crans qui contiennent la configuration de tout les
serveurs du Crans.

Il faut alors ajouter la VM nouvellement créer dans Ansible.

### Configuration minimale

Cela correspond à la base de toutes les VM du crans. Cela inclut de permettre
la connexion ssh par les nounous notamment.

Pour cela, il faut ajouter la VM dans `hosts` dans la catégorie correspondante
(généralement dans `[crans_vm]`).

Ensuite, ajoutez un fichier `host_vars/[serveur].adm.crans.org` contenant la
configuration réseau (`interfaces`).

Enfin, il faut rendre le LDAP accessible à votre machine :

```bash
ssh -L 1636:wall-e.adm.crans.org:636 wall-e.adm.crans.org
```

Vérifiez que votre config n'a pas de problème.

```bash
plays/root.yml -l [machine a déployer].adm.crans.org --extra-vars='ansible_become_user=root' -u root --check
```

Une fois que le check n'a plus d'erreur, vous pouvez lancer la même commande
sans le `--check`.
Plus d'info sur Ansible dans son
[guide dédier](https://gitlab.crans.org/nounous/ansible).

Il se peut qu'une erreur persiste. En effet, avec `--check` aucune
modification n'est appliquée. Donc, si une commande nécessite au préalable
qu'une autre ait eu lieu, une erreur peut se produire.

> :bulb: Il sera nécessaire d'entrer votre mot de passe lors de l'apparition
> de `BECOME PASSWORD`.

### Configurer un service

Tout d'abord, ajoutez le serveur dans `group_vars/reverseproxy.yml` dans la
partie `reverseproxy_sites`.

> - `reverseproxy.yml` contient le nom avec lequel le service sera accessible.

Dans `all.yml`, ajoutez un playbook (au nom du service pour l'identifier plus
facilement) et créez un fichier du même nom dans `plays` (inspirez-vous d'un
autre fichier librement car la nomenclature sera similaire).

Le fichier dans `plays` contient 4 éléments essentiels :
> - l'en-tête `#!/usr/bin/env ansible-playbook`
> - une petite note explicative (parfois un œuf de Pâques...)
> - `hosts:` qui correspond à la VM ou groupe de VM pour lequel le script doit
>   s'appliquer (la VM doit être déclarer dans le fichier `hosts`).
>   Généralement, il s'agira de `[nom_vm].adm.crans.org`.
> - `roles:` qui contiendra la (ou les) configuration des services qui seront
>   hébergés sur la VM. Généralement, il s'agira simplement du nom du service
>   qui sera hébergé.

Une fois les rôles créés, il faut créer un dossier du même nom dans le dossier
`roles` (pour chaque rôle précédemment ajouté). Chacun de ces dossiers
contient 2 autres dossiers :

> - `templates` qui contiendra tous les fichiers qui devront être copier sur
>   la machine. Typiquement des fichiers de configuration comme pour `nginx`
>   ou le `motd`. À noter que les `templates` peuvent contenir des variables
>   sous la forme `{{ nom_de_la_variable }}`, qui sera remplacée au moment de
>   l'exécution d'Ansible. Ces noms de variables sont généralement enregistrés
>   dans `group_vars/[nom_service].yml`. Si la variable est un secret, il faut
>   alors l'ajouter dans le pass et écrire `{{ vault.fichier.secret }}`. Les
>   `templates` sont facultatifs si jamais utilisés.
> - `tasks` dont le fichier `main.yml` sera automatiquement exécuté. Il
>   contient toutes les commandes à exécuter pour rendre le service
>   opérationnel. De même que pour `templates`, il est possible d'utiliser des
>   variables.

:bulb: Pour utiliser une variable de `group_vars/[nom_service].yml`, il faut
au préalable ajouter dans `hosts` la catégorie `[nom_service]` (entre
crochets) et le serveur juste en dessous.

Une fois la configuration terminée, ajoutez le playbook dans `all.yml`, pensez
à push les modifications sur le git Ansible et appliquez les modifications sur
le serveur.
