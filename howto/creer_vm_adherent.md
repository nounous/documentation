# Création de machine virtuelle pour les adhérent⋅es

Le crans propose un système de locations de vms dont vous pouvez retrouver les
tarifs dans les annexes des statuts de l'association. En voici un
récapitulatif ici, mais attention celui-ci peut ne pas être à jour et seul
ce qui est marqué dans les statuts fait foi.

|      Article                            | Tarifs |
|:---------------------------------------:|:------:|
| Service minimal                         | 10€    |
| Connexion internet                      | 30€    |
| Cœur additionnel                        | 5€     |
| 2GB de RAM additionnel                  | 5€     |
| Disque additionnel (par tranche de 16G) | 1€     |

Le service minimal comprend :

* un disque de 8G
* un cœur
* 2G de ram

Tous les services sont proposés pour une durée de 1 an.

## Utilisateur⋅ices

La documentation pour les utilisateur⋅ices se trouvent sur le
[wiki](https://wiki.crans.org/VieCrans/VPS).

## Nounous et apprenti⋅es

### Facturation

Quand cela est possible, privilégier la note. Sinon, voir avec un⋅e
trésorier⋅ère pour d'autres méthodes (espèces, virement, ...).

### LDAP

Quand un⋅e adhérent⋅e vous demande de lui créer une vm, il faut au
préalable læ rajouter dans le ldap des adhérent⋅es (qui n'est pas le même
que le ldap  re2o).

Celui ci se situe sur flirt et à la structure suivante:

```txt
ou=users
├── cn=toto
└── cn=titi
ou=clubs
└── o=rover
ou=hosts
├── cn=voyager
└── cn=curiosity
```

Pour référence ma configuration `shelldap` pour me connecter au ldap est la
suivante :

```txt
# +---------------------------------+
# | Connexion à flirt.adm.crans.org |
# +---------------------------------+
server: ldaps://flirt.adm.crans.org/
promptpass: yes
binddn: cn=admin,dc=adh,dc=crans,dc=org
basedn: dc=adh,dc=crans,dc=org
```

et le mot de passe est disponible dans le password-store sous `ldap-adh`.

#### Utilisateur⋅ices

Les objets utilisateur⋅ices dispose des champs suivants :

```txt
dn: cn=toto,ou=users,dc=adh,dc=crans,dc=org
objectClass: inetOrgPerson
objectClass: organizationalPerson
objectClass: person
objectClass: top
cn: toto
description: birthDate:1998-01-08
description: birthLocation:Cachan
description: membershipStart:20xx-xx-xx
description: membershipEnd:20xx-xx-xx
description: re2oId:xxxxx
givenName: Toto
mail: nobody [AT] crans [DOT] org
postalAddress: 60 rue camille desmoulins, 94230, Cachan
sn: Passoir
telephoneNumber: +336xxxxxxxx
userPassword: {CRYPT}$6$…
```

Le script `addadherent` du repo
[crans-ldap](https://gitlab.crans.org/nounous/crans-ldap) permet néanmoins de
configurer la plupart des informations à renseigner :

```bash
[ _shirenn@flirt ] $ ./addadherent --re2o-id xxxxx toto
Prénom: Toto
NOM: Passoir
Adresse email: nobody [AT] crans [DOT] org
Adresse: 60 avenue camille desmoulins, 94230, Cachan
Numéro de téléphone: +33600000000
Date de naissance (YYYY-MM-DD): 1998-01-08
Lieu de naissance: Cachan
```

Cela rajoutera automatiquement la date de début d'adhésion mais pas celle de
fin d'adhésion, celle-ci doit être rajoutée à la main en utilisant votre
client ldap favori.

#### Club

Les clubs sont des objets ldap très simple qui ne contiennent que le nom des
adhérent⋅es qui gèrent le club :

```txt
dn: o=rover,ou=clubs,dc=adh,dc=crans,dc=org
objectClass: organization
objectClass: top
description: toto
o: rover
```

Similairement, un script permet de rajouter ces informations automatiquement au
ldap :

```txt
[ _shirenn@flirt ]$ ./addclub rover
Pseudo du responsable: toto
Pseudo du responsable: titi
Pseudo du responsable:
```

#### Machines

Les machines sont des objets ldap qui répertorient des informations sur leur
configuration et leurs propriétaires :

```txt
dn: cn=curiosity,ou=hosts,dc=adh,dc=crans,dc=org
objectClass: top
objectClass: device
objectClass: ipHost
objectClass: ieee802Device
cn: curiosity
description: ip.icmp.in,ip.icmp.out,ip6.icmpv6.in,ip6.icmpv6.out,ip.tcp.out:0-65535,ip6.tcp.out:0-65535,ip.udp.out:0-65535,ip6.udp.out:0-65535,ip.tcp.in:22,ip6.tcp.in:22
ipHostNumber: 185.230.78.199
macAddress: 02:a0:00:01:99:12
owner: o=rover,ou=clubs,dc=adh,dc=crans,dc=org
serialNumber: 199
```

Le script `addadherenthost` crée la machine dans le ldap

```txt
[ _shirenn@flirt ]$ ./addadherenthost --ip 185.230.78.199 --mac
02:a0:00:01:99:02 --vmid 199 --club rover curiosity
```

Le part-feu par défaut laisse passer tous les paquets icmp, tous les paquets en
sortie et ne laisse rentrer que le port 22 en tcp, en ip legacy et en ipv6.

Une fois l'adhérent⋅e ou le club et la machine rajoutée dans le ldap. Les
scripts devraient mettre à jour la configuration du parefeu, du dhcp et de
proxmox.

### Proxmox

Il faut maintenant aller sur le cluster constitué de gulp, odlyd et stitch
pour créer la machine virtuelle. C'est du clic clic dans proxmox, vous êtes
assez grand⋅es pour savoir lire des menus. (Par contre faites bien attention
à  créer les disques sur la pool `vm`).

Ensuite, deux possibilités, soit l'adhérent⋅e vous a juste demandé de
créer la machine virtuelle et à ce moment là c'est fini et iel peut aller la
configurer seul⋅e (c.f. section suivante), soit iel vous a demandé si vous
pouviez lui installer une distribution de son choix.

Vous pouvez alors lui demander une clé publique ssh, et après avoir installé
la vm, vous la déposez dans `/root/.ssh/authorized_keys` (en faisant attention
à créer le `.ssh` avec les bonnes permissions `mkdir -m 0700 /root/.ssh`) et
vous supprimez les mots de passe que vous avez rentrer pendant l'installation
`passwd -d root`).
