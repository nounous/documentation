# Comment redémarrer le Crans ?

Le Crans dispose de beaucoup de serveurs physiques en salle opérateur de l'ENS
(actuellement en SQ39), ainsi que beaucoup de machines virtuelles tournant sur
les hyperviseurs. De temps en temps (environ une fois par an), vous serez
amené⋅es à redémarrer l'entièreté des machines, par exemple pour recâbler la
baie, et il faudra alors faire les choses dans l'ordre. Voici un petit guide
sur comment faire précisément.

## Préparation

Tout d'abord, il faut discuter quelques semaines (~2 mois) en avance du besoin
d'un redémarrage : ce n'est pas un acte à prendre à la légère car des personnes
(même des vieilleux !) utilisent des services du Crans dans la vie de tout les
jours, il faut donc éviter de les couper trop souvent. Cependant, pour faire
des mises à jour majeures, du recâblage, des changements de serveurs, ... cela
peut s'avérer nécessaire.

Il faut discuter du besoin d'un redémarrage pendant au moins 2 IN au préalable
pour bien définir ce qu'il y a précisément à faire, et quelles seront les
nounous présentes pour aider. Bien évidemment, les apprenti⋅es sont les
bienvenu⋅es, mais il est impératif que des nounous soient présentes pour bien
expliquer ce qu'il se passe.

Ensuite, une fois que le besoin de redémarrer est bien acté, il faut prévenir
la **totalité** des adhérent⋅es et des ancien⋅nes adhérent⋅es du Crans avec ce
qu'on appelle communément un "mail all". Pour cela, vous devez vous rendre sur
la VM `re2o`, écrire le mail dans un nouveau dossier puis exécuter le script
d'envoi de mail. Ce mail doit être envoyé au minimum deux semaines avant la
coupure, voire plus si possible. Il ne faut pas hésiter en plus de cela de
prévenir de nouveau les adhérent⋅es via d'autres plateformes de communications
(newsletter BdE, Discord, ...).

## Avant la coupure

Le jour de la coupure, le mieux est de réunir toutes les personnes souhaitant
participer (sans compter les retardaires) dans le bureau du Crans (actuellement
en MB87). Une bonne idée est de prendre le temps qu'il faut pour clairement
poser le plan du jour sur un tableau avec les différents objectifs à réaliser,
potentiellement en parallèle. N'hésitez pas à vous répartir sur différentes
tâches effectuables simultanément si vous êtes suffisamment nombreux⋅ses
(compter au moins deux personnes dont une nounou par tâche pour ne laisser
personne seul).

Vous pouvez au préalable ou par la suite récupérer les badges du Crans ouvrant
la salle opérateur SQ39 au PC sécurité : prendre deux badges au lieu d'un est
une plutôt bonne idée pour qu'un groupe de personnes puisse faire un
aller-retour sans se retrouver coincé à l'extérieur. Ne pas hésiter également à
prendre à boire, de préference dans des récipients qui ne se renversent pas
facilement et des boissons qui ne tâchent pas trop.

En parallèle, vous pouvez garder une trace de toutes les actions effectuées
dans la journée en notant tout ce que vous faites sur un pad (qui n'est pas
hébergé au Crans) pour pouvoir retracer toutes les actions, ce qui est pratique
lorsque que quelque chose casse (un peu souvent donc).

## Éteindre les serveurs

Pour éviter au maximum les problèmes, il est important que certaines machines
soient éteintes avant d'autres. Voici un ordre qui fonctionne avec
l'infrastructure actuelle :

- serv[ENS]
- VM adh (passer par Proxmox sur stitch/odlyd/gulp)
- hyperviseurs adh : stitch/odlyd/gulp
- zamok
- VM adm (sauf routeur-sam !, passer par Proxmox sur sam)
- hyperviseurs adm (sauf sam !) : jack/daniel
- cameron
- tealc
- sam

Surtout ne pas hésiter à vous ssh sur les machines au préalable pour ne pas se
retrouver coincé car le LDAP adm est éteint.

## Rallumer les serveurs

De même que pour l'extinction, l'allumage des serveurs peut se faire dans à peu
près n'importe quel ordre, sauf pour quelques machines importantes. Voici un
ordre qui fonctionne avec l'infrastructure actuelle :

- sam et routeur-sam
- tealc
- cameron
- hyperviseurs adm : jack/daniel
- zamok
- VM adm (passer par Proxmox sur sam)
- hyperviseurs adh : stitch/odlyd/gulp
- VM adh (passer par Proxmox sur stitch/odlyd/gulp)
- serv[ENS]

⚠ Attention : pour zamok, vous devez redémarrer le service postfix avec

```bash
sudo systemctl restart postfix.service
```

après avoir rétabli le réseau et **avant** de redémarrer redisdead afin de ne
perdre aucun mail. Plus exactement : il faut vérifier que le dossier
`/home/mail` sur zamok contienne des centaines d'adhérent⋅es en non pas
seulement quelques dizaines.

Ensuite, assurez-vous bien que **TOUS** les serveurs soient allumés, dont ceux
de serv[ENS] ! Profitez-en alors pour faire des mises à jour nécessaires
pendant que les adhérent⋅es ne sont pas encore de retour sur zamok et les
autres services.
