# How To : les tutos techniques

## Présentation

Ce dossier contient les "how to" (tutoriels) du Crans concernant des tâches
techniques demandant des connaissances généralement sur plusieurs parties de
l'infrastructure.

Si vous souhaitez que d'autres soient ajoutés, n'hésitez pas à vous
réferrer [au CONTRIBUTING.md](../CONTRIBUTING.md).

## Organisation

* [Changer de pseudo](changer_de_pseudo.md) : comment faire pour changer le
  nom d'utilisateur (pseudonyme) d'un⋅e utilisateur⋅rice. Cela peut être
  utile par exemple lors d'un changement d'état civil.

* [Créer une VM pour les adhérent⋅es](creer_vm_adherent.md) : comment
  faire pour créer une VM pour un⋅e adhérent⋅e.

* [Devenir nounou](devenir_nounou.md) : guide pour les nouvelles nounous !

* [Supprimer un⋅e utilisateur⋅rice](supprimet_utilisateur.md) : comment
  supprimer un⋅e utilisateur⋅rice ainsi que toutes ses données.
