# La suppression d'un⋅e utilisateurice

Pour diverses raisons dont il n'est pas question de juger, un⋅e
utilisateur⋅rice peut vouloir supprimer son compte Crans et les données
associées. En particulier cela peut rentrer dans le cadre du
[RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees/). Il y a
certaines informations qu'il faut garder une durée minimale, mais les autres
doivent être supprimées à la demande de l'utilisateurice. C'est pénible
mais faisable : respecter la volonté de l'utilisateur⋅rice dans ce cas est
important. Ce  petit guide est là pour rappeler tout ce qu'il ne faut pas
oublier.

## La demande de suppression

Il faut s'assurer que c'est bien la bonne personne qui fait la demande. En cas
de doute, on peut demander une preuve d'identité (pièce d'identité, mail
depuis l'adresse mail crans...). Ensuite, on peut vérifier que la personne à
bien compris ce que la suppression de compte impliquait : la perte de toutes
les données non sauvegardées par l'utilisateurice, ne plus avoir accès aux
services  qui nécessite un compte crans... Selon ce qui est demandé (et ce
qu'on a le droit de faire), le compte peut être seulement archivé, on peut
supprimer les données liées au compte (en totalité ou partie), voire les
données d'un service  non-lié à un compte crans (wiki...). Si la personne
est encore adhérente, elle va probablement devoir démissionner de
l'association.

## Re2o

Il faut bien un début, c'est sur l'intranet qu'on commence par
supprimer/archiver quelqu'un⋅e.

### Archiver sur Re2o

Si la personne à une facture/adhésion de moins de 10 ans, on ne peut pas
supprimer le compte. Dans ce cas-là, ou si c'est ce que préfère
l'utilisateurice, on archive le compte : dans le profil, il faut changer
l'état de "Actif⋅ves" à "Complétement archivé⋅es". On peut
éventuellement  supprimer certaines informations (bannissement,
établissement...) mais on doit garder les informations de contact et les
factures/adhésions de moins de 10 ans.

On peut repasser au bout des 10 ans pour tout supprimer (ça serait bien
d'avoir un truc automatique ou que l'utilisateurice nous le rappelle).

### Supprimer sur Re2o

Il faut aller dans l'interface d'administration
<https://re2o.crans.org/admin/users/user/> et supprimer l'user. Il va falloir
supprimer les objets protégés avant, django vous dira lesquels.

Ne pas oublier d'aller contrôler le LDAP sur `yson-partou` pour s'assurer que
le compte a bien été supprimé. Pour cela, faire un `sudo shelldap` sur
`yson-partou`, puis taper `cn=Utilisateurs` et s'assurer que `cat cn={pseudo}`
ne renvoie rien.

## Zamok

Le plus évident, il faut supprimer le home ainsi que les mails.

Il suffit de faire un `sudo rm -rf /home/{pseudo}`, pour supprimer le home. De
même pour les mails dans `/var/mail/{pseudo}`.

## Backups

Étape non pressante, mais on peut se rendre sur `backup-ft` et `backup-thot`
pour supprimer `/backup/borg-adh/home/{pseudo}`.

À faire avec précaution. Mais à faire puisqu'on ne garde pas des backups
éternellement. Pas pour des raisons de place mais pour des raisons de
non-conservation de données personnelles (même si les backup ne sont pas
concernées par le RGPD).

## Gitlab

S'il n'y a pas de compte, il n'y a rien à faire.

On se place dans l'hypothèse où l'on veut supprimer un compte. On commence
par se rendre dans l'interface d'administration de Gitlab, section utilisateurs
:
[https://gitlab.crans.org/admin/users](https://gitlab.crans.org/admin/users).
On se rend dans les paramètres du compte, on clique sur "Administration des
utilisateur" puis sur "Supprimer un compte utilisateur" ou "Supprimer un
compte utilisateur et ses contributions". Dans le deuxième cas, il faut faire
attention surtout s'il y a des contributions à des projets partagés (comme
ceux du  crans).

## Owncloud

Il suffit simplement de se rendre dans l'interface d'administration, section
utilisateurices :
[https://owncloud.crans.org/settings/users](https://owncloud.crans.org/settings/users),
puis de chercher le compte et de simplement le supprimer.

## Mailman

On commence par se rendre dans [Django-Admin](https://lists.crans.org/admin/).

S'il n'y a pas de compte, aucune question à se poser.

S'il y a un compte, on le supprime.

Potentiellement, la personne est présente sur mailman de manière non-liée à
son éventuel compte crans. Dans ce cas, il va falloir faire du ménage avec
l'adresse mail et donc de l'aide de l'utilisateurice qui va devoir indiquer son
adresse et les ML qu'iel reçoit/administre. Ça peut être un peu les bazar
si il y a plusieurs adresses non-liées. Si la personne est lae dernièr⋅e
propriétaire d'une liste, il faudra soit la supprimer soit lui trouver un⋅e
nouvelleau propriétaire (c'est pas obligatoire mais alors c'est root@crans.org
le propriétaire par défaut, ce qui n'est pas une solution durable).

## Wiki

Si la connexion par le CAS est configurée, il faut penser à
`rm /var/local/wiki/assowiki/{pseudo}`. Concerne a priori peu de monde.

Si la personne le demande, on peut supprimer son compte wiki et les pages qui
concernent son compte (page perso...), historique compris. Pour cela il faut
aller sur `kiwi` et supprimer le dossier contenant la page. Cela peut concerner
des personnes n'ayant pas de compte crans, mais il faudra le WikiNom.

## The Lounge

S’il y a eu une utilisation de WebIRC, on peut aller sur Zamok et
`rm /etc/thelounge/users/{pseudo}.json` (configuration),
`rm /etc/thelounge/logs/{pseudo}` et `rm /etc/thelounge/logs/{pseudo}.sqlite3`
(pour l'historique des messages).

## Autres données

Il y a probablement des données personnelles qui trainent ailleurs, il faudra
mettre à jour cette page si on en trouve. Par exemple, pour les membres
actif⋅ves qui ont un compte underscore ou les utilisateurices qui ont (eu) une
VM (perso  ou de club).

## Données hébergées mais pas administrée par le Crans

Le Crans héberge des VM pour les clubs (note, serveur photos, bdd med...) qui
peuvent contenir des données personnelles. La personne devra aller voir les
admins de ces services pour supprimer les données.
