# LDAP Adhérents

Le Crans dispose d'une base de données stockant les machines des adhérents
disposant d'une connexion active et accessible vie le protocole LDAP.

## Connexion

Le serveur LDAP est hébergé sur la machine `flirt` et est accessible sur le
port 389. Pour s'y connecter, on peut faire une redirection de ports comme par
exemple :

```bash
ssh -L 1636:172.16.10.114:636 tealc.adm.crans.org -J zamok.adm.crans.org
```

Le plus simple est d'utiliser `shelldap`, permettant de naviguer simplement à
travers la base (voir [la documentation sur le LDAP admin](ldap.md)).

## Schéma

Voici la hiérarchie actuelle du LDAP (mars 2023) :

```txt
dc=adh,dc=crans,dc=org
|
+-cn=admin
|
+-cn=membership
|
+-cn=reader
|
+-ou=users
| +-cn=user (inetOrgPerson)
|
+-ou=clubs
| +-o=club (organization)
|
+-ou=hosts
| +-cn=machine (device, ipHost, ieee802Device)
|
+-ou=networks
  +-ipNetworkNumber=<IPv6> (ipNetwork, ipHost)
```

## Utilisateurs

Les adhérents sont ajoutés dans `ou=users` et ont les attributs suivants :

* `cn`: le pseudo de l'adhérent

* `givenName`: le prénom de l'adhérent

* `sn`: le nom de famille de l'adhérent

* `mail`: l'adresse email de l'adhérent

* `postalAddress`: l'adresse postale de l'adhérent (au format 1 Rue Machin,
  75000 Ville)

* `telephoneNumber`: le numéro de téléphone de l'adhérent

L'attribut `description` est utilisé afin de stocker des informations
supplémentaires:

* `birthDate`: la date de naissance de l'adhérent au format `YYYY-MM-DD`

* `birthLocation`: la ville de naissance de l'adhérent

* `membershipStart`: la date de la première adhésion de l'adhérent au
  format `YYYY-MM-DD`

* `membershipEnd`: la date de fin de l'adhésion de l'adhérent au format
  `YYYY-MM-DD`

* `re2oId`: l'identifiant d'utilisateur de l'adhérent dans l'intranet

## Clubs

Les clubs sont ajoutés dans `ou=clubs` et ont les attributs suivants :

* `o`: le nom du club

* `description`: le pseudo de l'adhérent responsable du club

## Machines

Les machines sont ajoutées dans `ou=hosts` et ont les attributs suivants :

* `cn`: le nom de la machine

* `ipHostNumber`: les addresses IP de la machine

* `macAddress`: l'adresse MAC de la machine

* `description`: l'ouverture de ports de la machine

* `owner`: le dn du propriétaire de la machine (utilisateur ou club)

## Réseaux

Les sous-réseaux adhérents sont ajoutés dans `ou=networks` et ont les
attributs suivants :

* `dn`: le nom du sous-réseau

* `cn`: le pseudo de l'adhérent propriétaire du sous-réseau

* `description` (optionnel) : deux NSRecord et un DSRecord du sous-réseau

* `ipHostNumber`: IPv6 interface avec le sous-réseau

* `ipNetMaskNumber`: masque de sous-réseau

* `ipNetworkNumber`: première IPv6 du sous-réseau
