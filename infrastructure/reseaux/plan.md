# Plan

Voici le plan d'attribution des VLANs et adresses IP sur le réseau du Crans :

| Nom       | Numéro de VLAN         | Description                                                   | Ressources IPv4       | Ressources IPv6                              |
|-----------|------------------------|---------------------------------------------------------------|-----------------------|----------------------------------------------|
| void      | 1                      | VLAN par défaut des switches                                  |                       |                                              |
| srv       | 2                      | Serveurs du Crans à IP publiques                              | `185.230.79.0/26`     | `2a0c:700:2::/64`                            |
| srv-nat   | 3                      | Serveur du Crans derrière un NAT                              | `172.16.3.0/24`       | `2a0c:700:3::/64`                            |
| san       | 4                      | Interconnexion avec les baies de stockage                     | `172.16.4.0/24`       | `fd00:0:0:4::/64`                            |
| freebox   | 8                      | Interconnexion avec la FreeBox                                | `82.65.62.126/24`     | `2a01:e0a:9d4:e1b0:a873:65ff:fe63:6f77/64`   |
| adm       | 10                     | Administration des serveurs                                   | `172.16.10.0/24`      | `fd00:0:0:10::/64`                           |
| adh       | 12                     | Machines des adhérent⋅e⋅s à IP publiques                      | `185.230.78.0/24`     | `2a0c:700:12::/48`                           |
| adh-adm   | 13                     | Administration des machines des adhérent⋅e⋅s                  | `172.16.13.0/24`      | `fd00:0:0:13::/64`                           |
| ens       | 38 (2751 pour l'ENS)   | Interconnexion avec l'ENS (arrive détaggé sur les machines)   | `138.231.136.0/29`    |                                              |
| club      | 2754                   | Clubs et association dans l'ENS                               | `100.66.0.0/16`       | `2a0c:700:54::/48`                           |
| switchs   | 2755                   | Communication locale avec les switchs de l'ENS pour le RADIUS | `172.16.55.0/24`      | `fd00:0:0:55::/64`                           |
| lp        | 2756                   | Connexion avec l'imprimante                                   | `172.16.56.0/24`      | `fd00:0:0:56::/64`                           |

Le serveur dns du crans est romanesco (`172.16.10.128`).
