# DNS au crans

Le crans possède 2 serveurs DNS :

* romanesco : DNS récurisif
* silice : DNS autoritaire

## Configuration du DNS autoritaire

La configuration passe par le LDAP et ce passe en 2 temps.

### Configuration spéciale

Il s'agit des points de la configuration qui ne se génralise très peu.
L'automatisation est gérée par un script python disponible sur le
[gitlab](https://gitlab.crans.org/nounous/dns).

### Configuration générique

Il s'agit de la partie automatisé de la configuration du DNS. En effet, lorsque
vous ajoutez une nouvelle machine dans le ldap, par exemple en suivant le
[guide d'installation](../../howto/reer_vm_crans.md), vous avez ajouté plusieurs
informations :

* Créer l'entrée <machine>.<vlan>.crans.org et/ou <machine>.crans.org
* L'adresse IP (v4 et v6)
* Un (ou plusieurs) nom(s)

Le DNS va récupérer ces données et automatiquement attribuer à la machine et
ses alias les adresses IP v4 et v6 configurées.

### Remarques

Pour la configuration des services du crans ([pad](https://pad.crans.org),
[framadate](https://framadate.crans.org)), etc...), les noms `pad` et
`framadate` doivent être configurés pour pointer sur le reverse proxy. Ainsi,
ils sont considérés comme des alias du reverse proxy dans le ldap.

Par exemple, dans `cn=hodaur.crans.org` on peut lire la ligne
`cn: pad.crans.org` : `pad.crans.org` redirigera donc vers `hodaur.crans.org`
par un enregistrement `CNAME`.

### Régénérer la configuration

Un cron vient régulièrement régénérer la configuration toutes les 5 minutes.
Pour ne pas avoir à attendre, il est alors possible d'utiliser la commande :

```bash
sudo python3 /var/local/services/dns/dns.py -r
```
