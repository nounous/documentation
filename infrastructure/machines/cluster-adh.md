# Cluster adhérents

Le Crans dispose d'un cluster de virtualisation [Proxmox](/outils/os/proxmox.md)
comptant, en date du 7 novembre 2024 trois virtualiseurs, à savoir :

* [odlyd](physiques/odlyd.md)

* [stitch](physiques/stitch.md)

* [gulp](physiques/gulp.md)

Le stockage des machines virtuelles de ce cluster est fait sur `cameron` dans
le dataset `pool/vm-adh` via le VLAN `san`.
