# Debian

C'est le SEUL os qu'on utilise au crans.

## Bookworm

Voilà la liste des machines qui ont été passées sous bookworm :

* wall-e (100)

* eclat (104)

* gitlab-ci (106)

* roundcube (107)

* voyager (109)

* mailman (110)

* belenios (111)

* ptf (113)

* flirt (114)

* boeing (117)

* cas (120)

* ecilis (122)

* trinity (123)

* redisdead (124)

* routeur-2754 (127)

* romanesco (128)

* helloworld (131)
