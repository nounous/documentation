# Infrastructure

L'infrastructure du crans sur de nombreux serveurs. D'une part, des machines
physiques. D'autre part, des machines virtuelles.

## Machines physiques

Les machines physiques, que l'on peut retrouver dans [physiques](physiques) sont
spécialisées pour accomplir des tâches spécifiques :

* [backup](backup.md)
   * [ft](physiques/ft.md)
   * [thot](physiques/thot.md)
* [cluster adhérent](cluster-adh.md)
   * [odlyd](physiques/odlyd.md)
   * [stitch](physiques/stitch.md)
   * [gulp](physiques/gulp.md)
* [cluster administration](cluster-adm.md)
   * [sam](physiques/sam.md)
   * [daniel](physiques/daniel.md)
   * [jack](physiques/jack.md)
* serveur de stockage
   * [cameron](physiques/cameron.md)
   * [tealc](physiques/tealc.md)
* switch
   * arceus (en [LACP](lacp.md))
   * carapuce (en [LACP](lacp.md))
   * dracaufeu
   * salamèche
* [zamok](physiques/zamok.md) (serveur des adhérent⋅es)

Un récapitulatif des serveurs se trouve dans [serveurs](serveurs.md)

## Machines virtuelles

Les machines virtuelles ont pour mission d'accomplir une taĉhe bien spécifique.
Elles sont très nombreuses et mentionnées dans le [qui est qui](qui_est_qui.md).
