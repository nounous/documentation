# Cluster administration

Le Crans dispose d'un cluster de virtualisation [Proxmox](/outils/os/proxmox.md)
comptant, en date du 7 novembre 2024 trois virtualiseurs, à savoir :

* [sam](physiques/sam.md)

* [daniel](physiques/daniel.md)

* [jack](physiques/jack.md)

Le stockage des machines virtuelles de ce cluster est fait sur `tealc`.

[Plus d'information](../qui_est_qui.md) sur les vm qui sont hébergés
