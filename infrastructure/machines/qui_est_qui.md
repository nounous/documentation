# Who's who

Bon, on aime bien choisir des noms rigolos pour les VMs, mais parfois ça
amène à un peu de confusion. Du coup, on va répertorier ici les différentes
machines (virtuelles ou non) et leurs différentes fonctions. Si une machine
n'est pas renseignée, n'hésitez pas à aller jeter un coup d'œil sur [ansible](https://gitlab.crans.org/nounous/ansible)
ou [nixos](https://gitlab.crans.org/nounous/nixos) ou à râler sur `#roots` :)

## Cameron

C'est le serveur qui héberge les homes des adhérent⋅es et leurs
répertoires mails. C'est aussi depuis lui que sont effectués les backups
quotidiennes des données des adhérent⋅es. Une de ses pools sert à stocker
et à exposer sur le réseau les disques des VMs des adhérent⋅es pour
qu'elles soient montées sur le cluster Proxmox des adhérent⋅es.

Liste de logiciels installés :

- ZFS

Redémarrage et mise à jour :

- pas de redémarrage automatique
- éviter de redémarrer, car tous les adhérents perdent leurs disques

## Ft : le serveur de backups

C'est l'un des serveurs de backups. Les backups sont dites « off-site » pour
éviter qu'un accident dans notre salle serveur détruise toutes les données. En
théorie ils ne sont pas dans notre baie. Pour l'instant ft est dans notre baie.

C'est un serveur proxmox, donc peu de choses tournent sur les machines.
On décrit dans les sections qui suivent les VMs qui sont hébergés sur ce
serveur.

Liste de logiciels installés :

- Proxmox

Redémarrage :

- pas de redémarrage automatique
- redémarrage manuel possible (éviter si l'autre serveur de backup à des
  problèmes, car on a pas accès directement aux machines en cas de problème)

### backup-ft : le serveur de backup

C'est sur cette machine virtuelle que sont effectivement faites les backups via
borgmatic.

Liste de logiciels installés :

- borgmatic

Redémarrage et mise à jour automatique.

### routeur-ft : le routeur

Cette machine virtuelle sert à connecter leur hyperviseur et les machines
virtuelles qu'il héberge au reste du réseau d'administration via un tunnel
WireGuard dont l'autre pair est boeing.

Liste de logiciels installés :

- WireGuard

Redémarrage et mise à jour automatique.

## Gulp, Odlyd, Stitch : le cluster proxmox des adhérent⋅es

Les trois serveurs sont organisés en un cluster Proxmox pour hebergé les
machines virtuelles des adhérent⋅es.

Liste de logiciels installés :

- Proxmox

Redémarrage :

- pas de redémarrage automatique
- migrer les VM avant de redémarrer

## Sam, Daniel, Jack : le cluster Proxmox du Crans

Les trois serveurs sont organisés en un cluster Proxmox pour héberger les
machines virtuelles du Crans. Ils hébergent en sus une copie en lecture seule
des bases de données et un réplicat du serveur LDAP.

Liste de logiciels installés :

- Proxmox
- Postgres
- SLAPD (Standalone LDAP Deamon)

Redémarrage :

- pas de redémarrage automatique
- migrer les VM avant de redémarrer

### *-dev

Les serveurs avec `-dev` dans le nom ou un nom très proche d'un serveur de la
liste sont des serveurs de test qui ont plus ou moins la même conf que le
serveur copié.

### apprentis : la machine virtuelle des apprenti⋅es

Play ground pour que les apprenti⋅es puissent s'amuser à faire ce qu'iels
veulent. A priori rien est installé dessus, mais tout peut-être installé
dessus.

Redémarrage et mise à jour automatique.

OS : debian Bookworm

### apprentix : la machine virtuelle des apprenti⋅es

Play ground pour que les apprenti⋅es puissent s'amuser à faire ce qu'iels
veulent. A priori rien est installé dessus, mais tout peut-être installé
dessus.

OS : nixos

### belenios : la démocratie

C'est la machine virtuelle qui héberge le logiciel éponyme qui sert à
organiser des élections sécurisées. C'est un logiciel en OCaml, use with
care or with L3 info.

Redémarrage et mise à jour automatique.

Actuellement non fonctionnelle

### boeing : pont WireGuard

C'est la machine qui héberge la plupart des ponts WireGuard qu'on a vers
l'extérieur (ft, sputnik, thot).

Redémarrage et mise à jour automatique.

OS : debian Bookworm

Alias :

- ns1

### cas : CAS

C'est le CAS. (django-cas)

Redémarrage et mise à jour automatique.

OS : debian Bookworm

### chene : le serveur onlyoffice

C'est la machine virtuelle qui héberge onlyoffice.

OS : debian Bookworm

Alias :

- onlyoffice

### ecilis : DNS de test

C'est une machine virtuelle qui fait partie de l'infrastructure de test et qui
héberge un DNS autoritaire (Knot DNS).

Redémarrage et mise à jour automatique.

OS : debian Bookworm

### eclaircie : le nextcloud

C'est la machine virtuelle qui héberge le serveur nextcloud. Les données sont
automatiquement synchronisées avec owncloud (il y a un lien symbolique entre les
2 dossiers).

OS : debian Bookworm

Alias :

- nextcloud

### eclat : le miroir public

C'est la machine virtuelle qui synchronise les miroirs de logiciels et les
expose au reste d'internet pour que les adhérent⋅es puissent télécharger
les logiciels. Le miroir est hébergé sur tealc et c'est ce serveur que
toutes nos machines viennent interroger pour se mettre à jour. Seul les
machines extérieures utilisent eclat.

Liste de logiciels installés :

- rsyncd
- vsftpd
- Nginx
- apt-mirror
- NTP (syncronisation des clock)

Redémarrage et mise à jour automatique.

OS : debian Bookworm

Alias :

- ntp

### en7 : la connexion de secours

L'ENS nous a donné une IP de secours sur leur réseau au cas où il y ait un
problème sur les routeurs et qu'on ait besoin de se connecter sur le réseau.
Il est projeté d'y hébergé un serveur DNS et un serveur mail.

Redémarrage :

- pas de redémarrage automatique
- redémarrage manuel possible (sauf en période de crise et faire attention si
  c'est votre point d'entrée adm)

OS : debian Bullseye

### ethercalc : les tableurs

C'est la machine virtuelle qui héberge les tableurs ethercalc.

Redémarrage et mise à jour automatique.

OS : dedian Bullseye

### ferle : anchor RIPE

C'est la machine virtuelle qu'on met à disposition du RIPE pour être des bons
élèves. On a donc pas accès à la machine.

### flirt : le LDAP des machines des adhérent⋅es

C'est la machine virtuelle qui héberge le LDAP sur lequel sont consignés les
machines virtuelles des adhérent⋅es et des clubs. Elle dispose d'un serveur
SLAPD. On a l'ambition de rassembler les deux LDAP des adhérent⋅es un jour,
mais ça risque de pas convergé (c.f. constellation).

Redémarrage et mise à jour automatique.

OS : debian Bookworm

Pas d'ansible

### fyre : monitoring

C'est la machine virtuelle qui monitore l'état de l'infrastructure. Il utilise
la suite de logiciels Prometheus, Grafana et [NinjaBot](https://gitlab.crans.org/nounous/NinjaBot).

Redémarrage et mise à jour automatique.

OS : debian Bullseye

Alias :

- monitoring

### gitlab-ci : le serveur d'intégration continue

C'est le serveur sur lesquels sont exécutés les tâches d'intégration continue
grâce à docker (beurk).

Redémarrage et mise à jour automatique.

OS : debian Bookworm

### gitzly : le serveur gitlab

C'est le serveur Gitlab du Crans.

Redémarrage et mise à jour :

- redémarrage automatique
- mise à jour manuelle de Gitlab

OS : debian Bullseye

Alias :

- gitlab
- git

### helloworld : le serveur d'impression

C'est le serveur qui héberge le service web pour l'impression

Redémarrage et mise à jour automatique.

OS : debian Bookworm

Alias :

- imprimante
- printer

### hodaur : le reverse proxy

C'est le reverse proxy.

Liste de logiciels installés :

- Nginx

Redémarrage et mise à jour automatique.

OS : debian Bullseye

### irc : le serveur IRC

C'est le serveur IRC du Crans.

Redémarrage :

- pas de redémarrage automatique
- le redémarrage déconnecte les utilisateur·ices

OS : debian Bullseye

### jitsi : visioconférence

C'est le serveur Jitsi du Crans, l'autre logiciel qu'on utilise pour faire de
la visioconférence.

OS : nixos

### kenobi : les pads

C'est la machine virtuelle qui héberge les pads et les tmpads.

Redémarrage et mise à jour automatique.

OS : debian Bullseye

Alias :

- pad

### kiwi : le wiki

C'est le wiki du Crans qui tourne avec moinmoin (c'est du python2 donc il faut
pas mettre à jour debian qui va drop python2).

Redémarrage et mise à jour automatique.

OS : debian buster

Alias :

- wiki

### linx : serveur d'hébergement de fichier

C'est la machine virtuelle qui héberge le logiciel éponyme.

Redémarrage et mise à jour automatique.

La vm est éteinte.

### listenup : ???

J'ai aucune idée de l'utilité de cette VM.

Redémarrage :

- pas de redémarrage automatique

### livre : Stirling-PDF

C'est la machine virtuelle qui héberge le service stirling-pdf.

OS : NixOS

Alias :

- pdf
- stirling
- stirling-pdf

### mailman : les listes de diffusion

C'est la machine virtuelle qui héberge les listes de diffusion. On utilise
Mailman3, un logiciel Django et la machine dispose aussi de son instance de
Postfix pour pouvoir envoyer et recevoir les mails.

Redémarrage et mise à jour automatique.

OS : debian Bookworm

### neree : galene

C'est le serveur galene du Crans, l'un des deux logiciels qu'on utilise pour
faire de la visioconférence.

Redémarrage et mise à jour automatique.

OS : debian Bullseye

Alias :

- galene

### netns

Probablement en rapport avec des [network namespaces](https://www.man7.org/linux/man-pages/man7/network_namespaces.7.html).

Redémarrage et mise à jour automatique.

OS : debian Bullseye

### owl : serveur POP3/IMAP

C'est le serveur qui expose aux adhérentes leur email en IMAP ou en POP3.

Redémarrage et mise à jour automatique.

OS : debian Bullseye

Alias :

- imap
- pop

### owncloud : le serveur owncloud

C'est le serveur owncloud du crans.

Redémarrage et mise à jour :

- redémarrage automatique
- mise à jour manuelle de owncloud

OS : debian Bookworm

### proxy-pve-adh

Comme son nom l'indique...

Redémarrage et mise à jour automatique.

OS : debian Bullseye

### ptf : la mémoire (fatiguée)

C'est le serveur ftp sur lequel est hébergé les vidéos des installs party du
Crans. Il dispose d'un serveur vsftpd et d'un serveur Nginx. Il est peu
utilisé.

Redémarrage et mise à jour automatique.

OS : debian Bookworm

Alias :

- ptfs

### re2o : la gestion des adhérent⋅es

C'est l'intranet du Crans.

Redémarrage et mise à jour automatique.

OS : debian Bullseye

Alias :

- c3po

### redisdead : serveur mail principal

C'est le serveur mail principal du Crans, c'est sur lui que les mails arrivent
et de lui que les mails partent. Ce n'est cependant pas la destination finale
des mails qui sont redirigés vers Zamok. C'est un serveur Postfix, mais il y a
aussi un serveur OpenDKIM et PolicyD sur la machine.

Redémarrage et mise à jour automatique.

OS : debian Bookworm

Alias :

- smtp
- lists

### romanesco : le DNS récursif

C'est le serveur DNS récursif du Crans.

Liste de logiciels installés :

- Unbound

Redémarrage et mise à jour automatique.

OS : debian Bookworm

Alias :

- la-brosse

### roundcube : le (seul) webmail

C'est le webmail sur lequel les adhérent⋅es peuvent consulter leur mails.
C'est un logiciel PHP.

OS : debian Bookworm

Alias :

- roundcube-srv

### routeur-2754 : le routeur pour l'ENS

Ce routeur est responsable de connecter le Crans au VLAN 2754 de l'ENS.

Redémarrage et mise à jour :

- pas de redémarrage automatique
- mise à jour manuelle de Linux et nftables
- redémarrage manuel possible (surtout tant qu'il sert à rien)

OS : debian Bookworm

### routeur-{sam,daniel,jack} : les routeurs

Les routeurs sont responsables de connecter le reste du Crans au reste du
monde. Chacun d'entre eux est équipé pour pouvoir effectuer cette tâche
seule. À tout instant un seul d'entre eux s'occupe du routage et les autres
attendent. Si un problème devait arriver au routeur actif, l'un des deux
prendraient le relai via Keepalived. Chacun des routeurs disposent d'une copie
du pare-feu généré par un script, d'un serveur radvd pour broadcast des
router advertisement sur le réseau des adhérent⋅es, d'un serveur DHCP pour
aider les machines des adhérent⋅es à configurer leur IPv4 et d'un serveur
BIRD pour échanger des routes avec nos ISPs (aurore et via).

Liste de logiciels installés :

- nftables
- isc-dhcp-server
- radvd (Router Advertisement Daemon)
- BIRD (BGP entre autres)
- Keepalived

Redémarrage et mise à jour :

- pas de redémarrage automatique
- mise à jour manuelle de Linux et nftables
- ne pas redémarrer plusieurs en même temps et faire attention avec
  routeur-sam car c'est notre routeur principal (un peu de downtime si on ne
  change pas les routes avant)

OS : debian Bullseye

Alias :

- r{sam,dan,jack}

### silice : le DNS autoritaire

C'est le serveur DNS autoritaire du Crans.

Liste de logiciels installés :

- BIND

Redémarrage et mise à jour automatique.

OS : debian Bullseye

Alias :

- ns0

### trinity : pont Matrix

C'est le pont Matrix qui permet aux utilisateur·ices de Matrix de participer aux
discussions sur IRC.

Redémarrage :

- pas de redémarrage automatique
- le redémarrage déconnecte les utilisateur·ices

OS : debian Bookworm

### voyager : Framadate & la meilleure vm

C'est la machine virtuelle qui héberge Framadate (un logiciel PHP), et une
base de donnée MySQL. Mais surtout, c'est une VM qui a beaucoup servi de VM de
tests random, son état est donc chaotique et incertain.

Redémarrage et mise à jour automatique.

OS : debian Bookworm

Alias :

- everything
- framadate

### wall-e : le serveur LDAP

C'est le serveur qui héberge le LDAP d'administration. Sur celui-ci sont
consignés toutes nos machines, les utilisateuṛ·ices qui ont accès au réseau
et les privilèges dont iel dispose sur celui-ci, la configuration des zones
DNS.

Liste de logiciels installés :

- SLAPD (Standalone LDAP Deamon)

Redémarrage et mise à jour automatique.

OS : debian Bookworm

Alias :

- wall-e
- walle
- ldap-adm

### yson-partou : le LDAP des adhérent⋅es

C'est le LDAP qui liste les comptes des adhérent⋅es depuis Re2o.

Redémarrage et mise à jour automatique.

OS : debian Bullseye

Alias :

- re2o-ldap

## Sputnik : Serveur mail

C'est un serveur situé chez OVH et qui prend le relai de redisdead s'il cesse
de répondre.

Le serveur contient également un serveur DNS, un git2 de secours et héberge la
page [status](https://status.crans.org).

## Tealc

C'est le serveur qui héberge les disques de nos VMs. C'est un gros machins
pleins de disques qui utilise ZFS pour exposer une pool en NFS sur le réseau
pour qu'elle soit montée par le cluster Proxmox. Il héberge aussi un serveur
PostgreSQL sur lequel sont hébergées (presque) toutes les bases de données du
Crans. C'est aussi lui qui héberge les homes des nounous et le miroir des
logiciels.

Liste de logiciels installés :

- ZFS
- PostgreSQL
- Nginx

Redémarrage et mise à jour :

- pas de redémarrage automatique
- mise à jour manuelle de PostgreSQL
- éviter de redémarrer, car tous les services perdent leurs disques

*Nota bene* : **ATTENTION** La vdev ZFS qu'on utilise est configuré en
`ashift=9`, ce qui veut dire qu'elle ne peut accepter que des disques qui ont un
blocksize physique d'au plus `2**9=512 bytes`. La plupart des disques sur le
marché aujourd'hui ont une blocksize de `4096 bytes`. Si on remplace un disque
dans la pool avec disque de`4096` des importants problèmes de performances vont
se répercuter dans toute l'infra. En effet, le stockage pour les VM est sur
tealc, donc une baisse de performance niveau IO va entrainer une augmentation de
la charge moyenne des serveurs en la multipliant par 5 ou 10.

## Thot : le serveur de backup

Actuellement, thot est en cours de déploiement.

## Zamok

C'est le serveur physique des adhérent⋅es. Il sert principalement à deux
choses, donner un terminal aux adhérents et destination finale des mails.
C'est aussi lui qui héberge les pages personnelles des adhérent⋅es. Le
[webirc persistent](https://webirc.crans.org) y est aussi hébergé.

Liste des logiciels installés :

- Postfix + OpenDIKM + Rspamd
- Apache HTTP Server (bouh)
- webIRC (TheLongue)

Redémarrage et mise à jour :

- pas de redémarrage automatique
- mise à jour manuelle de Linux et nftables
- éviter de redémarrer car les adhérent⋅es peuvent avoir des scripts qui
  tournent
