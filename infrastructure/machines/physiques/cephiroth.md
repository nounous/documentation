# Cephiroth

Cephiroth est l'un des protagonistes du cluster ceph.

## Alias

Alias :

* cephiroth

## VLAN

Cephiroth est sur les vlan :

* adm (`172.16.10.3`)
* ceph
* san
* srv-nat
