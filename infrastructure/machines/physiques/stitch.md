# Stitch

Stitch est l'un des trois protagonistes du [cluster-adm](../cluster-adh.md).

## Alias

Alias :

* stitch

## VLAN

Stitch est sur les vlan :

* adm (`172.16.10.17`)
* san
* srv-nat
