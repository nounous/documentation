# Imprimante

Le crans dispose actuellement d'une imprimante `Lexmark X952de`.

## Connexion à distance

L'imprimante ne parle que sur le `vlan 2756` et a pour ip `172.16.56.1`.

L'accès à distance à l'interface d'administration de l'imprimante se fait
via la vm `helloworld` qui héberge le site pour commander les impressions.
Par une redirection du port 443 (https).

```bash
ssh -NL 8443:172.16.56.1:443 helloworld.adm.crans.org
```

Puis dans un navigateur il faut se rendre sur `https://localhost:8443`.
Certains onglets ne sont disponible qu'après avoir saisie le mot de passe
admin qui se trouve dans le pass (`crans/imprimante`) du Crans.
