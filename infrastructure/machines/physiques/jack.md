# Jack

Jack est l'un des trois protagonistes du [cluster-adm](../cluster-adm.md).

[Plus d'information](../qui_est_qui.md) sur les vm qui sont hébergés

## Alias

Alias :

* pve-3
* jack

## VLAN

Jack est sur les vlan :

* adm (`172.16.10.13`)
* san
* srv-nat
