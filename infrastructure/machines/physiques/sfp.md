# Optiques SFP(+) et fibres optiques

Le Crans dispose d'un backbone 10G pour relier ses serveurs, les modules
optiques utilisés sont des [10GBASE-SR compatibles
Arista](https://www.fs.com/fr/products/36982.html) (afin d'être compatibles
avec nos switches Arista). Ces modules sont reliés par des [jarretières
optiques LC LC OM4](https://www.fs.com/fr/products/40220.html).

Pour son interconnexion avec ViaRézo et Aurore le Crans utilise des modules
optiques [10GBASE-LR](https://www.fs.com/fr/products/36983.html) reliés aux
platines de fibres par des [jarretières optiques LC SC
OS2](https://www.fs.com/fr/products/96102.html).

## Qu'est-ce qu'est une optique SFP ?

C'est un module actif composé d'un micro-contrôlleur (par exemple un STM32
Cortex-M3) et d'un ou plusieurs lasers. Il permet de relier une fibre optique
à un switch.

### Codage des optiques

Une optique est codé avec un identifiant constructeur, changeable avec une
codeuse. Un switch d'un certain constructeur n'acceptera que certains codes.
Par exemple sur des Quanta, coder correctement les optiques permet de collecter
des métriques tel que leur température. Sur certains switchs il est possible
d'activer le support des optiques avec le mauvais code.

## Choses à ne pas faire

Il peut être tentant d'acheter une optique 10km pour une liaison de 100m en
partant sur l'idée que « qui peut le plus, peut le moins ». Dans le cas de
l'optique c'est faux. Utiliser une optique trop puissante sur une distance
beaucoup plus petite que son design va brûler le récepteur.

Une optique chauffe et un laser qui chauffe dérive légèrement. Malgré qu'il
y ait des plaques à effet Peltier dans certaines optiques pour compenser, il
reste une bonne idée de vérifier que les optiques sont correctement
refroidites par le switch.
