# Odlyd

Odlyd est l'un des trois protagonistes du [cluster-adm](../cluster-adh.md).

## Alias

Alias :

* odlyd

## VLAN

Odlyd est sur les vlan :

* adm (`172.16.10.16`)
* san
* srv-nat
