# Gulp

Gulp est l'un des trois protagonistes du [cluster-adm](../cluster-adh.md).

## Alias

Alias :

* gulp

## VLAN

Gulp est sur les vlan :

* adm (`172.16.10.18`)
* san
* srv-nat
