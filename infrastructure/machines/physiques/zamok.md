# Zamok

Zamok est un serveur laissé à la disposition des adhérents. Il est notamment
accessible en [ssh](/outils/logiciels/ssh.md). Il s'agit aussi du serveur qui
héberge les pages perso.

## Alias

Alias :

* users
* ssh (ssh.crans.org)
* zamok

## VLAN

Zamok est sur les vlan :

* adm (`172.16.10.31`)
* adh-adm
* san
