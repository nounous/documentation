# Arista 7150S-24

Le CRANS possède des switches
[Arista 7150S-24](https://www.arista.com/en/products/7150-series) avec 24 ports
SFP/SFP+.

## Se connecter au switch

Il existe différentes façons de se connecter au switch.

### Port console en série

Les switches Arista ont un port série au format RJ-45, il suffit de brancher un
cable USB-Série+Série-Ethernet et d'utiliser `screen`.

Par exemple :

```bash
sudo screen /dev/ttyUSB0
```

Pour éviter de lancer screen en root, il est également possible d'ajouter son
utilisateur dans le groupe `dialout` avec `gpasswd -a $USER dialout`.

### Port de management

> L'unité de management du switch est isolé du reste du switch. Il est donc
> impossible de ping les machines connectés au switch depuis l'interface
> d'administration.

Il est également possible de se connecter en SSH sur le port de management du
switch, que l'on peut activer depuis la connexion en série :

```txt
switch>enable
switch#config
switch(config)#management ssh
switch(config-mgmt-ssh)#exit
switch(config)#interface management 1
switch(config-if-Ma1)#ip address 172.16.10.100/24
switch(config-if-Ma1)#exit
switch(config)#exit
switch#write
```

Il faut ensuite créer un utilisateur pour la connexion ssh.

```txt
switch(config)#username [nom d'utilisateur] secret [mot de passe]
```

On peut aussi préciser sa clé ssh

```txt
switch(config)#username [nom d'utilisateur] sshkey file tftp:[ip du serveur tftp]/[fichier de clé publique]
```

## Remettre à zéro le switch

Se connecter en série au boot du switch puis entrer dans Aboot en utilisant
`^C` quand c'est indiqué puis entrer les commandes :

```txt
Aboot#cd /mnt/flash
Aboot#mv startup-config startup-config.old
Aboot#exit
```

Le switch va alors reboot avec la configuration par défaut. Le login par défaut
de l'administrateur est `admin` et n'a pas de mot de passe.

## Autoriser les SFP tiers

Par défaut le switch n'accepte que les SFP/SFP+ Arista, il est possible
d'utiliser des SFP tiers (comme ceux de !FiberStore) avec les commandes
suivantes :

```bash
switch>enable
switch#bash
[admin@switch ~]$ touch /mnt/flash/enable3px
[admin@switch ~]$ sudo reboot
```

Le switch va alors reboot en mode de compatibilité avec les SFP tiers.

## Actions d'administration

### Créer un VLAN

Pour que le switch accepte de commuter les trames taggées il faut créer le
VLAN :

```txt
switch>enable
switch#config
switch(config)#vlan 2
switch(config-vlan-2)#
```

Cela suffit à créer le VLAN, la liste des VLAN peut être affichée avec la
commande `show vlan`.

### Configurer les VLANs

On va commencer par la stratégie de bourrins. On va autoriser tous les VLANs
sur le port courant. Pour cela, on le passe en mode trunk pour qu'il accepte de
commuter sur tous les VLANs connus du switch :

```txt
switch(config)#interface ethernet 1
switch(config-if-Et1)#switchport mode trunk
```

C'est un peu violent et en plus ça a le mauvais gout de tagger tous les VLANs
sauf celui dit natif (par défaut celui de plus petit nombre défini ou 1). On
peut remédier au deuxième point en lui disant de tagger son VLAN natif :

```txt
switch(config)#interface ethernet 1
switch(config-if-Et1)# switchport trunk native vlan tag
```

Et si on veut restreindre le caractère bourrin d'autoriser tous les VLANs, on
peut specifier lesquels nous intéressent (et même en ajouter à posteriori) :

```txt
switch(config)#interface ethernet 1
switch(config-if-Et1)# switchport trunk allowed vlan 1-3,9-77,999
switch(config-if-Et1)# switchport trunk allowed vlan add 7
```

### Configurer le LACP

Pour configurer le LACP entre 2 ports, on commence par les sélectionner dans
l'interface de configuration, puis on lui dit de faire du LACP sur ces deux
ports. On utilise le plus petit nombre de port sélectionné comme identifiant de
LACP.

```txt
thor(config)#interface ethernet 3-4
thor(config-if-Et3-4)#channel group 3 mode active
thor(config-if-Et3-4)#exit
```

Puis ensuite on peut configurer le reste (VLANs et tout …) dans l'interface
Port-Channel 3. (Et non pas sur les interfaces eth).

```txt
thor(config)#interface Port-Channel 3
thor(config-if-Po3)#
```

### Configurer le MLAG (Multi chassis Link AGregregation)

Nos switch arista supportent une feature que arista appele le MLAG. Cette feature
permet notaments de faire du LACP avec des ports entre deux switch et donc d'avoir
une redondance sur les switchs.

Il faut commencer par configurer le MLAG entre les deux switchs.

```txt
  TODO
```

On peut ensuite configurer un lien en LACP. On configure ici les ports 3 de
chaque switch en redondance sur le port-channel 3 / mlag 3.

sur le premier switch :

```txt
arceus(config)#interface ethernet 3
arceus(config-if-Et3)#channel-group 3 mode active
arceus(config-if-Et3)#interface port-channel 3
arceus(cofnig-if-Po3)#mlag 3
```

sur le second switch :

```txt
carapuce(config)#interface ethernet 3
carapuce(config-if-Et3)#channel-group 3 mode active
carapuce(config-if-Et3)#interface port-channel 3
carapuce(config-if-Po3)#mlag 3
```

On peut vérifier l'état des liens configurés en mlag via :

```txt
carapuce(config-if-Po11)#show mlag interfaces
                                                                 local/remote
   mlag       desc             state       local       remote          status
---------- ---------- ----------------- ----------- ------------ ------------
      1                  active-full         Po1          Po1           up/up
      3                  active-full         Po3          Po3           up/up
      5                  active-full         Po5          Po5           up/up
      6                     inactive         Po6          Po6       down/down
      7                     inactive         Po7          Po7       down/down
      8                  active-full         Po8          Po8           up/up
      9                     inactive         Po9          Po9       down/down
     10                  active-full        Po10         Po10           up/up
     11                  active-full        Po11         Po11           up/up
     12                     inactive        Po12         Po12       down/down
```

Il faudra configurer les vlans et autres sur les port-channel des deux switchs.

### Mettre à jour le switch

> C'est pas forcément nécessaire et ça peut tout casser, si néanmoins tu
> ressens la nécessité de le faire, voila comment.

Bon la première étape, c'est de trouver les images de EOS. Et là, c'est déjà
pas trivial de les trouver… Arista fait payer les images de ses OS et du coup,
il faut chercher par des moyens détourner de les trouver. Nous on les avaient
trouvé là <http://41.78.188.108/sw/eos/> mais rien n'assure que cela vous aidera.

Ensuite on va télécharger l'image sur le switch, mais là un problème de place
pourrait survenir, il n'y a pas beaucoup d'espace mémoire dans la mémoire flash
et il faudra probablement supprimer l'ancienne image avant de télécharger la
nouvelle (considérer la copier quelques part avant pour la sauvegarder en
utilisant par exemple :

```bash
copy [ancienne image] scp:[user]@[ip du serveur]/[chemin vers le home du user]/[ancienne image]
```

```txt
georges(config)# delete [ancienne image] #### uniquement si nécessaire
georges(config)# copy tftp:[ip du serveur tftp]/[nouvelle image] flash:/[nouvelle image]
```

Et là on va dire à l'os de boot dessus :

```txt
georges(config)# boot system flash:/[nouvelle image]
```

Il se peut que la commande rate à cause de manque d'espace sur la mémoire
flash. Si c'est le cas, je vous conseille de recopier l'ancienne image sur le
switch et de passer votre chemin. Mais si vous êtes vraiment tenieux, vous
pouvez essayer de stocker l'image autre part et ensuite de la deployer (mais
c'est à vos risques et périls mais surtout à celui du switch). Si vous avez
cependant réussi à déployer l'image, "il ne faut surtout pas oublier de
sauvegarder les modifications".

```txt
georges(config)#write
```

## Rendre compatible le switch aux personnes ayant de l'hyperacousie

Par défaut les `fan-speed` sont à 80% ce qui donne un bruit de disqueuse, tout
en gardant des températures très basses tout en ayant aucune charge.

Il peut être intéressant de baisser au moins temporairement la vitesse si vous
travaillez à côté du switch et que vous êtes en visio. Le bruit généré n'est
pas linéaire avec la vitesse du ventilo et trop baisser la vitesse peut
éteindre violemment le switch dès qu'il chauffe trop. Déjà 10% de vitesse en
moins réduit grandement le bruit.

Par les observations d'erdnaxe en visio, 60% est un bon compromis,

```txt
georges(config)#environment fan-speed override 60
```

Pour voir la vitesse actuelle,

```txt
georges(config)#show environment cooling
```

Pour remettre le mode auto,

```txt
georges(config)#environment fan-speed auto
```
