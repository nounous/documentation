# Daniel

Daniel est l'un des trois protagonistes du [cluster-adm](../cluster-adm.md).

[Plus d'information](../qui_est_qui.md) sur les vm qui sont hébergés

## Alias

Alias :

* pve-2
* daniel

## VLAN

Daniel est sur les vlan :

* adm (`172.16.10.12`)
* san
* srv-nat
