# Thot

Ce document détaille la procédure d'installation qui a été suivi pour installer
thot.

## Installation initiale de debian

On utilise normalement l'installateur de debian en utilisant le partitonnement
suivant :

```txt
                              +---------------------------+ +--------------------------+
+-----------+---------------+ | +-----------------------+ | | +----------------------+ |
| /dev/sda1 |   /dev/sda2   | | |       /dev/sda3       | | | |       /dev/sda4      | |
+-----------+---------------+ | +-----------------------+ | | +----------------------+ |
   ⤷  GRUB    ⤷ /boot/efi     |                           | |                          |
+-----------+---------------+ | +-----------------------+ | | +----------------------+ |
| /dev/sdb1 |   /dev/sdb2   | | |       /dev/sdb3       | | | |       /dev/sdb4      | |
+-----------+---------------+ | +-----------------------+ | | +----------------------+ |
                              |            RAID           | |            RAID          |
                              +---------------------------+ +--------------------------+
                                ⤷ /boot (/dev/md0)             ⤷ see below (/dev/md1)

+-----------------------------------------------------------------------+
| /dev/md1                                                              |
| +-------------------------------------------------------------------+ |
| | /dev/md1_crypt                                                    | |
| | +---------------------------------------------------------------+ | |
| | | /dev/mapper/thot-vg                                           | | |
| | | +---------------------------+---------------------------+---+ | | |
| | | | /dev/mapper/thot--vg-root | /dev/mapper/thot--vg-swap | … | | | |
| | | +---------------------------+---------------------------+---+ | | |
| | |   ⤷ /                         ⤷ swap (coucou esum)            | | |
| | +---------------------------------------------------------------+ | |
| +-------------------------------------------------------------------+ |
+-----------------------------------------------------------------------+
```

Et on finit en installant GRUB sur /dev/sda.

## Dropbear

On va maintenant installer un petit serveur ssh dans le initramfs pour qu'au
boot du serveur on puisse se ssh et déchiffrer les disques.

```bash
sudo apt install dropbear-initramfs
```

On edite ensuite les options du logiciel dans
`/etc/dropbear-initramfs/config` pour lui préciser des options de lancement :

```bash
DROPBEAR_OPTIONS="-I 180 -j -k -p 2222 -s"
```

L'option importante qu'on set ici est `-p 2222` qui dit au serveur ssh
d'écouter sur le port 2222 plutôt que 22. On fait ça pour qu'un client ssh ne
croit pas que l'host ait changé entre l'état au boot et l'état courant.

En plus de ça on va fournir au serveur une clé ssh publique via laquelle on
pourra se connecter. Pour générer une clé ssh (`ssh-keygen(1)`) on fait

```bash
ssh-keygen -t ed25519 -f <keyname>
```

Puis on copie la clé publique de `<keyname>.pub` dans
`/etc/dropbear-initramfs/authorized_keys`.

### Configuration réseau

On va aussi configurer le initramfs pour configurer ses interfaces réseaux au
boot. On édite le fichier `/etc/initramfs-tools/initramfs.conf` et on rajoute
une variable `IP` à la fin du fichier :

* Pour qu'il prenne une ip statique

  ```bash
  IP=<ip>::<gateway>:<netmask>:<hostname>:<interface>
  ```

* Pour qu'il prenne une ip via dhcp

  ```bash
  IP=::::<hostname>:<interface>:dhcp
  ```

Ensuite, on régénère le initramfs

```bash
initramfs -u
```

Pour tester que ça fonctionne, on reboot et après le chargement du initramfs,
on fait un `ssh -i <keyname> -p <ip>` suivi d'un `cryptroot-unlock` qui
devrait vous demander la passphrase pour déchiffrer le disque.

## Installation de proxmox

<https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_11_Bullseye>

## Installation de zfs

<https://timor.site/2021/11/creating-fully-encrypted-zfs-pool/>
