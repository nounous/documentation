# Tealc

Tealc est l'un des serveurs de stockage du crans. Il contient notamment toutes
les bases de données ainsi que les disques des VMs et des homes des nounous.

## Alias

Alias :

* ldap
* mirror
* mirror2
* pgsql
* tealc

## VLAN

Tealc est sur les vlan :

* adm (`172.16.10.1`)
* san
