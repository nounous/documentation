# Infrastructure

## Présentation

Ce dossier contient toute la documentation concernant l'infrastructure du
Crans, c'est à dire les machines, les VM (machines virtuelles), les réseaux
virtuels ou encore le fonctionnement des services internes au Crans (les LDAP,
PostgreSQL, ...).

## Organisation

* [`machines`](machines) : contient la documentation de l'ensemble des
  machines physiques et virtuelles du Crans. On y inclut également par extension
  les appareils physiques (comme les switchs).

* [`reseaux`](reseaux) : documentation sur les réseaux (virtuels ou non)
  utilisés au Crans, et sur leur utilisation.

* [`services`](services) : documentation sur les services mis à disposition
  par le Crans et pour le Crans (les LDAP, PostgreSQL, le CAS, ...).
