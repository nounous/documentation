# Documentation Crans

Voici la documentation technique du Crans à destination des apprenti⋅e⋅s
et des nounous du Crans. Vous pouvez retrouver (prochainement) toute la
documentation mise en forme à l'adresse <https://doc.crans.org>. Attention :
ce n'est pas la documentations des utilisateurices qui se situe dans [ce
dépôt](TODO).

## Utilisation

Pour la bonne maintenance des services du Crans, il est impératif d'avoir une
documentation permettant une bonne compréhension du fonctionnement global de
l'infrastructure et des services. C'est pourquoi il est essentiel que ce
dépôt soit le plus à jour possible en toutes circonstances.

Il est par ailleurs recommandé à toutes les nounous et aux apprenti⋅e⋅s
souhaitant s'investir dans le Crans d'avoir une copie locale de ce depôt (avec
un petit `git clone`) : il est plus simple de dépanner un service critique
sans avoir à chercher où est la documentation (sur un serveur gitlab qui est
potentiellement cassé également ^^').

Si vous voyez un service/un serveur/... non documenté, ou si vous souhaitez
voir un tutoriel (appelé ici "how to") sur un sujet, réferrez-vous à
[CONTRIBUTING.md](CONTRIBUTING.md). Il est **impératif** de le lire avant de
contribuer pour une bonne cohérence organisationnelle et stylistique.

## Organisation

Tous les dossiers contiennent un fichier `README.md` que vous pouvez lire pour
comprendre le rôle de ce qui est présenté dans ce dossier ainsi qu'une
brêve description de tous les fichiers et sous-dossiers présents.

Voici l'organisation générale de la documentation :

* [`compte_rendus`](compte_rendus) : ensemble des comptes rendus des IN
  (Inter-Nounous) ;

* [`howto`](howto) : liste non exhaustive de tutoriels explicatifs sur une
  démarche précise nécessitant généralement la manipulation de plusieurs
  services ;

* [`infrastructure`](infrastructure) : documentation concernant
  l'infrastructure du Crans, c'est-à-dire les machines (physiques et
  virtuelles), les réseaux utilisés ou encore le fonctionnement des services
  mis à disposition pour une utilisation interne au Crans (les LDAP,
  PostgreSQL, ...) ;

* [`outils`](outils) : présentation rapide (ou détaillée selon la
  pertinence) de tous les outils et protocoles utilisés par le Crans ;

* [`services`](services) : présentation rapide des différents services du
  Crans mis à disposition des utilisateurices.

En plus de ces dossiers, on trouve également les fichiers suivants jouant un
rôle particulier :

* [`CONTRIBUTING.md`](CONTRIBUTING.md) : fichier expliquant comment bien
  contribuer à la documentation du Crans ;

* [`CRITICAL.md`](CRITICAL.md) : que faire si c'est la panique ?

* [`LICENSE.md`](LICENSE.md) : licence sous laquelle est publiée la
  documentation du Crans.

* [`SECURITY.md`](SECURITY.md) : bases de sécurité, valables au Crans mais
  pas que.
