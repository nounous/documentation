# Securité

Le but ici est de donner quelques conseils généraux de sécurité qui est bon
à respecter le plus possible lors de changements de l'infrastructure ou de
services. La liste ne se veut pas exhaustive, mais permet de donner l'esprit
global qu'il faut avoir.

## Sécurité des comptes utilisateurs

Que ce soit les membres de l'association, comme les membres actif⋅ves, il faut
prendre les précautions pour s'assurer que ses identifiants ne fuient pas dans
la nature :

* **Ne pas avoir de comptes locaux avec des mots de passe actifs.**
  Un compte avec possibilité d'authentification par mot de passe DOIT être
  dans l'annuaire LDAP. Cela permet de faciliter le changement de mot de
  passe et ne pas laisser des mots de passe cassés traîner.

* **Ne pas partager un compte.**
  Et oui, on évite de filer son mot de passe à son voisin.

* **Avoir un mot de passe solide.**
  `toto2345` n'est pas un bon mot de passe et prend en moyenne moins de 5
  minutes à être bruteforcée avec
  [john](https://fr.wikipedia.org/wiki/John_the_Ripper).

* **Favoriser l'authentification par paire de clées.**
  Pour les services le permettant (SSH, Git...), il est préférable d'avoir
  une paire de clé. Une clé privée ne doit jamais quitter le périphérique
  où elle a été créée (que ça soit un token matériel ou une machine
  Linux). Il est préférable de protéger la clé privée par un mot de
  passe. Ainsi si une de vos machine est compromise, vous pouvez révoquer
  seulement sa clé et tracer où elle a été utilisée.

* **Favoriser la double authentification.**
  Pour les services critiques (paiement, accès root...) il est préférable
  d'ajouter un second facteur d'authentification et d'alerter l'équipe si
  seulement le premier facteur a réussi mais le second a échoué.
  Pour les services Web, voir le standard WebAuthn. Pour OpenSSH il existe
  des intégrations avec des applications de second authentification.

* **Utiliser un gestionnaire de mots de passe pour les secrets partagés.**
  Pour les secrets partagés au sein de l'équipe technique, on les chiffre
  pour les clés privées de chaque administrateur.

## Sécurité des logiciels développés

* **Ne pas fixer strictement les versions des paquets.**
  Il faut s'assurer que les contraintes sur les dépendances permettent
  d'obtenir les mises à jour de sécurité. Souvent il n'est que nécessaire
  de fixer la version majeure (`~=2.1`, plutôt que `==2.1.1`).

* **Privilégier le gestionnaire de paquet de la distribution.**
  La distribution rétroporte les mises à jour de sécurité. Il est donc de
  bon goût d'utiliser le plus possible ses paquets pour simplifier la mise
  à jour de l'ensemble de l'infrastructure.

* **Utiliser des outils de linting avec des règles de sécurité.**
  Les bons outils de linting connaissent les patterns pouvant potentiellement
  se traduire en faille de sécurité.

* **Lancer le logiciel en non-privilégiés.**
  Un logiciel ne devrait jamais avoir besoin de `root` ou de `sudo`.
  Lors de l'écriture du service, on peut également ajouter des protections
  supplémentaires pour augmenter l'isolation.

## Sécurité de l'infrastructure

* **Faire ses backups, et vérifier qu'elles marchent.**

* **Faire des scans des ports ouverts.**
  Pour cela on peut lancer `nmap` sur la plage d'adresses IP.

* **Monitorer et alerter.**

* **Segmenter les services.**
  Isoler les services sur différentes machines et augmenter l'indépendance
  permet d'améliorer la robustesse de l'infrastructure.
