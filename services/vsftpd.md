# VSFTPD

VSFTPD est une implémentation de serveur ftp connue pour être très
sécurisée  (comme son nom, very secure FTP daemon, l'indique). Un serveur FTP
est un  serveur d'échange de fichier un peu daté qui permet à des clients de
récuperer  des fichiers d'un serveur, et de les y pousser. Aujourd'hui on lui
préferait  l'utilisation de sftp qui utilise le protocole ssh comme support si
le client a  besoin d'un accès en écriture au serveur ou simplement du
protocole http/https  sinon. Cependant certaines technologies ainsi que des
sysadmins vieillissant ne  savent pas faire autre chose que du ftp. C'est pour
ça entre autre qu'on  continue de supporter le protocole à certains endroits
dans l'infrastructure.

## Principe

Comme décrit plus tôt, le protocole FTP est un ancêtre de l'internet (la
première RFC qui le mentionne prédate l'utilisation de la stack TCP/IP). En
particulier il n'a initialement pas été prévu pour opérer à travers des
pare-feu et des NAT. C'est pourquoi aujourd'hui un serveur FTP peut
fonctionner de deux manières différentes :

### FTP actif

Quand un client initie une connection avec un serveur FTP, il fournit au
serveur  un numéro de port sur lequel le serveur pourra tenter de se connecter
pour procéder à l'échange de données. Cependant au moment où le client
redirige le  serveur sur un autre port, rien ne permet d'affirmer que celui
sera contactable par le serveur (en fait de nos jours, si les deux machines ne
sont pas sur le même réseau local, il est quasiment certain que le serveur
n'arrivera pas à contacter le client).

### FTP passif

Pour remedier à ça, le protocole ftp surporte maintenant un mode passif, où
c'est à la charge du serveur de rediriger l'utilisateur sur un port
contactable  du serveur.

### Connexion au serveur

Pour se connecter à un serveur ftp un client peut le faire de manière anonyme
si le serveur lui permet. Mais il est possible de restreindre l'accès à une
base d'utilisateurs sécurisée par mot de passe. Cependant, le protocole ne
prévoit pas que le message soit chiffrée pendant le transfert.

### Support de TLS

Le support de TLS a été ajouté au protocole. Il est possible pour le client
de demander à l'initialisation de la connexion que celle-ci s'effectue de
manière chiffrée. Cela permet en particulier d'éviter de faire transiter des
messages en clair sur le réseau.

## Installation

On se contente de tirer le logiciels des dépots de logiciels de debian :
`sudo apt install vsftpd`.

## Configuration

Munissez vous de votre `man 5 vsftpd.conf` préférée et d'un bon remontant
parce que mes ailleux c'est pas clair.

### Configuration réseau

L'option `listen_ipv6=YES` permet de dire au daemon d'écouter **aussi** en
ipv6. Elle ne doit donc pas être utilisée avec l'option `listen=YES` qui ne
ne configurera qu'un socket ipv4.

Comme expliquer précedemment pour fonctionner de nos jours il est assez
fréquent qu'un serveur ftp doivent supporter le mode passif.

```txt
# Autorise le client à demander au serveur l'utilisation du mode passif
pasv_enable=YES
# Configure la range de ports du serveurs vers l'un desquels le serveur pourra
# rediriger le client
pasv_min_port=45000
pasv_max_port=48000
```

### Configuration SSL

Il est possible de fournir à vsftpd un certificat et une clé privée pour
qu'un client puisse demander l'établissement d'une connexion chiffrée.

```txt
# Active le chiffrement
ssl_enable=YES
# Rensigne l'emplacement des certificats
rsa_cert_file= /etc/letsencrypt/live/crans.org/cert.pem
rsa_private_key_file= /etc/letsencrypt/live/crans.org/privkey.pem
# Autorise les clients anonymes à bénéficier du chiffrement
allow_anon_ssl=YES
```

### Écriture des logs

On peut demander à vsftpd de logger les connexions, les téléchargements et
les téléversements au serveurs.

```txt
xferlog_enable=YES
```

### Configuration des utilisateurs

Ici la configuration des utilisateurs dépend principalement de ce que l'on
souhaite faire avec le serveur. Je couvrirais donc les deux cas d'utilisations
qu'on a au crans, le cas d'un mirroir de logiciel (donc accesible pour des
utilisateurs non privilégiés en lecture seule uniquement) et le cas d'un
serveur où une liste d'utilisateur peut venir déposer des fichiers.

Pour notre premier cas d'utilisation, la configuration est triviale :

```txt
# Autorise les utilisateurs anonymes à se connecter
anonymous_enable=YES
# Expose le dossier /pool/mirror/pub à ces utilisateurs en lecture seule
anon_root=/pool/mirror/root
```

Pour le second, c'est un peu plus déclicat :

```txt
# Autorise la connexion aux utilisateurs locaux (compte unix)
local_enable=YES
# Restreint les utilisateurs qui peuvent se connecter à ceux présent dans
# /etc/vsftpd.user_list
userlist_enable=YES
userlist_deny=NO
# Autorise les utilisateurs à écrire sur le serveur
write_enable=YES
# Restreint l'accès en écriture au home des utilisateurs
chroot_local_user=YES
```
