# Framadate

Framadate est un software qui permet de faire un choix commun entre plusieurs
options depuis un navigateur web.

## Installation

Pour installer le logiciel, on clone le dépot du projet depuis le serveur gitlab
de framadate :

```bash
git clone https://framagit.org/framasoft/framadate/framadate.git -b 1.1.11 /var/www/framadate
```

Il est aussi nécessaire de tirer quelques
dépendances à la main :

```bash
sudo apt install php-fpm php-intl php-mbstring php-mysql composer
```

On utilise ensuite composer pour installer les
dépéndances php `cd /var/www/framadate; sudo -u www-data composer install`.
On créée le fichier de log à la main

```bash
sudo -u www-data touch /var/www/framadate/admin/stdout.log; sudo chmod 0600 /var/www/framadate/admin/stdout.log
```

## Configuration de la base de données

Le logiciel a besoin d'une base de donnée, ici on a pas le choix et on doit
utiliser une base de données mysql qu'on installera donc en local :
`sudo apt install mariadb`. Dans laquelle on crééra (via le shell mysql `sudo mysql`):

* `CREATE USER 'framadate'@'localhost' WITH PASSWORD 'ploptoto';` un
  utilisateur framadate

* `CREATE DATABASE framadate;` une base de données éponyme

* `GRANT ALL PRIVILEGES ON DATABASE framadate to 'framadate'@'localhost';` sur
  laquelle on donnera tous les droits à l'utilisateur framadate

## Configuration du logiciel

Le fichier de configuration du logiciel est trouvable dans
`/var/www/framadate/app/inc/config.php`. Le fichier de configuration a des
défauts plutôt sains mais il y a quand meme quelques options de configuration à
préciser :

* pour la connexion à la base de donnée :

  ```txt
  // Database server name, leave empty to use a socket
  const DB_CONNECTION_STRING = 'mysql:host=localhost;dbname=framadate;port=3306';
  // Database user
  const DB_USER= 'framadate';
  // Database password
  const DB_PASSWORD = 'ploptoto';
  ```

* pour le serveur smtp, on authorise l'envoir de mail `'use_smtp' => true`,
  on précise le serveur smtp auquel se connecter `'port' => 25` et la source
  des mails : `const ADRESSEMAILREPONSEAUTO = 'framadate@crans.org';`

## Configuration nginx

Il faut maintenant configurer nginx et protéger l'accès à la zone
d'administration par mot de passe.

## Migrations

Il ne reste plus qu'à effectuer les migrations dans la section d'administrations
du site `https://framadate.crans.org/admin/migrations.php`
