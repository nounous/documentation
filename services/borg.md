# Borg

Pour prevenir certains incidents dus à des erreurs de manipulation, le collège
technique a mis en place en système de backups. Le but de celui-ci est de
pouvoir, si le besoin s'en fait ressentir, aller rechercher un fichier supprimé
ou écrasé. Bien qu'il serait possible de simplement faire une copie périodique
des données sur un serveur distant, on lui préfère des solutions un peu plus
développées qui permettent de faire des sauvegardes incrémentales. Fut un temps,
la solution technique en place était le logiciel
[BackupPC](https://backuppc.github.io/backuppc/), cependant celui-ci commençant
à dater et ayant une faible ergonomie, nous utilisons aujourd'hui le logiciel
[borg](https://borgbackup.readthedocs.io/en/stable/) et son frontend
[borgmatic](https://torsion.org/borgmatic/).

Avant de présenter ce logiciel, attardons nous un peu sur ce qu'on entend par
faire des backups incrémentales. Pour cela revenons à notre piètre solution
initiale qui consiste à copier (à grands coups de rsync, unison, scp…) toutes
les données que l'on souhaiterait sauvegarder. Pour que ces sauvegardes soient
utiles, il faut les réaliser assez fréquemment (quelque part entre un jour et
une semaine disons). Cependant, à chaque nouvelle backup réalisée, celle-ci va
venir écraser le contenu de la précédente et ainsi, on ne dispose jamais plus
que d'une copie des données de la veille. Ainsi, des dysfonctionnements
automatiques ou simplement de la négligence peuvent faire que l'on ne se rendra
compte que l'on avait besoin de récupérer notre fichier il y a déjà quelques
jours et qu'il a maintenant déjà été supprimé de la copie de sauvegarde. On
préfererait donc pouvoir disposer d'une collection de sauvegardes qui remonte
sur quelques jours voir moi en fonction de la politique de rétention de
sauvegarde qu'on mettrait en place. Par exemple, une sauvegarde pour chacun des
septs derniers jours puis quelques unes espacée d'une semaine. Si l'on reprend
notre solution naïve, on peut facilement en étendre l'usage pour ce nouvel
office : on réalise une copie complète des données par jour que l'on souhaite
sauvegarder. Suivant la politique de rétention que l'on a donné plus tôt cela
consisterais en une quinzaine de copies distinctes des données à sauvegarder et
donc une multiplication par quinze de la taille de celle-ci. Si je jette un œil
rapide à la taille que prends aujourd'hui les données utilisateur⋅ices, je vois
qu'on utilise un peu plus de 2.5T de stockage, donc si l'on suit la logique, on
devrait disposer d'une ou plusieurs machines totalisant un stockage d'environ
40T. Inutile de le dire je pense, mais cela fait (beaucoup) trop. Heureusement
pour nous, d'un jour sur l'autre l'intégralité de nos données stockées ne varie
pas et seule une petite partie change sur le cours d'une journée. Ainsi, si l'on
ne stocke que les différences accumulé sur une journée par rapport à la veille,
on se retrouvera avec une taille beaucoup plus raisonnable de sauvegarde. C'est
~la déduplication.

## Qu'est ce que l'on sauvegarde ?

Au crans, on sauvegarde deux types de données :

* D'un côté, on a les données d'administration, c'est une partie des disques
  de tous les serveurs (généralement le /etc et le /var sauf cas particulier),
  le home des nounous, les bases de données…

* De l'autre côté; on a les données des adhérents, c'est à dire : le contenu
  de leur répertoire personnel et de leur dossier mail qui au jour où j'écris
  ses lignes peut contenir au maximum 30G et 10G de données respectivement.

Même si les deux types de sauvegardes sont fait avec borg/borgmatic, la manière
dont les sauvegardes sont effectivement lancé varie entre les deux. Je
m'attarderais ici plus sur le fonctionnement générale de borg/borgmatic et donc
je présenterais le cas de la sauvegarde de données administratives qui est une
utilisation assez normale du logiciel. L'explication de comment se passe les
sauvegardes pour les données adhérents est disponible [ici](services/borg.md).

## Borg et borgmatic

Borg est un logiciel client/serveur permettant d'effectuer des sauvegardes
incrémentales, dédupliquées et chiffrées par le client. Le serveur est simple à
installer et utilise ssh comme protocole de transport entre lui et le client.
Ainsi, il suffit de donner un accès ssh au client via une paire de clés pour lui
autoriser à discuter avec le serveur. Quelque chose ressemblant à ça dans le
`.ssh/authorized_keys` de l'utilisateur sur le serveur qui fera les backups :

```txt
command="borg serve --restrict-to-path {{ chemin vers les sauvegardes }}",restrict {{ clé publique ssh }}
```

Avec l'installation du paquet `borg` c'est la seule chose à faire sur le serveur
et le reste du travail sera réalisé par le client. Après avoir aussi installé
`borg` sur le client, on peut déjà commencer à faire quelques sauvegardes.
Cependant, si l'on utilise que borg pour les faire (ce qui est possible), on se
retrouvera avec des commandes bien trop longues dans lesquels on passe notre
temps à spécifier des arguments plusieurs fois : comment contacter le serveur ?
où stocker les sauvegardes ? quel politique de rétentions garder ? que faut-il
sauvegarder ? Borgmatic est un frontend de borg qui définir une bonne fois pour
toutes ses options dans un fichier de configuration pour ensuite n'avoir plus
qu'à faire des appels simples à borgmatic qui pasera à borg tous les paramètres
dont il a besoin. Le but n'est pas ici d'être exhaustif sur ce que peut/doit
contenir ce fichier de configuration. Je vous redirige pour ça vers la
[documentation
officielle](https://torsion.org/borgmatic/docs/reference/configuration/). Ici,
je vais plus chercher à vous donner quelques options minimales et des exemples
d'utilisations de borgmatic. Commençons par donner le contenu d'un fichier de
configuration partiel :

```txt
location:
    source_directories:
        - /etc
        - /var
    repositories:
        - ssh://borg@backup-server:/folder/to/backup/to

storage:
    encryption_passphrase: PASSPHRASE
    ssh_command: ssh -i /etc/borgmatic/id_ed25519_borg

retention:
    keep_daily: 4
    keep_monthly: 6
```

Dans la section `location`, on définit deux choses : ce que l'on souhaite
sauvegarder et où on souhaite le sauvegarder. Ici on va backupé le `/etc` et
`/var` de notre client sur un serveur distant qu'on contactera via ssh. Dans la
section suivante, `storage`, on définit quelques options relatives au stockage
et à comment y accéder. Premièrement, on donne la passphrase de chiffrement des
sauvegardes (attention, si elle est perdue, les sauvegardes ne seront plus
accessibles). Ensuite, on précise comment accéder au serveur (ici, l'information
importante est où se trouve la clé privée pour pouvoir se connecter). Enfin,
dans la section retention, on définit notre politique de retention : le serveur
ne doit stocker qu'une backup quotidienne sur les 4 derniers jours, et une
backup mensuelle sur les 6 derniers mois.

En supposant les autres options de fonctionnement renseignées dans le fichier,
que je ne détaillerais pas ici, il est maintenant possible de commencer à
utiliser le logiciel et faire nos premières sauvegardes.

### Créer l'archive de stockage

La commande suivante permet de créer l'archive où seront stockées les
sauvegardes sur le serveur.

```bash
borgmatic init -e repokey
```

### Faire une première sauvegarde

Pour faire une sauvegarde, rien de plus simple, on appelle simplement le
programme borgmatic sans option. Il est néanmoins possible de demander un
affichage plus verbeux pour le débogage.

```bash
borgmatic
borgmatic -v 2
```

### Lister les différentes sauvegardes dans l'archive

```bash
borgmatic list
```

### Monter une sauvegarde précise sur un point de montage

```bash
borgmatic mount --archive {{ nom de l'archive ou latest pour la dernière }} --mount-point /mnt --path {{ chemin à monter de l'archive }}
borgmatic mount --archive latest --mount-point /mnt --path etc/nginx/nginx.conf
```

### Automatiser les backups

Pour réaliser des backups périodique, on peut en laisser la charge à un cron ou
à systemd à l'aide d'un timer.
