# Radvd

Radvd est une implementation du mécanisme d'autoconfiguration des adresses IP
en ipv6. Il permet au client de connaitre l'adresse du routeur et de se
construire lui même une adresse à partir du préfixe annoncé par le routeur
et de son adresse MAC. Des extensions du protocoles permettes aussi la
configuration des serveurs dns.

## Principe

### Adresse ipv6 et EUI-64

Il est possible pour un pair possédant une adresse MAC et un prefixe ipv6 de
taille au plus `/64` de construire une adresse ipv6. Cependant, rien n'assure
qu'elle soit unique à ce stade et il faudra par la suite le vérifier. Mais on
ne s'interesse ici qu'au processus de construction.

De son adresse MAC (donnée à 48 bits), on dérive un identifiant de
l'interface sur le lien de 64 bits en insertant les 16 bits `FF:FE` au milieu
de l'adresse MAC puis on inverse le 7 ème bit. On peut ensuite suffixer cet
identifiant au préfixe dans lequel on chercher à construire notre addresse
ipv6. Ainsi `00:25:90:69:8c:74` devient `00:25:90:ff:fe:69:8c:74` puis
`02:25:90:ff:fe:69:8c:74` qui donne l'adresse `fd00::10:0225:90ff:fe69:8c74/64`
dans le prefixe `fd00:0:0:10::/64`.

### Processus d'autoconfiguration complet

Lorsqu'une machine configure une interface supportant l'ipv6 sur un lien, celle
ci va tenter de se crééer une premier adresse local au lien. Elle est donc
judicieusement appelé ip de lien locale. Pour cela, il va se contruire comme
expliquer précedemment une adresse dans le prefixe `fe80::/64`. Pour s'assurer
qu'elle est effectivement unique, il va envoyer au groupe ipv6 all-node
(`ff02::1`) qui comme son nom l'indique contient tous les nœuds si quelqu'un
utilise déjà l'adresse. Si c'est le cas, il ne poussera pas sa configuration
plus loin. Sinon après un délai sans réponse, il s'assignera l'adresse.

Notre pair a donc maintenant une adresse sur le lien et il peut écouter les
annonces (judicieusement appelé routeur-advertissement ou RA) périodiques
qu'envoie le routeur au groupe all-node. S'il ne désire pas patienté jusqu'à
la prochaine annonce le client peut envoyer un routeur-sollicitation au groupe
all-routers. Dans ces annonces le routeur inclut entre autre le prefixe dans
lequel le client devrait prendre son adresse, son adresse de lien local ainsi
que des informations à propos des serveurs dns. Une fois ces informations
récupérées le client peut procéder aux dernières étape de configuration
de son interface réseaux en assignant l'adresse qu'il a construit à partir
du préfixe que lui a fourni le routeur (en vérifiant en amont son unicité
sur le lien) et en configurant la gateway à l'adresse de lien local du
routeur.

## Installation

On se contente de tirer le logiciel des repository debian
`sudo apt install radvd`.

## Configuration

La page de manuel détaillant les options de configurations de radvd est
consultable en utilisant `man 5 radvd.conf`.

La configuration du logiciel se fait par bloc d'interface sur lesquel on va
publier nos RA. Dans un bloc on peut ensuite définir

* des options génériques à propos de l'annonce comme par exemple l'intervalle
  auquel notre routeur devrait publier des annonces ou le temps que la route
  devrait être conservé par le client.
* les prefixes que le routeur va annoncer sur le lien et certaines options
  associées
* les options de configurations du DNS

Par exemple, le bloc suivant configure des annonces sur l'interface ens1 envoyé
toutes les 30 secondes avec comme préfixe `2a0c:700:12::/64`, `adh.crans.org`
comme liste de recherche pour les dns et `2a0c:700:12::ff:fe00:9912` comme
adresse du serveur dns.

```txt
interface ens1 {
        AdvSendAdvert on;
        AdvDefaultPreference high;
        MaxRtrAdvInterval 30;

        prefix 2a0c:700:12::/64 {
                AdvRouterAddr on;
        };

        # La zone DNS
        DNSSL adh.crans.org {};

        # Les DNS récursifs
        RDNSS 2a0c:700:12::ff:fe00:9912 {};
};
```
