# Services DHCP

Le script de services DHCP permet de générer des fichiers de bails statiques
pour [/critical/networking/isc-dhcp-server](isc-dhcp-server) à partir de la base
de données d'adhérent du crans. Actuellement celle ci est gérée par re2o donc on
fait des requêtes à l'API de re2o pour récupérer la liste des machines des
adhérents.

## Installation

Pour installer le logiciel, il faut cloner le répertoire git
<https://gitlab.crans.org/nounous/dhcp.git>. Actuellement la convention veut
que les services soient placé sous le chemin `/var/local/services/` mais rien ne
l'impose. Pour tourner  le script a besoin du paquet `python3-jinja2`. Il faut
aussi crééer le dossier `generated` à la racine du dépot dans lequel seront
stocker les fichiers de bails générer.

## Configuration

### Re2o

Pour récupérer la liste des machines, le script a besoin de pouvoir parler à
l'API re2o. Pour ça on fournit au script les informations de connexions dans le
fichier de configuration `re2o-config.ini` à la racine du dépot :

```txt
[Re2o]
hostname = 172.16.10.156
username = services
password = ynerant_aime_le_php
```

### dhcp.json

Dans le fichier de configuration `dhcp.json` aussi présent à la racine, on
réalise le reste de la configuration du logiciel. Une seule option est supporté
pour le moment : `extensions` qui permet de filtrer sur les extensions des
machines pour lesquels on souhaite exporter un fichier de bails :

```json
{
    "extensions": [
        "adh.crans.org"
    ]
}
```
