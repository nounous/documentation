# Pare-feu

Voir `/critical/networking/nftables.md` pour une documentation minimale sur
nftables.

Voici un pare-feu basique dans lequel des adhérent·e·s se trouvent derrière un
routeur :

```txt
#!/usr/sbin/nft -f

flush ruleset

#           +~~~~~~+
#           | IPV4 |
#           +~~~~~~+
define adh_prefix = 172.16.54.1 - 172.16.54.98
define srv_prefix = 185.230.79.0/24

define nat_out = 185.230.79.37

#           +~~~~~~+
#           | IPV6 |
#           +~~~~~~+
define adh_prefix6 = 2a0c:700:54::/64


# définit les adresses utilisées par les adhérent·e·s
define adh4 = 100.66.0.0/16 ### Qu'est-ce que cette adresse ?

#           +~~~~~~~~~~~~~~+
#           | Filter table |
#           +~~~~~~~~~~~~~~+
table inet filter {
    # Définiton des ports ouverts sur chaque machine
    # (Utilise un mapping plutôt que des sets pour éviter uen complexité
    # terrifiante en O(nm) et passer à O(n+m) \(\ddot\smile\))
    set authorized_in_forward_tcp4 {
        type ipv4_addr . inet_service
        flags interval
    }
    set authorized_in_forward_udp4 {
        type ipv4_addr . inet_service
        flags interval
    }
    set authorized_in_forward_tcp6 {
        type ipv6_addr . inet_service
        flags interval
    }
    set authorized_in_forward_udp6 {
        type ipv6_addr . inet_service
        flags interval
    }

    chain input {
        type filter hook input priority 0; policy drop;

        # Accept local traffic
        meta iiftype loopback accept comment "allow from loopback"

        # Accepts existsing connections
        ct state { related, established } accept
        ct state invalid drop

        # Accept SSH and DHCP
        meta l4proto { udp, tcp } th dport { ssh, 67 } ct state new accept

        # Accept ping
        ip protocol icmp accept icmpv6 type { nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-router-advert, nd-neighbor-advert, echo-request, echo-reply } accept
    }

    chain forward {
        type filter hook forward priority 0; policy drop;

        # Accept established and ping connnections
        ct state { established, related } accept
        ct state invalid drop

        # On log tout ce qui est neuf et qui passe
        log prefix "FORWARD: "
        ip protocol icmp accept icmpv6 type { nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-router-advert, nd-neighbor-advert, echo-request, echo-reply } accept

        # Ouverture de ports pour les gens se trouvant derrière
        ip daddr . tcp dport @authorized_in_forward_tcp4 accept
        ip6 daddr . tcp dport @authorized_in_forward_udp4 accept
        ip daddr . udp dport @authorized_in_forward_tcp6 accept
        ip6 daddr . udp dport @authorized_in_forward_udp6 accept

        # Accepter toutes les connexions sortantes des clubs / adh et les logger
        ip saddr $adh_prefix accept
        ip6 saddr $adh_prefix6 accept
    }
}

#           +~~~~~+
#           | NAT |
#           +~~~~~+
table inet loggonsTout {
    chain prerouting {
        type nat hook prerouting priority dstnat;
        # On log ce qui est neuf
        ct state new log prefix "LOGALL: "
    }
}

table ip nat {
    chain postrouting {
        type nat hook postrouting priority srcnat;

        # traffic des adhérants et des clubs ===> $nat_out (range)
        ip saddr $adh_prefix snat to $nat_out persistent
    }
}
```
