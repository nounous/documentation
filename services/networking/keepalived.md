# Keepalived

Keepalived est une implémentation du protocole VRRP. Il permet de partager des
ips entre plusieurs machines. Si celle qui qui porte l'adresse à un moment
tombe, c'est une des autres qui prend le relai.

## Principe

Le protocole VRRP permet de s'assurer qu'un ensemble d'adresses ip soit
toujours tenues par un pair sur le reseaux. Par exemple, au crans, pour
rajouter de la résilience sur la panne d'un routeur, les ips du routeurs sont
partagées entre plusieurs machines. Celle qui porte les ressources en
fonctionnement normal est appelée le `MASTER` et les autres sont appelées
les `BACKUP` et on leur assigne des priorités décroissantes. Sur le lien
local que partage les différentes machines, elles négocient qui devrait
porter les ressources actuellement. En ipv4, la négociation se fait en
multicast en utilisant l'ip `224.0.0.18` et en ipv6 cela ce fait via le groupe
`ff02::12` (il faut bien faire attention à ce que le parefeu ou qu'un
commutateur ne le filtre pas). Si le porteur de l'ip actuel arrète de
transmettre des alertes VRRP alors le candidat avec la plus haute priorité
s'affectera les ressources.

## Installation

On se contente de tirer le logiciel des repositorys debian
`sudo apt install keepalived`. Il y est packagé (correctement) depuis Jessie.

## Configuration

Il est fortement recommandé de sortir le `man 5 keepalived.conf` qui détaille
assez bien les différents cas d'utilisation du logiciel.

Le logiciel nous permet de définir quelques options générale dans la section
global_defs. En particulier on peut y préciser le nom de la machine et la
configuration de l'envoi de mail :

```txt
global_defs {
  notification_email { root@crans.org }
  notification_email_from keepalived@crans.org
  smtp_server smtp.adm.crans.org
  router_id routeur-sam
}
```

On peut ensuite rentré dans la configuration des instances. Une instance
correspond à un bloc de ressource que l'on souhaite partagé entre deux
machines. Comme les protocoles sous-jacent sont distincts en ipv4 et en ipv6,
la  configuration pour des ressources ipv4 et ipv6 se fait dans des blocs
différents. Dans une instance, on doit préciser la priorité de la machine
dans l'instance, l'interface surlaquelle se font les alertes vrrp,
l'identifiant de l'instance et un bloc définissant les ressources qui sont
partagées. Il est aussi possible de définir un script qui sera appelé au
changement d'état. On donne ici l'exemple d'une instance en ipv4.

```txt
vrrp_instance VI_ALL {
  # Définit l'état de priorité de la machine dans l'instance
  state MASTER
  priority 150
  # Active l'envoie des mails lors des changement d'état
  smtp_alert

  # Lien sur lequel se font les alertes VRRPS
  interface ens18
  # Identifiant de l'instance sur le lien
  virtual_router_id 60
  # Fréquence des envois d'alertes VRRPs
  advert_int 2

  # Script appelé lors des changement d'état
  notify /var/local/services/keepalived/keepalived.py

  # Bloc définissant les ressources partagées
  virtual_ipaddress {
      185.230.79.62/26 brd 185.230.79.63 dev ens22 scope global
  }
}
```

### Script de changement d'état

Comme préciser dans la section précédente, il est possible d'appeler un
script  lors des changements d'état de l'instance. Keepalived appelera alors
le script avec les paramètres suivant :
`script [TYPE] [NAME] [STATE] [PRIORITY]` où `[TYPE]` vaut `INSTANCE` dans
notre cas, `[NAME]` donne le nom de l'instance concernée, `[STATE]` donne
l'état vers lequel on transitionne et `[PRIORITY]`  la  priorité dans
l'instance.

En utilisant un script, il est possible (comme on le fait actuellement au
crans) de laisser à keepalived le soin de démarrer certains services.
