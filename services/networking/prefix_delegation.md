# Délégation de préfixe IPv6

La longueur des adresses IPv6 (128 bits) nous permet de déléguer des
préfixes (c'est à dire des sous-réseaux) IPv6.

Il est possible de fournir des préfixes grâce au protocole DHCPv6 mais cette
solution n'a pas été privilégiée au Crans : nous lui avons préféré une
attribution statique.

Dans [Re2o](/services/re2o.md) une application Django, `prefix_delegation`, a
été écrite permettant notamment d'attribuer des préfixes IPv6 de taille
fixée à des utilisateurs.

Dans cette application il est possible de définir des sous-réseaux IPv6 ainsi
que le taille des préfixes contenus dans ces sous-réseaux puis de créer des
préfixes et de les attribuer à des adhérents en leur assignant des
passerelles (machine de l'adhérent à contacter pour router ce préfixe).

L'application fourni une API Rest HTTP permettant au script `prefix_delegation`
de rafraîchir la liste des préfixes et de leur passerelle dans le noyau de la
VM de routage.

Ce script utilise un protocole particulier pour gérer ses routes, ce protocole
est défini dans `/etc/iproute2/rt_protos.d` sur les machines où le script des
déployé.

Le reverse [DNS](/tools/dns.md) est également délégué : des serveurs DNS
peuvent être attribués à un ou plusieurs préfixes afin de résoudre les
adresses IPv6 délégués vers des noms de domaines choisis par l'adhérent. Il
est même possible de configurer du DNSSEC pour le reverse DNS. Ceci est géré
par le script `dns` sur la VM `silice`.
