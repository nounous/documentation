# Bird

Bird est un daemon de routage. Il supporte de nombreux protocoles de routages
comme BGP and OSPF. Au Crans on l'utilise pour faire de l'échange de route
avec notre FAI grace au protocole BGP. C'est donc le seul que je détaillerais
ici pour le moment.

## Principe

### L'IANA, le RIPE et le Crans, les IPs et les numéros d'AS

Après l'adoption du protocole IP, il a été décidé que ce serait à l'IANA
(Internet Assigned Number Authority) de se charger de l'allocation des adresses
IPs. Elle délègue cependant cette responsabilité à des entités régionales
appelées RIR pour Regional Internet Registry. En Europe, c'est le RIPE (pour
Réseaux IP Européens, cocorico) qui remplit cette office. Les gens à qui un
RIR alloue des ressources est appelée un LIR pour Local Internet Registery. Et
vous savez quoi, depuis 2017 le Crans est un LIR à part entière \o/. Et le
RIPE (notre RIR) nous a assigné les ressources suivantes :

* `185.230.76.0/22`
* `2a0c:700::/32`
* `204515`

Les deux premiers éléments de cette liste suivent assez logiquement ce que
j'ai dit plus haut. Le RIPE est censé assigné au LIR européen des ressources
IP. Il nous a donc délégué un `/22` d'IPv4 et un `/32` d'IPv6 (aussi
appelée une mole). Mais le troisième élément semble pour le moment plus
abscons. En fait l'IANA et les RIR ne se contente pas de fournir des adresses
IP mais toutes les ressources relatives à l'internet. En particulier, le dernier
élément de notre liste est un numéro d'AS pour autonomous system. Un AS est
un réseaux informatique qui opère sa propre politique de routage interne.
C'est le cas au Crans. Un numéro d'AS est identifiant unique alloué par un
RIR et est utilisé dans certains protocoles internet entre AS comme le
protocole BGP.

### Le protocole BGP

Le protocole BGP est un protocole de routage qui permet l'échange de route
entre deux AS. J'avoue avoir un peu la flemme de rentrer dans les détails de
comment marche le protocole (si ça vous intéresse aller lire la RFC). Mais
l'idée générale est que deux AS pairs dans le réseau vont s'échanger des
routes qui décrivent ce qu'ils sont capables d'atteindre. Si un AS recoit deux
chemins différents pour la même route, il choisira celle qui a l'AS path le
plus court, c'est à dire celle qui passe par le moins d'AS différent avant
d'arriver à bon port.

## Installation

On tire bird2 des repository debian : `sudo apt install bird2`.

## Configuration

Le logiciel n'inclut pas de page de manuel détaillant les différentes options
de configuration mais le [site internet de bird](https://bird.network.cz/) est
très bien fourni en explication.

Comme le logiciel supporte plusieurs protocole sa configuration est ségmenté
en fonction des différents protocoles configurées en plus de quelques options
de configurations fournis générales.

### Options de configurations générales

Il est nécessaire de définir l'identifiant du routeur sur le lien. Cet
identifiant en ipv6 comme en ipv4 doit être une adresse ipv4 qui pointe vers
le routeur pour assurer son unicité.

### Le protocole kernel

Le protocole kernel permet de définir la politique que bird va utiliser pour
inserer des routes dans le kernel. Les options import et export permettent de
définir quels routes bird va importer de la table de routage et exporter vers
la table de routage. Au Crans, on choisit assez simplement d'insérer toutes
les routes que bird connait dans le noyau et de ne rien importer du noyau.
L'option scan time permet de régler la fréquence à laquelle bird lira le
contenu de la table de routage du kernel. Il est possible d'être plus
précis⋅e sur les routes importé et exporté du protocol en utilisant des
filtres.

```txt
protocol kernel {
  ipv4 {
    import none;
    export all;
  };
}

protocol kernel {
  ipv6 {
    import none;
    export all;
  };
}
```

### Le protocole device

C'est pas vraiment un protocole, il permet simplement à bird de lister les
interfaces du routeur.

```txt
protocole device {}
```

### Le protocole static

Le protocole static permet de définir des routes manuellement. Il est possible
de définir plusieurs types de routes mais au Crans on n'en définit que d'un
seul type : les routes reject. Le comportement de ces routes est différent
selon s'il est inséré dans le kernel où s'il est partagé via un protocole
de partage de routes. Dans le premier cas, il dit au kernel assez litteralement
de jetter tous les paquets vers cette route, comme le kernel choisi la route de
plus long préfixe qui correspond à la destination cela revient à dire au
routeur de jetter les paquets que le routeur ne sait pas joindre dans le bloc
d'ips. Cependant dans un protocole d'échange de routes, il signifie que le
routeur connait une route vers les éléments de ce bloc d'ips. Par exemple :

```txt
protocol static {
  ipv4;
  route 185.230.76.0/22 reject;
}

protocol static {
  ipv6;
  route 2a0c:700::/32 unreachable;
}
```

### Le protocol BGP

Comme expliqué au dessus, le protocole BGP permet de faire de l'échange de
routes entre différents AS. Il faut spécifier notre numéro d'AS, le numéro
d'AS du pair et l'adresse sur laquelle on souhaite le contacter. Il est
possible de préciser avec quelle adresse on veut le contacter. On peut là
aussi définir des filtres d'import et d'exports de routes. Par exemple :

```txt
protocol bgp aurore4 {
    description "BGP4 session with example";
    local 203.0.113.1 as Crans_asn;
    neighbor 203.0.113.2 as example_asn;
    strict bind;

    ipv4 {
        import all;
        export where source ~ [ RTS_STATIC ];
    };
}

protocol bgp aurore6 {
    description "BGP6 session with example";
    local 2001:db8:28::1 as Crans_asn;
    neighbor 2001:db8::28::2 as example_asn;
    strict bind;

    ipv6 {
        import all;
        export where source ~ [ RTS_STATIC ];
    };
}
```
