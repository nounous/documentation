# ISC-DHCP-SERVER

ISC-DHCP-SERVER est une implémentation du protocole DHCP qui permet à un
hôte  via une requète DHCP au serveur de récupérer une adresse IP, des
informations à  propos des dns et l'adresse de la passerelle. Il est
actuellement un peu  vieillisant mais il est très robuste, il est envisagé de
le remplacer par son  successeur kea quand il sera plus stable.

## Principe

Le protocole DHCP utilise le transport UDP et les ports 67 du serveur et 68 du
client.

### Configuration initiale

Le protocole DHCP fonctionne de la manière suivante :

1. le client qui cherche à configurer son interface réseaux envoie une
   requète DHCPDISCOVER sur le lien en broadcast avec comme adresse cible
   255.255.255.255 en spécifiant son adresse MAC dans la requète

1. le serveur vérifie s'il est configuré pour répondre au client :

   * si ce n'est pas le cas il lui renvoie un DHCPDECLINE

   * sinon il lui fait une offre cohérente par rapport à sa configuration et
     son état actuel via un paquet DHCPOFFER

1. le client lui répond avec une requète DHCPREQUEST dans lequel il précise
   l'adresse qu'il souhaiterait que le serveur dhcp lui alloue

1. le serveur statue finalement sur l'adresse qu'il alloue effectivement au
   client dans un paquet DHCPACK où il précise en plus de l'adresse des
   options configurations supplémentaires comme l'adresse des serveurs de noms
   ou l'adresse de la passerelle. Il précise aussi pour combien de temps il
   réalise cette allocation (on parle de bail ou de lease).

### Rafraichir le bail

Au bout du temps configuré par le serveur DHCP, il est possible pour le client
de demander au serveur de renouveller l'allocation de l'adresse s'il l'utilise
encore. Pour cela, il se contente de reprendre à partir de la troisième
étape précédente.

### Filtrage et protection

Attention, il n'est pas rare qu'on composant actif (un switch par exemple) sur
le lien dispose par défaut de fonctionnalité bloquant par sécurité le
traffic dhcp. C'est une option de sécurité désactivable. Il est aussi
possible parfois  d'utiliser cette fonctionnalité à notre avantage pour
améliorer la sécurité de notre infrastructure en précisant l'adresse de
notre serveur dhcp permettant au  switch de bloquer les acquittements dhcp
excepté celle provenant de nos serveurs.

Certains switchs permettent aussi de configurer certains ports qu'il interdit
au machines derrière de communiquer avec une autre adresse que celle qu'elles
ont récupéré par dhcp.

## Installation

On se contente de tirer le logiciel des repository debian
`sudo apt install isc-dhcp-server`.

## Configuration

La page de manuel détaillant les options de configurations de radvd est
consultable en utilisant `man 5 dhcpd.conf`. Le ficihier de configuration
principale se trouve dans `/etc/dhcp/dhcpd.conf` et un fichier de configuration
additionel se trouve dans `/etc/default/isc-dhcp-dserver`.

### /etc/dhcp/dhcpd.conf

Le logiciel se configure en fonction des plages de sous-réseaux qu'on souhaite
que le serveur dhcp administre. Il permet en plus de préciser des options de
configurations par défaut et des options de configuration générale. Un sous
réseau se définit dans un bloc `subnet` pour lequel on précise le
sous-réseau, le masque associé et l'interface sur laquelle il servira ces
requêtes :

```txt
subnet 100.64.0.0 netmask 255.255.0.0 {
  interface "ens23";
}
```

Il y a deux manières (utilisées au crans) de régler comment le serveur dhcp
alloue des ips à ces clients. La première est de lui donner dans un fichier
externer une liste d'association ip MAC au format suivant :

```txt
host gulf.cachan-adh.crans.org {
  hardware ethernet 02:65:6C:01:01:01;
  fixed-address 185.230.76.12;
}
```

et de préciser l'option suivante dans le bloc de configuration du sous-réseaux.

```txt
include "/var/local/services/dhcp/generated/dhcp.adh.crans.org.list";
```

Les clients présents dans cette liste seront alors considéré comme connus par
le serveur dhcp. Pour les clients qu'il ne connait pas il peut alors décider de
les accepter ou de les refuser en précisant soit `allow unknown-clients;` soit
`deny unknown-clients;` dans le bloc de sous-réseaux.

L'autre manière de procéder est de lui laisser gérer lui même l'allocation des
adresses ips à utiliser en lui précisant simplement dans quels plages il a le
droit de venir se servir, pour cela on précise l'option suivante dans le bloc :

```txt
pool {
  range 100.65.1.0 100.65.255.254;
}
```

Vu qu'a priori les clients ne sont pas connus, il faut assez fréquemment
préciser en conjoncure de cette option l'option `allow unknown-clients;`.

Il reste ensuite à régler la durée pour laquelle les adresses sont alloués :

```txt
    default-lease-time 600;
    max-lease-time 7200;
```

Ainsi que les options de configuration réseaux a passé au client :

```txt
    option subnet-mask 255.255.0.0; # Précise le sous-réseaux dans lequel l'adresse assignée se trouve
    option broadcast-address 100.64.255.255; # Précise l'adresse de broadcast que le client doit configurer
    option routers 100.64.0.99; # Précise l'adresse de la passerelle
    option domain-name-servers 100.64.0.101,100.64.0.102; # Précise l'adresse des serveurs de noms
    option domain-name "adh-nat.crans.org"; # Précise le nom de domaine de l'hote
    option domain-search "adh-nat.crans.org"; # Précise la liste de recherche par défaut pour les fqdn
```

### /etc/default/isc-dhcp-server

Ce fichier contrôle les options utilisées pour lancer le daemon
isc-dhcp-server. Dans notre cas on se contente de préciser sur quels interfaces
le serveur devrait écouter en ipv4, mais il est possible de préciser d'autres
options.

```txt
INTERFACESv4="ens22 ens23 ens1 enp1s3"
```

Attention pour que le serveur démarre effectivement, il faut que interfaces
listées ici soit bien démarrées et dispose d'une ipv4, sinon celui-ci refusera
de se lancer.
