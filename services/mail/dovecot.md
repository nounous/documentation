# Dovecot

Dovecot est entre autre une implémentation de serveur IMAP, de serveur POP3 et
de serveur d'authentification SASL. Les deux premiers protocoles permettent à
un utilisateur de consulter ces mails (soit en les consultant sur le serveur
pour IMAP, soit en les téléchargeant localement pour POP3) tandis que le
dernier sert à authentifié l'utilisateur, authentification qui pourra être
utilisé par d'autre logiciels comme par exemple postfix pour vérifier
l'idendité d'une  personne souhaitant envoyer un mail.

## Installation

On tire les différents composants logiciels des repository debian :
`sudo apt install dovecot-imapd dovecot-ldap dovecot-pop3d dovecot-sieve`.

## Configuration

La configuration initiale packagée par debian est morcellée en une série de
fichier commentée dans `/etc/dovecot/conf.d` qui permette de segmenter la
configuration en fonction des différents mécanismes du logiciel.

### 10-auth.conf

Ce fichier contrôle l'authentification des utilisateurs au serveur. On
configure dans le fichier directement les options de connexions puis en
incluant des fichiers `auth-{{ mecanisme }}.conf.ext` on précise les
différents bas d'utilisateurs et de mot de passe à utiliser.

```txt
# On autorise les clients à utiliser une authentification en claire car on
# configurera par la suite le fait que tous les échanges entre le client
# et le serveur soient chiffrées par TLS.
disable_plaintext_auth = no
# On configure deux méthodes d'authentification pour le client plain et login.
auth_mechanisms = plain login
# On souhaite tirer les utilisateurs du ldap. On inclue donc le fichier de
# backend ldap qui va définir les bases d'utilisateurs et de mot de passes.
!include auth-ldap.conf.ext
```

Le fichier `auth-ldap.conf.ext` va lui même inclure le fichier de configuration
de connexion au ldap.

```txt
# Addresse du serveur ldap
uris = ldap://172.16.10.157/
# Nom d'utilisateur et mot de passe utilisés pour s'authentifer auprès du
# serveurs
dn = cn=Utilisateurs,dc=crans,dc=org
dnpass = "erdnaxe_aime_debian"
# Base de noms en dessous de laquelle se trouve les utilisateurs dans le ldap
base = cn=Utilisateurs,dc=crans,dc=org
# Les options de configurations user_filter et pass_filter permettent de filtrer
# les ojets ldap à inclure dans les bases d'utilisateurs et de mot de passes.
user_filter = (&(objectClass=posixAccount)(uid=%u))
pass_filter = (&(objectClass=posixAccount)(uid=%u))
# Les options de configurations user_attrs et pass_attrs permettent de savoir
# quels champs récupérer de l'objet ldap et d'altérer certains pour qu'ils
# correspondent mieux à l'installation de la machine.
user_attrs = homeDirectory=home={{ dovecot.home_path }}/%u,uidNumber=uid,gidNumber=gid
pass_attrs = uid=user,userPassword=password
```

### 10-logging.conf

C'est le fichier de configuration qui gère l'écriture des logs du daemon. Il
permet de séléctionner quels événèments doivent être loggé et quels champs de
l'objet doivent être inclus dans les logs.

```txt
plugin {
  mail_log_events = delete undelete expunge copy mailbox_delete mailbox_renam
  mail_log_fields = uid box msgid size
}
log_timestamp = "%Y-%m-%d %H:%M:%S "
```

### 10-mail.conf

Dans ce fichier de configuration on définit les boites mails avec lesquels
dovecot va travailler.

```txt
# Emplacement des boites mails et des indexes de dovecot
mail_location = maildir:~/Mail:INBOX=/var/mail/%u/:INDEX=/var/dovecot-indexes/%u
# Structure initiale à donner à une boite mail
namespace inbox {
  inbox = yes
}
# Nom du groupe gérant les boites mails
mail_privileged_group = mail
# Listes de plugins génériques à charger. On inclue mail_plugins pour ajouter
# des plugins dans la liste et pas écraser le contenu actuel de la variable. Le
# plugin notify est une dépendance du plugin mail_log qui a été configuré dans
# le fichier 10-logging.conf
mail_plugins = $mail_plugins mail_log notify
# Limite le nombre de connections qu'un utilisateur peut réaliser au serveur
mail_max_userip_connections = 15
```

TODO: documenter ce que font mail_log et notify; documenter la dernière ligne
de la conf.

### 10-master.conf

C'est le fichier de configuration principal de dovecot. C'est ici qu'on va
définir les différents services que proposeront notre installation. Au crans
il s'agit d'un serveur IMAP(s), d'un serveur POP3(s) et d'un serveur SASL.

#### IMAP

La configuration du service se décompose en deux partie : le service de
connexion et d'authentification au daemon imap et la configuration imap.

```txt
service imap-login {
# Notre service écoute en clair sur le port 143 seulement sur le réseaux
# d'administration (pour les webmails) et il écoute en chiffré sur le 993 pour
# le reste du monde.
  inet_listener imap {
    address = 127.0.0.1, [::1], 172.16.10.126, [fd00::10:0:ff:fe01:2610]
    port = 143
  }
  inet_listener imaps {
    address = *, [::]
    port = 993
    ssl = yes
  }
  service_count = 0
  process_min_avail = 6
}
service imap {
  process_limit = 16384
}

```

#### POP3

Le service pop3 se configure de manière analogue.

```txt
service pop3-login {
  inet_listener pop3 {
    address = 127.0.0.1, [::1], 172.16.10.126, [fd00::10:0:ff:fe01:2610]
    port = 110
  }
  inet_listener pop3s {
    address = *, [::]
    port = 995
    ssl = yes
  }
  process_min_avail = 6
  service_count = 0
}
service pop3 {
  process_limit = 16384
}
```

#### SASL

Pour le service d'authentification SASL, on souhaite qu'il ne soit accessible
qu'en local et en clair. On ne définit donc qu'un seul bloc d'écoute en local
sur le port `4242` (il n'y a pas de port réservé par l'IANA pour le
protocole).

```txt
service auth {
  client_limit = 1024
  inet_listener {
     address = 127.0.0.1, [::1], {{ dovecot.inet_listener }}
     port = 4242
  }
}
```

### 10-ssl.conf

Dans ce fichier on se contente de dire à dovecot où trouver nos certificats
pour les connextions chiffrées :

```txt
ssl_cert = </etc/letsencrypt/live/crans.org/fullchain.pem
ssl_key = /etc/letsencrypt/live/crans.org/privkey.pem
```
