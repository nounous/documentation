# Services Mail

Le script de services mails permet de générer des fichiers d'alias et virtual
pour [/critical/mail/postfix](postfix) à partir de la base de données des
adhérents du crans. Actuellement celle ci est gérée par re2o donc on fait des
requêtes à l'API de re2o pour récupérer la liste des adresses, alias et
redirections mails.

## Installation

Pour installer le logiciel, il faut cloner le répertoire git
`https://gitlab.crans.org/nounous/mail.git` . Actuellement la convention veut
que les services soient placé sous le chemin `/var/local/services/` mais rien ne
l'impose. Pour tourner  le script a besoin du paquet `python3-jinja2`. Il faut
aussi crééer le dossier `generated` à la racine du dépot dans lequel seront
stocker les fichiers de bails générer. Il est aussi nécessaire de récupérer les
fichier `aliases` et `virtual` du dépot git
`https://gitlab.crans.org/nounous/mail_aliases.git` et de venir les mettre à la
racine du dépot sous les noms `aliases_local` et `virtual_local`.

## Configuration

### Re2o

Pour récupérer la liste des adresses mails, le script a besoin de pouvoir parler
à l'API re2o. Pour ça on fournit au script les informations de connexions dans le
fichier de configuration `re2o-config.ini` à la racine du dépot :

```txt
[Re2o]
hostname = 172.16.10.156
username = services
password = ynerant_aime_le_php
```

### mail.json

Dans le fichier de configuration `mail.json` aussi présent à la racine, on
réalise le reste de la configuration du logiciel. Actuellement le script ne
prends pas d'options, on peut donc se contenter de lui passer le dictionnaire
vide en guise de configuration.

```json
{}
```

## Conformation avec les normes email

Les milters (filtres mails) suivants sont déployés sur le serveur mail :

* openDKIM qui appose une en-tête de signature DKIM dans les mails émis.

* openDMARC qui vérifie le passage des tests SPF et DKIM des mails entrants. Un
  fichier d'historique (`/var/lib/opendmarc/opendmarc.history`) est complété à
  chaque mail vérifié. Un script permet de remplir une base de donnée à partir
  de l'historique et un autre d'envoyer des rapports aux émetteurs des mails
  traités.
