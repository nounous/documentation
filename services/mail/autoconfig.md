# Autoconfig

Dans le protocole mail, la configuration du client mail est généralement laissé
à la charge du client. C'est à lui de savoir à quels serveurs il souhaite se
connecter, sur quels ports et en précisant le mécanisme ainsi que les
informations d'authentification. Cependant ces informations n'étant pas
toujours facilement accessible à l'utilisateur, thunderbird a mis en place une
manière externe par laquelle le client peut récupérer toutes les informations
nécessaires à sa configuration de tels sortes que l'utilisateur n'ait qu'à
fournir son adresse mail et son mot de passe.

## Principe

Attention, ce mécanisme de création de compte n'est pour le moment normalisé
dans aucune RFC, c'est le client mail qui choisit sa propre implementation du
mécanisme. Il est donc complètement possible que

1. le client n'implémente pas du tout le mécanisme ou qu'il n'en implémente
   qu'une partie

1. deux clients différents n'aient pas la même implémentation du mécanisme

Quand on lui fournit une adresse mail de la forme `utilisateur@domain.tld`, le
client va essayer de récupérer le contenu de l'uri suivante
`http://autoconfig.domain.tld/mail/config-v1.1.xml` qui est un fichier xml
assez simple qui décrit en quelques lignes une configuration que le client
peut adopter. Une référence complète de sa syntaxe est trouvable à
l'adresse suivante :
<https://wiki.mozilla.org/Thunderbird:Autoconfiguration:ConfigFileFormat>.
Voilà une configuration minimale dans le cas du crans :

```xml
<clientConfig version="1.0">
  <emailProvider id="crans.org">
    <domain>crans.org</domain>
    <displayName>Mail crans</displayName>
    <displayShortName>crans</displayShortName>
    <incomingServer type="smtp">
      <hostname>redisdead.crans.org</hostname>
      <port>465</port>
      <socketType>SSL</socketType>
      <username>%EMAILLOCALPART%</username>
      <authentication>plain</authentication>
    </incomingServer>
    <outgoingServer type="imap">
      <hostname>owl.crans.org</hostname>
      <port>993</port>
      <socketType>STARTTLS</socketType>
      <username>%EMAILLOCALPART%</username>
      <authentication>plain</authentication>
    </outgoingServer>
  </emailProvider>
</clientConfig>
```

Le fichier de configuration se lit assez bien, on dit à l'utilisateur de faire

* du smtp pour envoyer des mails en se connectant avec une connexion chiffrée
  à `redisdead.crans.org` sur le port `465` et en utilisant la partie locale
  de son adresse mail (le `user` dans `user@domain.tld`) comme nom
  d'utilisateur et en utilisant le mode d'authentification ~~plain~~ LOGIN
  (voir la suite)

* du imap pour recevoir les mails en se connectant avec une connexion STARTTLS
  à `owl.crans.org` sur le port `993` et en utilisant la partie locale de son
  adresse mail comme nom d'utilisateur et en utilisant le mode
  d'authentification ~~plain~~ LOGIN

La plupart des clients mails interprète l'utilisation du mot clé `plain`
comme étant le méchanisme `login` du protocole SASL et non le méchanisme
`plain` de ce même protocole. Au crans, les deux méthodes d'authentification
étant supporté, cela ne pose pas de soucis.
