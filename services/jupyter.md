# Jupyter notebook, édition et exécution de code python à distance.

<https://jupyter.org/>

## Utilisation par un.e adhérent.e sur zamok

On commence par créer le dossier "racine" de nos notebooks, normalement
jupyter ne pourra pas accéder aux fichiers en dehors de ce dossier.

```bash
mkdir jupyter
cd jupyter
```

On va ensuite créer en environement virtuel python dans lequel il faut
installer le package `notebook` ainsi que tous les packages nécessaires à
l'exécution de notre code (par exemple `pandas`, `numpy` ou `matplotlib`).

```bash
python3 -m venv env
source env/bin/activate
pip install notebook
```

On peut ensuite lancer le serveur jupyter notebook avec: `jupyter notebook`.
Quelques lignes de logs devraient apparaitre dont:

```txt
To access the server, open this file in a browser:
    file:///home/paulon/.local/share/jupyter/runtime/jpserver-3662553-open.html
Or copy and paste one of these URLs:
    http://localhost:8888/tree?token=67c9b4d3cdf0fc24dc27d9cd8d3ffa1cc52a2bf7112dd7a5
    http://127.0.0.1:8888/tree?token=67c9b4d3cdf0fc24dc27d9cd8d3ffa1cc52a2bf7112dd7a5
```

On peut donc maintenant faire du port forwarding avec ssh pour accéder depuis
sa machine au port utilisé par jupyter sur zamok :

```bash
ssh -L <port local>:localhost:<port dans l'url de jupyter> zamok.crans.org
```

On va partir du principe qu'on utilise le même port dans les deux cas, ici
`8888` en faisant donc `ssh -L 8888:localhost:8888 zamok.crans.org`.

On peut alors juste copier coller l'adresse dans notre navigateur :
<http://localhost:8888/tree?token=67c9b4d3cdf0fc24dc27d9cd8d3ffa1cc52a2bf7112dd7a5>
et voilà!
