# Linx

Linx est un logiciel d'hébèrgement temporaire et de partages de fichiers.

## Installation

Pour installer le logiciel, on commence par installer go sur le serveur,
`apt install golang`. Puis on télécharge l'une des
[https://github.com/andreimarcu/linx-server/releases](releases) du logiciel.
On peut enfin installer le logiciel en faisant `go install` sur la le fichier
téléchargé.

## Configuration

La configuration du logiciel est relativement peut et il est laissé à
l'utilisateur le choix de où il souhaite la placer. On propose ici de la
mettre dans `/etc/linx/server.conf`. Dans le fichier, on va préciser, l'adresse
du serveur, le port et l'ip à laquelle il va s'attacher, la taille maximale des
fichiers, leur temps de rétention et leur emplacements sur le disque.

```txt
bind = 172.16.10.119:8080
sitename = CRANS Linx
siteurl = https://linx.crans.org/
maxsize = 10000000
maxexpiry = 604800
filespath = /var/lib/linx/files/
metapath = /var/lib/linx/meta/
```

Pour vérifier que cela fonctionne, on peut maintenant lancé le logiciel en
faisant `linx-server -config /etc/systemd/system/linx-server.conf`.

## Service systemd

Pour que le logiciel puisse se lancer automatiquement au démarrage du serveur,
on va placer un fichier d'unit systemd dans
`/etc/systemd/system/linx-server.service`.

```txt
[Unit]
Description=Linx
After=network.target

[Service]
Type=simple
User=linx
Group=linx
WorkingDirectory=/var/lib/linx/
ExecStart=/usr/local/sbin/linx-server -config /etc/linx/server.conf
Restart=always

[Install]
WantedBy=multi-user.target
```
