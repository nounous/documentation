# Galène, visio-conférence

Galène est un logiciel de visio-conférence écrit en Golang (pure) sous
licence MIT. Il s'appuie sur le standard Web W3C WebRTC afin de connecter
plusieurs navigateurs Web ensembles pour réaliser des visio-conférences.

Site officiel : <https://galene.org/>

Il est trivial à installer sur tous les systèmes d'exploitation en suivant le
fichier `INSTALL` du dépôt de code. Il suffit d'avoir Golang et d'ouvrir le
parefeu correctement.

## Acronymes

L'audio-visuel est un domaine remplie d'acronymes. Pour le bien être des
futurs responsables techniques qui s'arracheront les cheveux en essayant de
debugguer Galène, voici une liste partielle d'acronymes :

* **WebRTC** : Web Real Time Communication. Protocole permettant aux
  navigateurs Web de gérer des flux UDP. Utilisé par exemple par WebTorrent
  pour réaliser du Torrent, ou par toutes les applications classiques de
  visio-conférences. Le chiffrement est forcé dans le standard.

* **HLS** : HTTP Live Streaming. Le principe consiste à découper un flux
  vidéo en segments et d'exposer ces segments sur un serveur HTTP. En 2021, le
  HLS est utilisé par PeerTube (sur du WebTorrent), Twitch et YouTube.

* **SRT** : ce n'est pas le standard de sous-titre `.srt`, mais un protocole
  libre qui a pour but de remplacer `RTMP` (qui est un stream de fichier FLV,
  héritage de Flash Player).

* **RTP** : Real Time Protocol, standard audio-visuel initialement pour le
  téléphone SIP, VoIP, VoLTE (appels sur 4G) et la Télévision Digitale (IPTV).

* **DTLS-SRTP** : Secure Real Time Protocol avec un chiffrement standardisé.

* **SFU** : Selective Forwarding Unit. Galène est un SFU, il reçoit des
  flux de plusieurs navigateurs Web et les redistribue selon des salons et
  permissions.

* **Pion** : la librarie Golang pour utiliser WebRTC. Utilisé par Galène.

* **TURN** : Traversal Using Relays around NAT. Serveur intermédiaire
  (proxy) pour traverser un parefeu trop limitant.

* **STUN** : Session Traversal Utilities for NAT. Serveur permettant de
  réfléchir les adresse IP publiques d'un utilisateur et de taper sur son
  parefeu de l'extérieur. Un TURN contient un STUN.

* **ICE** : Interactive Connectivity Establishment. Procédure pour trouer
  des parefeux (en utilisant des STUN et TURN) et ouvrir des flux UDP entre des
  machines distantes.

* **VP8/VP9/AV1** : Codecs vidéos royalty-free développés par Google. Ils
  sont intégrés dans le code source de Chromium et Firefox. AV1 est le
  successeur de VP9 qui est le successeur de VP8. AV1 est un codec vidéo
  adaptatif.

* **Opus** : Codec audio libre intégré dans le code source des navigateurs
  Web. Il est très performant pour coder de la musique ou de la voix. Il hérite
  de SILK qui est le codec historique de Skype.

* **H264/H265** : Codecs non royalty-free (bouh!) poussés par la fondation
  MPEG et Apple. Les vielles versions de Safari ne supportent que ces codecs.

## How to debug

Le mieux est d'utiliser Firefox pour débugger le WebRTC car leur interface est
plus intuitive. Sur Firefox vous pouvez ouvrir `about:webrtc` et sur Chromium
`chrome://webrtc-internals`.

Souvent ce qui casse c'est l'ICE et la connexion met soit beaucoup de temps à
s'établir, ou ne s'établit pas. Le tableau décrivant l'ICE dans Firefox est
utile car il donne les raisons des fails.

## Faire gaffe à la sécurité

Un TURN est un proxy. Il faut donc s'assurer qu'il ne permet pas d'accéder aux
réseaux d'administration à partir de l'extérieur.

Voir
<https://www.rtcsec.com/article/slack-webrtc-turn-compromise-and-bug-bounty/>.
