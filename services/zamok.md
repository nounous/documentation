# zamok

`zamok` est le serveur des adhérents du Crans.

## Mais qui es-tu, zamok ?

De tout temps, l'homme a cherché à répondre à la question de sa genèse.
Cette page n'aidera en aucun façon. Zamok, en Nouveau Criméen, ça veut dire
château. Il paraît aussi que ça veut dire « Clef », mais j'ai pas trouvé
dans quelle langue.

En tout cas, Zamok est la fidèle bécane (qui a changé cinq fois de boîte)
qui permet aux adhérents geeks de se connecter à une machine tout le temps
allumée pour discuter sur IRC et faire tourner à la sauvage ses algorithmes.

Zamok héberge aussi les créations web des adhérents ou des clubs.

Ah, et les mails sont triés dessus.

## Les services

* `ssh` permet d'obtenir un shell sur zamok, on peut alors avoir accès à
  différents services par ce biais (liste non exhaustive) :
   * lecture de mails avec `mutt`
   * utilisation de `latex`
   * utilisation d'`emacs` et `gnus`
   * conversion d'images avec `convert`
   * différents shells Unix (dont `zsh`, `bash`, `tclsh`, `ksh`, `sh`)
* Un serveur web qui permet l'accès aux pages personnelles des adhérents.
* Un serveur MySQL pour les pages personnelles des adhérents
* C'est le serveur sur lequel les mails des comptes Cr@ns sont délivrés
  (procmail, spam assassin, etc)
