# Re2o

Re2o est un système de gestion d'association réseau étudiante écrit en
[Django](https://www.djangoproject.com/) un framework
[Python](https://www.python.org/) permettant de développer facilement des
sites webs interagissant avec une base de données ses sources sont disponibles
sur le [GitLab FedeRez](https://gitlab.federez.net/re2o/re2o).

Re2o permet de gérer la base de données des adhérents et de leur machine
ainsi que quelques autres objets tels que les définitions des zones
[DNS](/tools/dns.md).

Il est déployé sur la VM `re2o` et est joignable en [HTTP](tools/http.md)(S)
sur `intranet.crans.org`.
