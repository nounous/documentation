# NinjaBot

NinjaBot est un bot [IRC](/tools/irc.md) qui permet d'envoyer des notifications
en temps réel sur des canaux de discussion, ses sources sont disponibles
[ici](https://gitlab.crans.org/nounous/NinjaBot).

Au Crans il est utilisé pour retransmettre les alertes du monitoring sur
`#monitoring` et possède le nick `monitoring`.

NinjaBot utilise [Flask](https://flask.palletsprojects.com/) afin d'écouter
des webhooks et de pousser des notifications en fonction des données envoyées
par le webhook. Ces notification sont ensuite envoyée sur une fifo lue par le
bot IRC en lui-même : il est donc possible de modifier le comportement des
notifications sans redémarrer le bot en lui-même.

NinjaBot est déployé sur la VM `monitoring` et le service systemd qui lui est
associé est `ninjabot`.
