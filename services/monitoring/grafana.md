# Grafana

Grafana est une traceur de données à destination des humains. Il est codé en
NodeJS et correctement packagé dans une source de paquets Debian.

L'idée de Grafana est de se connecter à des sources de données puis de
réaliser des dashboards contenant des modules faisant des requêtes à ces
sources de données.

Là vous vous dites « *Mais du coup il faut passer beaucoup de temps pour
créer les dashboards ? C'est nul !* ». Effectivement comparé à Munin il y a
plus de travail nécessaire pour afficher des données, mais cela permet une
plus grande adaptabilité. Beaucoup de dashboards existent en ligne (surtout
sur les dépôts de code des exporters Prometheus) et sont téléchargeable en
JSON pour ensuite les importer.

## Securité

Par défaut Grafana crée un compte avec pour login `admin` et mot de passe
`admin`. Parfois quand on bidouille la configuration, cet utilisateur est
recréé automatiquement. Pensez à le supprimer s'il apparait dans la liste
des utilisateurs ou changer son mot de passe.

Évitez d'installer des plugins qui ne sont pas de confiance. Le service Systemd
de Grafana le conteneurise pour éviter des potentielles exécutions de code
distantes.
