# Outils

## Présentation

Ce dossier contient une présentation rapide de plusieurs outils logiciels,
systèmes d'exploitation et protocoles utilisés par le Crans pour
l'installation et la maintenance des serveurs et des services qui y sont
associés.

Le but n'est pas de réexpliquer l'entièreté des  fonctionnalités, mais
simplement d'avoir une explication de ce que l'on parle  pour avoir des bases
de compréhension.

## Organisation

Chaque fichier de ce dossier contient une présentation, au moins succinte, de
l'outil ou du protocole. On peut y mettre autant de détails que l'on juge
nécessaire bien évidemment (il vaut mieux trop de documentation que pas
assez), mais ce n'est pas non plus absolument vital. On s'assurera en plus que
chaque fichier contient un lien vers de la documentation officielle (si elle
existe) et/ou vers une explication relativement simple du fonctionnement.

Si l'outil ou le protocole est assez complexe et qu'il n'existe pas de bonne
documentation **simple** en ligne, il est alors vivement recommandé que les
nounous/apprenti⋅es le comprenant bien écrivent une explication de son
fonctionnement.

Les fichiers sont nommés par l'acronyme de l'outil ou du protocole (car on les
connait en général mieux que les noms complets).

On retrouve les dossiers suivants :

* [`logiciels`](logiciels) : contient la documentation des logiciels utilisés
  par le Crans.

* [`protocoles`](protocoles) : contient la documentation des protocoles
  utilisés pas le Crans.

* [`os`](os) : contient la documentation des systèmes d'exploitation
  utilisés par le Crans.
