# Proxmox

[Proxmox VE](https://www.proxmox.com/en/proxmox-ve) est un environnement de
virtualisation basé sur Debian et QEMU/KVM.

Il permet d'administrer des machines virtuelles (VM) et des conteneurs via
une interface Web en HTTPS sur le port 8006 ou via une ligne de commande.

## Ma VM ne s'éteint pas proprement !

Depuis Proxmox 6.0 il se peut que l'interface Web n'arrive pas à éteindre
une machine virtuel en envoyant un signal ACPI<sup>1</sup> à la machine.

<sup>1</sup> Advanced Configuration and Power Interface, permet à l'OS de
communiquer avec le système de gestion d'énergie (bidirectionnel).

La solution est de passer par la ligne de commande avec `qm shutdown ID`.
Une fois cette commande réalisée, il sera possible d'éteindre la VM proprement
depuis l'interface Web.

Même si ce bug peut être frustrant à 6h du matin un jeudi, il veut mieux
toujours laisser les machines proprement synchroniser leurs systèmes de fichier
puis s'éteindre. Un `qm stop` ou un arrêt brutal via l'interface Web causera
des disparitions ou rollback des derniers fichiers écrits (grâce au journal
des systèmes de fichiers Ext4).
