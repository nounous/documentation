# Systèmes d'exploitation

## Présentation

Ce dossier contient une présentation des systèmes d'exploitation utilisés
par le Crans.

Le but n'est pas de réexpliquer l'entièreté des  fonctionnalités, mais
simplement d'avoir une explication de ce que l'on parle  pour avoir des bases
de compréhension.

## Organisation

L'organisation est la même que décrite dans [le fichier `README.md` du
dossier parent](../README.md).
