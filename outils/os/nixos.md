# NixOS

Le Crans utilise [NixOS](https://nixos.org/) sur plusieurs machines virtuelles.

Une documentation complète est disponible au [dépôt git de la configuration
NixOS du Crans](https://gitlab.crans.org/nounous/nixos).
