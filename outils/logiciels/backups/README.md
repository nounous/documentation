# Outils de backups

## Présentation

Ce dossier contient une présentation des logiciels utilisés par le Crans pour
faire ses sauvegardes, ou backups en anglais. On entend ici par backup une
opération qui consiste à dupliquer de manière sécurisée (chiffrée en
l'occurrence) sur d'autres serveurs les données des utilisateurices et des
machines du Crans.

Comme dans tout le dossier [logiciels](../../../), le but n'est pas de
réexpliquer l'entièreté des fonctionnalités mais simplement d'a voir les bases
de compréhension.

## Organisation

Vous trouverez ici deux fichiers correspondant aux deux logiciels utilisés pour
les backups au Crans :

- [borg](./borg.md) pour les logiciels borg et borgmatic ;
- [restic](./restic.md) pour le logiciel du même nom.

Il est cependant fortement conseillé de lire la fin de ce fichier présentant
certaines informations concernant les backups qu'il est important de connaître
avant de lire plus en détails les autres fichiers détaillant plus
spécifiquement les logiciels.

## Contexte

### Historique

Pour prevenir certains incidents dus à des erreurs de manipulation, le collège
technique a mis en place en système de backups. Le but de celui-ci est de
pouvoir, si le besoin s'en fait ressentir, aller rechercher un fichier supprimé
ou écrasé. Bien qu'il serait possible de simplement faire une copie périodique
des données sur un serveur distant, on lui préfère des solutions un peu plus
développées qui permettent de faire des sauvegardes incrémentales. Fut un temps,
la solution technique en place était le logiciel
[BackupPC](https://backuppc.github.io/backuppc/), cependant celui-ci commençant
à dater et ayant une faible ergonomie, nous utilisons aujourd'hui le logiciel
[borg](https://borgbackup.readthedocs.io/) et son frontend
[borgmatic](https://torsion.org/borgmatic/). De plus, le logiciel
[restic](https://restic.readthedocs.io/) a été installé en parallèle pour sa
simplicité d'utilisation : borg/borgmatic et restic cohabitent donc pour le
moment pour que les jeunes membres actif⋅ves puissent comparer afin de faire un
choix à long terme.

### Backups incrémentales

Avant de présenter ces logiciels, attardons nous un peu sur ce qu'on entend par
faire des backups incrémentales. Pour cela revenons à notre piètre solution
initiale qui consiste à copier (à grands coups de rsync, unison, scp...) toutes
les données que l'on souhaiterait sauvegarder. Pour que ces sauvegardes soient
utiles, il faut les réaliser assez fréquemment (quelque part entre un jour et
une semaine disons). Cependant, à chaque nouvelle backup réalisée, celle-ci va
venir écraser le contenu de la précédente et ainsi, on ne dispose jamais plus
que d'une copie des données de la veille. Ainsi, des dysfonctionnements
automatiques ou simplement de la négligence peuvent faire que l'on ne se rendra
compte que l'on avait besoin de récupérer notre fichier il y a déjà quelques
jours et qu'il a maintenant déjà été supprimé de la copie de sauvegarde.

On préfererait donc pouvoir disposer d'une collection de sauvegardes qui
remonte sur quelques jours voir moi en fonction de la politique de rétention de
sauvegarde qu'on mettrait en place. Par exemple, une sauvegarde pour chacun des
septs derniers jours puis quelques unes espacée d'une semaine. Si l'on reprend
notre solution naïve, on peut facilement en étendre l'usage pour ce nouvel
office : on réalise une copie complète des données par jour que l'on souhaite
sauvegarder. Suivant la politique de rétention que l'on a donné plus tôt cela
consisterais en une quinzaine de copies distinctes des données à sauvegarder et
donc une multiplication par quinze de la taille de celle-ci.

Si je jette un œil rapide à la taille que prends aujourd'hui les données
utilisateurices, je vois qu'on utilise un peu plus de 2.5T de stockage, donc si
l'on suit la logique, on devrait disposer d'une ou plusieurs machines
totalisant un stockage d'environ 40T. Inutile de le dire je pense, mais cela
fait (beaucoup) trop. Heureusement pour nous, d'un jour sur l'autre
l'intégralité de nos données stockées ne varie pas et seule une petite partie
change sur le cours d'une journée. Ainsi, si l'on ne stocke que les différences
accumulé sur une journée par rapport à la veille, on se retrouvera avec une
taille beaucoup plus raisonnable de sauvegarde. C'est ~la déduplication.

### Qu'est-ce que l'on sauvegarde ?

Au Crans, on sauvegarde deux types de données :

- D'un côté, on a les données d'administration, c'est une partie des disques
  de tous les serveurs (généralement le /etc et le /var sauf cas particulier),
  le home des nounous, les bases de données...

- De l'autre côté; on a les données des adhérents, c'est à dire : le contenu
  de leur répertoire personnel et de leur dossier mail qui au jour où j'écris
  ses lignes peut contenir au maximum 30G et 10G de données respectivement.
