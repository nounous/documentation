# Borg

Page officiel de borg : <https://www.borgbackup.org/>.

Documentation de borg : <https://borgbackup.readthedocs.io/>.

Page officielle de borgmatic : <https://torsion.org/borgmatic/> (contient la
documentation).

## Borg et borgmatic

Borg est un logiciel client/serveur permettant d'effectuer des sauvegardes
incrémentales, dédupliquées et chiffrées par le client. Le serveur est simple à
installer et utilise ssh comme protocole de transport entre lui et le client.
Ainsi, il suffit de donner un accès ssh au client via une paire de clés pour lui
autoriser à discuter avec le serveur. Quelque chose ressemblant à ça dans le
`.ssh/authorized_keys` de l'utilisateur sur le serveur qui fera les backups :

```txt
command="borg serve --restrict-to-path {{ chemin vers les sauvegardes }}",restrict {{ clé publique ssh }}
```

Avec l'installation du paquet `borg` c'est la seule chose à faire sur le serveur
et le reste du travail sera réalisé par le client. Après avoir aussi installé
`borg` sur le client, on peut déjà commencer à faire quelques sauvegardes.
Cependant, si l'on utilise que borg pour les faire (ce qui est possible), on se
retrouvera avec des commandes bien trop longues dans lesquels on passe notre
temps à spécifier des arguments plusieurs fois : comment contacter le serveur ?
où stocker les sauvegardes ? quel politique de rétentions garder ? que faut-il
sauvegarder ? Borgmatic est un frontend de borg qui définir une bonne fois pour
toutes ses options dans un fichier de configuration pour ensuite n'avoir plus
qu'à faire des appels simples à borgmatic qui pasera à borg tous les paramètres
dont il a besoin. Le but n'est pas ici d'être exhaustif sur ce que peut/doit
contenir ce fichier de configuration. Je vous redirige pour ça vers la
[documentation
officielle](https://torsion.org/borgmatic/docs/reference/configuration/). Ici,
je vais plus chercher à vous donner quelques options minimales et des exemples
d'utilisations de borgmatic. Commençons par donner le contenu d'un fichier de
configuration partiel :

```txt
location:
    source_directories:
        - /etc
        - /var
    repositories:
        - ssh://borg@backup-server:/folder/to/backup/to

storage:
    encryption_passphrase: PASSPHRASE
    ssh_command: ssh -i /etc/borgmatic/id_ed25519_borg

retention:
    keep_daily: 4
    keep_monthly: 6
```

Dans la section `location`, on définit deux choses : ce que l'on souhaite
sauvegarder et où on souhaite le sauvegarder. Ici on va backupé le `/etc` et
`/var` de notre client sur un serveur distant qu'on contactera via ssh. Dans la
section suivante, `storage`, on définit quelques options relatives au stockage
et à comment y accéder. Premièrement, on donne la passphrase de chiffrement des
sauvegardes (attention, si elle est perdue, les sauvegardes ne seront plus
accessibles). Ensuite, on précise comment accéder au serveur (ici, l'information
importante est où se trouve la clé privée pour pouvoir se connecter). Enfin,
dans la section retention, on définit notre politique de retention : le serveur
ne doit stocker qu'une backup quotidienne sur les 4 derniers jours, et une
backup mensuelle sur les 6 derniers mois.

En supposant les autres options de fonctionnement renseignées dans le fichier,
que je ne détaillerais pas ici, il est maintenant possible de commencer à
utiliser le logiciel et faire nos premières sauvegardes.

## Créer l'archive de stockage

La commande suivante permet de créer l'archive où seront stockées les
sauvegardes sur le serveur.

```bash
borgmatic init -e repokey
```

## Faire une première sauvegarde

Pour faire une sauvegarde, rien de plus simple, on appelle simplement le
programme borgmatic sans option. Il est néanmoins possible de demander un
affichage plus verbeux pour le débogage.

```bash
borgmatic
borgmatic -v 2
```

## Lister les différentes sauvegardes dans l'archive

```bash
borgmatic list
```

## Monter une sauvegarde précise sur un point de montage

```bash
borgmatic mount --archive {{ nom de l'archive ou latest pour la dernière }} --mount-point /mnt --path {{ chemin à monter de l'archive }}
borgmatic mount --archive latest --mount-point /mnt --path etc/nginx/nginx.conf
```

## Automatiser les backups

Pour réaliser des backups périodique, on peut en laisser la charge à un cron ou
à systemd à l'aide d'un timer.
