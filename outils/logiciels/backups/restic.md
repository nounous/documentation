# Restic

Page officielle de restic : <https://restic.net/>.

Documentation de restic : <https://restic.readthedocs.io/>.

Dépôt officiel de restic rest-server : <https://github.com/restic/rest-server>.

## Présentation

Restic est un logiciel client/serveur permettant d'effectuer des sauvegardes
incrémentales, dédupliquées et chiffrées par le client. Différents serveurs
sont utilisables par le client, nous présenterons ici le serveur REST de restic
utilisant le protocole HTTP, qui est installé actuellement au Crans sur
[thot](../../../infrastructure/machines/physiques/thot.md).

Pour la configuration d'un client, vous aurez besoin d'installer le paquet
`restic` sur les machines sous debian, restic étant déjà packagé dans nixpkgs
pour les machines sous NixOS.

## Configuration du serveur

Le choix le plus simple à mettre en place pour la partie serveur de restic est
restic rest-server, très simple à mettre en place. Vous pouvez trouver la
configuration actuelle dans le module restic du
[dépôt NixOS](https://gitlab.crans.org/nounous/nixos).

Le fonctionnement de ce serveur REST est le suivant : le client compresse et
chiffre ses données avec un mot de passe, vient s'authentifier en
[basic auth](https://en.wikipedia.org/wiki/Basic_access_authentication) auprès
du serveur, puis dépose la backup au fur et à mesure de son avancée sur le
serveur. Chaque client dispose donc de deux mots de passe : une clef de
chiffrement pour les données, ainsi qu'un mot de passe pour s'authentifier
auprès du serveur REST.

Deux notions sont importantes à comprendre ici dans la configuration du serveur
REST.

- L'option `privateRepos`/`--private-repos` : permet de faire en sorte qu'un⋅e
  utilisateurice (dans notre cas une machine) ne puisse accéder qu'à ses
  propres backups et puisse créer tous les dépôts souhaités dans un sous
  dossier à son nom. Ainsi, une fois authentifiée, la VM flirt aura accès à
  tous dépôt de la forme `restic:http://thot.adm.crans.org/flirt/<dépôt>`, ce
  qui est particulièrement pratique pour ne pas avoir à gérer les accès à la
  main pour chaque machine.
- Les clients sont authentifiées par le serveur grâce à un fichier `.htpasswd`
  situé à la racine du dossier de données. Ainsi, si le dossier de données est
  `/backups`, le fichier d'authentification sera `/backups/.htpasswd`. Ce
  fichier est un fichier d'authentification standard : vous pouvez consulter la
  [page Wikipédia](https://en.wikipedia.org/wiki/.htpasswd) dédiée pour plus
  d'informations. Vous pouvez alors ajouter une machine à ce fichier avec la
  commande :

  ```bash
  sudo htpasswd -B /backups/.htpasswd <username> [<password>]
  ```

  Si le mot de passe n'est pas spécifié, il sera alors demandé sur l'entrée
  standard.

## Configuration du client

Théoriquement, restic est uniquement un logiciel qui s'utilise directement.
Cependant, nous souhaitons faire des sauvegardes automatiquement en utilisant
des systemd timer, on va donc créer plusieurs fichiers pour ne pas à avoir tout
à spécifier directement dans les lignes de commande.

Actuellement, la configuration des clients restic se situe dans le dossier
`/etc/restic` sous debian, et dans le
[dépôt NixOS](https://gitlab.crans.org/nounous/nixos) pour les machines sous
NixOS. Dans les deux cas, on retrouve les mêmes éléments (on donne les noms
pour la configuration par défault des machines sous debian) :

- un fichier d'environnement `base.env`, contenant plusieurs variables d'environnement
  dont l'utilité précise peut être trouvée dans
  [la documentation de restic](https://restic.readthedocs.io/en/stable/040_backup.html#environment-variables).
- un fichier `base-repo` contenant la position du dépôt (serveur REST), avec
  les identifiants de connexion, qui se présente sous la forme
  `restic:http://<username>:<password>@thot.adm.crans.org/<username>/base`.
- un fichier `base-password` contenant le mot de passe de chiffrement des
  sauvegardes.
- deux fichiers `base-includes` et `base-excludes` indiquant respectivement les
  dossiers inclus et exclus pour les sauvegardes.

En plus de cela, on trouve un service systemd `restic-base` ainsi qu'un timer
associé dans le dossier `/etc/systemd/system/`. Ce service lancera deux
commandes : une commande pour lancer une sauvegarde, et une commande pour
supprimer les anciennes sauvegardes qui ne seront pas gardées en tant que
sauvegarde journalière/hebdomadaire/mensuelle/annuelle (définies par la
configuration).

## Initialiser un dépôt

En supposant que vous avez la configuration décrite ci-dessus, vous pouvez
initialiser un dépôt grâce à la commande suivante :

```bash
restic init --repository-file /etc/restic/data-repo --password-file /etc/restic/data-password
```

## Faire une sauvegarde à la main

Pour faire une sauvegarde à la main, rien de compliqué, il suffit de lancer le
service systemd normalement de la manière suivante :

```bash
sudo systemctl start restic-base.service
```

(Même si c'est surprenant, vous pouvez faire CTRL+C à ce moment dans votre
terminal, la tâche continuera en fond.)

De plus, si la variable `RESTIC_PROGRESS_FPS` a été affectée à une valeur
strictement positive, vous pourrez voir la progression de la sauvegarde dans
le journal. Vous pouvez donc y accéder par la commande :

```bash
sudo journalctl -xefu restic-base
```

## Consulter l'état des sauvegardes

Pour consulter la liste actuelle des sauvegardes, vous pouvez simplement
utiliser la commande :

```bash
restic snapshots --repository-file /etc/restic/data-repo --password-file /etc/restic/data-password
```

De plus, vous pouvez facilement monter une sauvegarde sur un point de montage
avec la commande suivante :

```bash
restic mount /mnt --repository-file /etc/restic/data-repo --password-file /etc/restic/data-password
```

Cela bloquera alors votre terminal, et la sauvegarde sera démontée dès que vous
quitterez votre terminal, évitant d'avoir une sauvegarde montée et oubliée.
