# APT

APT (pour Advanced Package Tool) est un des gestionnaires de paquets de
[Debian](https://www.debian.org/). Il dispose d'une interface en CLI, voici
quelques exemples de commandes :

| commande | description |
|----------|-------------|
| `apt install package` | Installe `package` |
| `apt search package` | Recherche `package` |
| `apt list` | Liste les paquets |
| `apt list --installed` | Liste les paquets installés |
| `apt remove package` | Désinstalle `package`, sans supprimer sa configuration |
| `apt purge package` | Purge `package`, i.e. désinstalle et supprime sa configuration |
| `apt depends package` | Liste les dépendances de `package` |
| `apt rdepends package` | Liste les paquets dépendants de `package` |
| `apt policy package` | Affiche les informations de priorité pour `package` |
| `apt show package` | Affiche les informations de `package` |
| `apt update` | Met à jour les listes de paquets |
| `apt upgrade` | Met à jour les paquets |

Un paquet Debian va placer des fichiers dans `/bin/`, `/usr/` par exemple. Il
faut donc avoir les droits administrateurs pour installer, mettre à jour ou
supprimer un paquet. Il n'est pas possible d'installer un paquet APT pour
seulement un utilisateur.

## Les sources de paquets

APT est en réalité une interface au dessus de DPKG (Debian PacKaGe) qui va
simplifier le téléchargement et l'installation de paquets en les
synchronisant de sources de paquets.

Les sources de paquets sont définies dans `/etc/apt/source.list` et dans le
dossier `/etc/apt/source.list.d/`. Un système Debian classique au Crans
devrait avoir les sources suivantes :

```txt
# Paquets principaux de Debian Buster
deb https://ftps.crans.org/debian buster main

# Paquets "volatiles" pour avoir des mises à jour un peu plus régulièrement
deb https://ftps.crans.org/debian buster-updates main

# Paquets apportant les mises à jour de sécurité
deb https://ftps.crans.org/debian-security buster/updates main
```

Si on souhaite également synchroniser la liste des sources de paquets pour
potentiellement les reconstruire, on peut dupliquer chaque ligne en substituant
`deb` par `deb-src`.

## Vérifier l'état de santé des paquets systèmes Debian

Debian fournit un outil nommé `debsums` (Debian Checksum) pour vérifier que
les fichiers installés par les paquets Debian n'ont pas été altérés. Un
fichier d'un paquet peut être altéré soit par un virus ou un administrateur
chaotique qui écrase ses exécutables, soit quand il s'agit de changer sa
configuration.

Pour lister rapidement la liste des fichiers de configuration changés :
`debsums -ce`.
