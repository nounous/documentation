# Ansible

Ansible est un moyen de créer des configurations de serveur et de les déployer.
Il permet ainsi l'accélération du déploiement de certains services et de
permettre de les réinstaller en cas de problèmes.

Cette documentation n'étant pas exhaustive, il sera nécessaire de faire des
tours dans les dossiers d'ansible et dans la
[documentation officielle](https://docs.ansible.com/ansible/latest/index.html)
d'ansible.

## Prérequis

La configuration d'ansible du crans nécessite un certain nombre de prérequis.

### Installation d'Ansible

Pour utiliser l'[Ansible du crans](https://gitlab.crans.org/nounous/ansible),
il faut installer Ansible sur votre machine personnelle. Il est possible de le
faire avec la commande :

```bash
sudo apt install ansible
```

Ou équivalent pour les autres gestionnaires de paquets.

Si vous avez un nixos ou le gestionnaire de paquets nix, vous pouvez utiliser le
`shell.nix` présent dans le git du ansible du crans.

### Accès au LDAP

Pour faciliter le déploiement, il est possible (et nécessaire) d'utiliser le
ldap pour configurer les machines. Ceci nécessite l'installation du paquet
python correspondant

```bash
sudo apt install python3-ldap
```

Ou équivalent pour les autres gestionnaires de paquets. Si vous utilisez le
`shell.nix`, alors l'installation de `python3-ldap` est déjà effectuée.

Ensuite, il faut donner l'accès au ldap à votre machine avec la commande :

```bash
ssh -L 1636:wall-e.adm.crans.org:636 wall-e.adm.crans.org
```

### Accès au pass

Certains fichiers de configuration contiennent des secrets. Ceux-ci sont alors
dans le pass du crans. Il est donc nécessaire d'installer le pass du crans
comme indiqué sur le git.

### Configuration de ssh

Il faudra copier coller cette configuration ssh dans `~/.ssh/config` en
remplaçant `[pseudo]`:

```bash
Host *.adm.crans.org
    User _[pseudo]
    ProxyJump _[pseudo]@hodaur.crans.org
```

## Création d'un fichier de configuration

### Glossaire

- playbook : un playbook est un système de gestion et de déploiement de
  configurations sur des machines de manière simple, reproductible et
  réutilisable. Un playbook peut-être appliquer sur des machines ou des groupes
  de machines.

### Architecture

Pour bien comprendre comment configurer ansible, il faut d'abord comprendre
l'architecture du git
[Ansible du crans](https://gitlab.crans.org/nounous/ansible) :

- `hosts` : est un fichier qui contient les machines et qui les regroupent en
  groupe. Cela permet d'exécuter les configurations correspondantes au groupe
  de la machine directement.
- `all.yml` : est juste un fichier contenant tous les playbook déjà configurés.
  En particulier, il permet de ré-executé toutes les configurations.
- `group_vars` : est un dossier qui contient les variables utiles pour
  exécuter une configuration d'un groupe donné
- `host_vars` : est un dossier contenant les variables qui sont spécifiques à
  chaque machine. Par exemple, la configuration réseau.
- `plays` : est un dossier qui contient le pont entre tout les éléments déjà
  mentionnés. Il s'agit du fichier que l'on va exécuter. Il contient le (ou
  les) groupe qui va être concerné par son exécution. Il va aussi dire
  qu'elles seront les rôles à exécuter mais aussi définir de potentielles
  variables.
- `roles` : il s'agit du dossier qui contient l'ensemble des fichiers de
  configurations ainsi que les commandes à exécuter.

:bulb: Pour utiliser des variable de groupe, il faut que la machine soit dans
le groupe correspondant dans `hosts`

### Configuration basique

Pour commencer, il va falloir ajouter une configuration qui ne dépend pas (ou
presque) de la machine. Il s'agit du playbook `root`. Celui-ci contient toutes
les instructions pour initialiser les éléments essentiels d'une machine au
crans. Par exemple, les backups, le monitoring, le ssh ou encore les nounous.

Pour ajouter cette configuration, il suffit de suivre les étapes suivantes :

- ajouter la machine dans `hosts`, dans la catégorie `crans_vm` s'il s'agit
  d'une vm.
- ajouter la configuration réseau dans `host_vars` (dans un fichier du même nom
  que la machine).

Voici une configuration possible issue de la vm voyager :

```yaml
---
interfaces:
  adm: ens18
  srv_nat: ens19

loc_unattended:
  automatic_reboot: true

loc_needrestart:
  override: []
```

C'est bon la configuration est prête ! Il faut dès lors la déployer. Pour cela,
vous devez exécuter la commande :

```bash
plays/root.yml -l [machine à déployer].adm.crans.org --check
```

ATTENTION :

- Le `--check` sert à vérifier que la configuration fonctionne et donc
  n'effectue aucune modification. De manière général, il est préférable de
  vérifier le fonctionnement de la configuration avant de la déployer.
  Cependant, et comme on peut le constater dans le playbook root, des erreurs
  peuvent avoir lieu bien que la configuration fonctionne. Cela vient du fait
  que certains fichiers de configuration ont besoin d'un paquet qui doit être
  installer au préalable pour être écrit.
- Si la machine que vous cherchez à déployer est fraîchement installer, la
  procédure change légèrement car vous n'avez pas encore de compte sur la
  machine. Il est alors nécessaire de faire un tour sur la
  [documentation appropriée](/howto/creer_vm_crans.md).

:hint: Lorsqu'il sera demandé "BECOME PASSWORD", entrez votre mot de passe.

### Configuration du service

Le machine que vous déployez aura, a priori, une configuration qui lui sera
propre car lié à un service spécifique qu'elle hébergera. Pour cela, il est
nécessaire de créer votre propre configuration. Pour cela, il convient de
suivre les étapes suivantes :

- ajoutez la machine aux groupes appropriés
- Si besoin : créer un groupe dans `hosts` contenant le machine.
- (facultatif) Si vous avez créer un nouveau groupe : créer un fichier dans
  `group_vars`. contenant les variables qui seront à utiliser dans la
  configuration. ATTENTION : ce fichier devra avoir le même nom que le groupe
  précédemment défini.
- Si vous avez besoin de secrets : créer dans le pass un fichier dans
  `crans/ansible` contenant les secrets qui seront utilisés dans la
  configuration
- créer un fichier dans `plays` contenant a minima :
   - une entête disant comment exécuter le fichier
     (`#!/usr/bin/env ansible-playbook`) (suivi de `---`)
   - le groupe sur lequel il s'applique (`hosts:`)
   - les rôles à exécuter (`roles:`)
- écrire le rôles à exécuter

Ce dernier point mérite de s'y attarder.

Pour créer un rôle, il faut créer un dossier (le rôle) contenant un autre
dossier minimum : `tasks`. Ce dernier contiendra les fichiers qui exécuteront
les commandes. Pour écrire ces fichiers, vous pourrez vous aidez de la
[documentation d'Ansible](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/).
Si vous avez besoin d'effectuer une tâche qui n'est pas prise en charge par le
module par défaut d'Ansible, il est possible d'utiliser d'autres collections
présentent sur cette
[documentation](https://docs.ansible.com/ansible/latest/collections/). Il sera
alors nécessaire d'installer ces paquets selon la procédure décrite dans la
documentation.

Cependant, dans les rôles, vous serez amené⋅e à écrire des fichiers complets.
Ceci seront contenu dans le dossier `templates`. Celui-ci pourra contenir des
variables du groupe prédéfini (`{{ fichier.variable }}`) ou des secrets
(`{{ vault.fichier.variable }}`). Pour la clarté, ces fichiers auront le même
chemin dans `templates` que sur la machine une fois déployée. Il est aussi
possible de faire des boucles avec la syntaxe :

```jinja
{% for variable in liste %}

...

{% endfor %}
```

Pour plus d'information sur la syntaxe des templates, référez vous à la
[documentation de jinja](https://jinja.palletsprojects.com/en/3.1.x/templates/)

Enfin, lors de la configuration, il peut-être nécessaire d'exécuter des
opérations qui auront lieux UNIQUEMENT en cas de changement. Ceci seront dans
le dossier `handlers`. Par exemple, redémarrer nginx après modification des
fichiers de configuration. Pour plus d'information sur les handlers, vous
pouvez jeter un œil sur cette
[documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_handlers.html).

Voici un bref récapitulatif de la structure d'un dossier de roles :

```bash
roles/[service]/
├── handlers
│   └── main.yml
├── tasks
│   └── main.yml
└── templates
    └── [des templates]
```

Ces propos sont très abstraits et probablement peu limpides. Si vous êtes dans
ce cas, regardez un fichier de configuration quelconque en même temps pour vous
familiarisez avec la structure et le fonctionnement ainsi que la syntaxe.
Cependant, vous devriez avoir les clés pour comprendre les fichiers de
configuration !

### Configuration des backups

Bien qu'ansible configure par défaut les backups, quelques actions sont
nécéssaires. Pour ce qui est des backups sous borg, elles seront automatiquement
configurer et aucune action n'est requise. Pour les backups sous restics, il
peut être nécessaire de faire les actions qui suivent.

#### Configuration de base

##### Génération des secrets

Les backups sous restic nécessite la génération de secrets dans le pass pour
chaque machine (plus de détail dans la documentation de restic). Cela lui
confère une sécurité accrue (plus que la configuration de borg).

Si la machine que vous cherchez à configurer a déjà eu des backus sous restic,
alors il est fort probable que rien ne soit à faire à ce stade.

Si la machine est neuve (ou n'a jamais eu de backups restic), vous devez générer
2 secrets. Pour être sûr⋅e qu'il n'y ait pas de problèmes, on utilisera les
minuscules, majuscules et chiffres. En particulier, aucuns symboles ou caractères
spéciaux ne seront utilisés. Vous devrez générer un mot de passe avec 64
caractères (`rest_password`) et un de 256 caractères (`repo_password`). Il faut
alors les ajouter dans le pass dans le fichier `ansible/restic`. Ajouter alors
la machine selon le format :

```yaml
<nom machine>:
  rest_password: <mdp de 64 caractères>
  repo_password:
    base: <mdp de 256 caractères>
```

Ensuite, il faut dire à `thot` que vous souhaitez utiliser le `rest_password`
que vous venez de générer. Pour cela, vous devez vous ssh sur `thot` et éxécuter
la commande :

```bash
$ sudo su
# cd /backup
# htpasswd -B .htpasswd <nom machine>
```

Puis le secret vous sera demandé (puis re-demandé pour confirmer).

##### Configuration dans Ansible

Par défaut, sont backupé `/var` et `/etc`. Si cela ne convient pas, il est
possible de modifier cela en ajoutant (au besoin) dans
`host_vars/<nom machine>` :

```yaml
loc_restic:
  config:
    base:
      # pour exclure des chemins
      to_exclude:
        - <premier chemins>
        - <deuxième chemins>

      # pour ajouter des chemins
      to_backup:
        - /etc
        - /var
        - <un autre chemin>

      # changer la rétention des backups
      retention:
        - [--keep-daily, <int>]
        - [--keep-weekly, <int>]
        - etc
```

Enfin, vous pourrez appliquer la configuration ansible à l'aide de la commande :

```bash
plays/restic_client.yml -l <nom machine>.adm.crans.org --check
```

> Pensez à retirer le `--check` une fois que vous aurez vérifier que tout s'est
> bien passé !

#### Configuration de backup supplémentaire

Il est possible que la machine que vous déployez nécessite d'effectuer d'autres
backups, mais vous souhaitez les rendre indépendantes des backups de base comme
c'est le cas pour `tealc` et `cameron` (à l'heure où ces lignes ont été
rédigées).

##### Génération d'un nouveau secret

Comme mentionner dans la partie précédente, des secrets sont nécessaires pour le
fonctionnement de restic. Si vous êtes ici, c'est que vous avez déjà un
`rest_password` et un `repo_password`. Il faut alors générer un autre
`repo_password` (256 caractères avec minuscules, majuscules et chiffres) et le
placer dans le pass. Vous devriez avoir quelque chose qui ressemble à :

```yaml
<nom machine>:
  rest_password: <mdp de 64 caractères>
  repo_password:
    base: <mdp de 256 caractères>
    <un nom>:  <le mdp de 256 caractères que vous venez de générer>
```

##### Configuration dans Ansible

Vous devez alors annoncer à ansible ce qui doit être backupé. Ajoutez alors, si
la variable n'est pas là ou complétez sinon, dans
`host_vars/<nom machine>.adm.crans.org` :

```yaml
loc_restic:
  config:
    <un nom>: # le même nom que dans le pass
      # chemins à exclure
      to_exclude:
        - <premier chemins>
        - <deuxième chemins>

      # chemins à inclure
      to_backup:
        - <un chemin>
        - <un autre chemin>

      # changer la rétention des backups
      retention:
        - [--keep-daily, <int>]
        - [--keep-weekly, <int>]
        - etc
```

Enfin, vous pourrez appliquer la configuration ansible à l'aide de la commande :

```bash
plays/restic_client.yml -l <nom machine>.adm.crans.org --check
```

> Pensez à retirer le `--check` une fois que vous aurez vérifier que tout s'est
> bien passé !

### Configurer le reverseproxy

Si votre service nécessite de recevoir une connexion depuis l'extérieur, il
faut alors dire que l'on souhaite en recevoir. Ceci passe alors par Hodaur (le
reverseproxy). Pour cela il suffit de :

- ajouter dans `group_vars/reverseproxy.yml` le site dont il est question
  dans la catégorie `reverseproxy_sites`. Voici un exemple de ce qu'il faut
  ajouter `- {from: helloworld.crans.org, to: 172.16.10.131}`
- exécuter le playbook reverseproxy
  (`plays/reverseproxy -l hodaur.adm.crans.org --check`)
