# GPG

GPG (GNU Privacy Guard) est une implémentation libre et open source du
protocole OpenPGP (Pretty Good Privacy), un protocole cryptographique
permettant de signer, chiffrer et déchiffrer des documents (notamment des
emails).

## Clef

Une clef GPG est une clé numérique qui permet d'être identifié de façon
unique dans le monde numérique.

Elle est composée de deux éléments :

* une clé privée protégée par une phrase clé qui permet d'éviter à une
  tierce personne d'utiliser cette clef si elle parvient à la voler

* une clé publique qui une fois distribuée et signée par son entourage,
  permet à celui-ci de vérifier que les messages que l'on signe le sont bien
  avec la clé privée associée à cette clé publique.

Elle peut être générée à l'aide d'une des commandes suivantes (selon le
nombre d'option que vous souhaitez avoir) :

* `gpg --generate-key`

* `gpg --full-generate-key`

* `gpg --full-generate-key --expert`

## Web of trust

Le web of trust (ou toile de confiance en français) permet d'établir
l'authenticité de l'appartenance d'une clef publique ; il est décentralisé :
il n'a donc pas besoin d'autorité de certification.

Voici les règles par défaut du web of trust d'OpenPGP, on estime que la clef
publique appartient au bon propriétaire si :

* On a soi-même fait confiance à cette clef

* Une personne en qui on a totalement confiance a signé cette clef

* Trois personnes en qui on a partiellement confiance ont signé cette clef
