# SSH

SSH (Secure Shell) est un protocole permettant d'exécuter des commandes sur un
serveur distant. Il utilise le port 22 en TCP.

Sa version 2 est spécifiée par les RFC
[4250](https://datatracker.ietf.org/doc/html/4250),
[4251](https://datatracker.ietf.org/doc/html/4251),
[4252](https://datatracker.ietf.org/doc/html/4252),
[4253](https://datatracker.ietf.org/doc/html/4253) et
[4254](https://datatracker.ietf.org/doc/html/4254).

## OpenSSH

Le client de prédilection pour utiliser SSH est le client de la suite OpenSSH
(qui fourni également un serveur et d'autres utilitaires permettant de gérer
les clefs cryptographiques), disponible dans le paquet Debian `openssh-client`.

## Clefs SSH

Vous pouvez générer des clefs cryptographiques permettant de vous
authentifier directement sur le serveur distant (plutôt que par mot de passe)
avec l'utilitaire `ssh-keygen` ainsi :

```bash
ssh-keygen -t ${TYPE} -b ${BITS}
```

Où `TYPE` peut être :

* `rsa`

* `ecdsa` (dans ce cas `BITS` doit valoir 256, 384 ou 521)

* `ed25519` (dans ce cas `BITS` doit valoir 256)

`ssh-keygen` vous demande ensuite le chemin du fichier de clef ainsi que sa
passphrase et génère une clef privée ainsi qu'une clef publique se terminant
par `.pub`.

Un autre utilitaire, `ssh-copy-id` permet de copier la clef sur le serveur
distant par exemple :

```bash
ssh-copy-id -i ~/.ssh/id_ed25519.pub login@zamok.crans.org
```

Attention : il faut absolument garder la clef privée sur votre serveur et ne
pas l'envoyer sur le serveur distant.

## Exemples

À la première connexion `ssh` demande s'il faut faire confiance à la clef du
serveur :

```bash
benjamin@om ~ $ ssh gitlab.crans.org
The authenticity of host 'gitlab.crans.org (185.230.79.14)' can't be
established.
ECDSA key fingerprint is SHA256:StU+25vCTTs+opjyjMZTLvl+gvR+ViQWUkE1jRENnkQ.
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

Il est de bon usage de confirmer l'authenticité de cette clef afin de ne pas
être vulnérable aux attaque de type Man in the Middle. Pour celà il y a une
solution : l'administrateur du serveur doit vous confirmer l'empreinte
(fingerprint) de la clef par un moyen sécurisé. Voici une liste non
exhaustive de moyens plus ou moins sécurisés :

* Si vous êtes vous-même l'administrateur du serveur la commande `for key in
  /etc/ssh/ssh_host_*.pub; do ssh-keygen -lf ${key}; done` sur le serveur pour
  afficher les empreintes de vos clefs.

* Se faire confirmer la clef oralement par l'administrateur.

* Se faire confirmer la clef par un mail d'un administrateur de confiance
  signé GPG.

* Utiliser des clefs stockées dans le [DNS](/tools/dns.md) et signées
  DNSSEC, ceci peut se faire en ajoutant l'option `-oVerifyHostKeyDNS=yes` à
  `ssh`.

Une fois que vous vous êtes assuré de l'authenticité de la clef vous pouvez
entrer `yes` dans le prompt ci-dessus.

`ssh` vous demande ensuite votre mot de passe ou la passphrase de votre clef,
entrez le et appuyez sur Entrée pour vous connecter, félicitations vous
devriez avoir votre shell sur le serveur distant.

## Exemple de conf ssh

Après avoir `ssh-copy-id` sa clef public comme précisé plus haut on peut
définir une conf qui permet d'aller sur nimporte quel serveur depuis votre
machine via un `ProxyJump`.

```txt
Host *.adm.crans.org
    User _usernounou
    ProxyJump _usernounou@hodaur.crans.org
```
