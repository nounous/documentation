# ZFS

## ZFS c'est quoi ?

ZFS est un système de fichier avec plein de features natives, notamment de la
gestion de volume, du RAID (RAID-Z), des snapshots, etc.

> ZFS a été ouvert sous licence
> [CDDL](https://en.wikipedia.org/wiki/Common_Development_and_Distribution_License)
> incompatible avec la GPL. Par conséquent, ZFS n'est pas supporté nativement
> par le noyau Linux.

## Petit glossaire

* RAID-Z#N: Le raid de ZFS. Le chiffre #N correspond au nombre de disques que
  l'on peut perdre (e.g. RAID-Z2 correspond à du RAID 6 classique).

* Pool: Une *pool* de stockage est le bloc de base de ZFS. C'est un ensemble
  logique de données. Une pool ZFS est composée d'un ou plusieurs *vdev*.

* Vdev: Un *vdev* (pour ''virtual device'') est un sous élément d'une pool.
  Un vdev peut contenir un ou plusieurs disques physiques. Dans le cas où
  plusieurs disques sont utilisés, les données sont étendues (*stripped*) sur
  tous les disques afin d'améliorer sa durée de vie.

* Dataset: *Dataset* est le terme générique pour désigner un volume de
  fichiers ZFS. Chaque dataset a un nom unique de la forme *Pool/Dataset*. La
  racine de la pool est techniquement un dataset aussi. Les dataset sont
  organisés comme des dossiers classique, avec une arborescence, et les enfants
  héritent des propriétés des parents.

## Installation

Il faut ajouter les dépots `contrib` de debian dans lesquels se trouvent les
paquets installés ci-dessous.

Pour installer le module noyau ZFS sous Debian et les outils d'administration :

```bash
apt install linux-headers-amd64
apt install zfs-dkms
```

Il faut ensuite reboot pour loader le module zfs, puis :

```bash
apt install zfsutils-linux
```

Cela installe aussi `zfs-zed` un daemon permettant de monitorer les pool ZFS.
Les headers du noyau doivent être installés à chaque mise à jour du noyau.

## Créer une pool ZFS

La première étape de création d'un filesystem ZFS est la création d'une
pool. Une pool contient des vdev qui eux même contiennent des disques, la
commande suivante permet de créer une pool de nom «pool» et montée dans
`/pool` avec un vdev en RAIDZ-3 :

```bash
zpool create -f -m /pool pool raidz3 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03494499 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03563166 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03570970 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03575828 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03614378 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03626582 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03694920 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03704580 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03707506 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03731830 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03734763 \
    ata-WDC_WD2003FYYS-02W0B0_WD-WMAY03734769
```

Les identifiants de disques peuvent être récupérés dans `/dev/disks/by-id/`.

L'état des pool peut-être consulté avec la commande :

```bash
zpool status
```

## Créer un dataset

Une fois la pool créée il est préférable de créer des datasets qui
permettent une gestion plus fine de la pool. Pour cela on utilise la commande
suivante qui créé un dataset du nom de «home» dans la pool «pool» :

```bash
zfs create pool/home
```

## Partager un dataset en NFS

ZFS permet de partager un dataset en NFS pour cela il faut installer le paquet
`nfs-kernel-server` :

```bash
apt install nfs-kernel-server
```

Le dataset «home» de la pool «pool» peut ensuite être partager ainsi :

```bash
zfs set sharenfs="no_root_squash,rw=@172.16.10.0/24" pool/home
```

Cette commande partage le dataset au sous-réseau `172.16.10.0/24`, l'option
`no_root_squash` permet au root du client NFS d'avoir tous les droits sur les
fichiers du dataset.

Il est également préférable d'activer les acl POSIX sur la pool, dans le cas
contraire il peut y avoir des problèmes de permissions à la création de
fichiers :

```bash
zfs set acltype=posixacl pool
```

## Détruire un dataset

Pour détruire le dataset `pool/path` on utilise la commande :

```bash
sudo zfs destroy pool/path
```

## Désactiver le sync

Renvoyer une confirmation d'écriture sur le disque après chaque opération
diminue énormément les performances du NFS. On peut dire à ZFS de mentir au
NFS sur le retour des opérations sync(). Pour ça, on utilise la commande :

```bash
sudo zfs set sync=disabled pool/path
```

## Changer un disque

Récupérer le guid du disque qu'on souhaite changer avec `zdb` (`zpool status`
peut aider à récuperer le nom du disque concerné). Une fois que c'est fait
on peut retirer le disque avec : `zpool offline pool ${guid}` puis on remplace
l'ancien disque par le nouveau :
`zpool replace pool ${guid} ${nouveau disque}`.
Après ça, on peut monitorer la reconstruction avec `zpool status`.

## Chiffrement

ZFS permet de nativement chiffrer un dataset. Attention, il faut que ça soit
fait à la **creation** du dataset. Ça ne peut pas se changer après.

### Créer un dataset chiffré

Pour créer un dataset chiffré il suffit de rajouter les options de
chiffrement avec `encryption=on`. Il faut aussi spécifier le format de la clé
(`raw, hex, passphrase`). Par défaut, la clé va être promptée, mais il est
possible de la stocker sur le système de fichier, ou bien via une URL.
L'option `encryption` permet aussi de spéficier l'algorithme de chiffrement à
utiliser.

Par exemple:

```bash
sudo zfs create -o encryption=aes-256-gcm -o keyformat=passphrase pool/dataset
```

Pour vérifier que tout va bien on peut récupérer la propriété de
chiffrement :

```bash
sudo zfs get encryption pool/dataset
```

Pour changer la clé il suffit d'utiliser la commande suivante :

```bash
sudo zfs key -c pool/dataset
```

qui va demander la nouvelle passphrase.

Il est aussi possible de changer la source de la clé (pour aller chercher la
clé dans un fichier par exemple).

### Monter/démonter un dataset chiffré

Par défaut le dataset sera monté. Pour le démonter il suffit d'utiliser la
commande `unmount`. Attention, la clé de chiffrement sera toujours chargée,
et le dataset pourra être remonté sans redemander la clé. Pour ça, il faut
décharger la clé :

```bash
sudo zfs unmount pool/dataset && sudo zfs unload-key pool/dataset
```

Pour remonter le dataset il faut alors recharger la clé :

```bash
sudo zfs load-key pool/dataset && sudo zfs mount pool/dataset
```
