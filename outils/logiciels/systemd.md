# Systemd

Systemd est une suite logiciel pour systèmes GNU/Linux. Cette suite logiciel
fournit de nombreuses fonctionnalités, notamment pour configurer son OS et
suivre les *daemon*\ s actifs et leurs journaux.

Ce document décrit *certaines* fonctionnalités de Systemd bien choisies,
lesquelles sont jugées intéressantes pour l'administration système ou pour
utilisateurices.  Il sera sans doute découpé en plusieurs morceaux s'il devient
trop long.

## Unités de configuration: un format unifiant.

### Services.

### Minuteurs.

### (Auto)mount.

## Configuration Réseau.

### Obtenir des adresses IPs.

### Configuration DNS (récursif).

## Journalisation.

### Lecture des journaux: les bases.

### Espaces de noms.

### Partage de journaux entre machines.

## Aspect pratique (pour utilisateurices)

### Chiffrement de disque.

### Chargeur d'amorçage.

## Conteneurisation et virtualisation.

### Conteneurisation.

### Virtualisation.

## Gestion des secrets.

## Trucs et astuces.

### Systèmes dynamiques: gestion des erreurs dans les services.

### Administration à distance.

<!--  LocalWords:  IPs virtualisation utilisateurices Journalisation daemon
<!--  LocalWords:  Systemd Linux
 -->
 -->
