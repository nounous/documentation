# SMTP

SMTP (Simple Mail Transfer Protocol) est un protocol d'échange de courrier
électronique. Il utilise 3 ports différents :

* Le port 25 en TCP (smtp) pour l'échange de mail entre serveurs.

* Le port 465 en TCP (submissions) pour la soumission de mail chiffrée par TLS.

* Le port 587 en TCP (submission) pour la soumission de mail en clair avec
  possibilité de chiffrement via la commande STARTTLS.
