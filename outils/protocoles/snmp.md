# SNMP

Le protocol SNMP est un protocol minimaliste pour communiquer avec un
équipement mis en réseau. Il est implémenté sur de nombreux périphériques
tel que des bornes WiFi, des commutateurs réseaux et des multiprises
connectées.

## SNMPv3 vs SNMPv1/v2

Pour des raisons de sécurité, vous devriez toujours utiliser SNMPv3 qui a un
système d'authentification et de chiffrement. Si votre documentation parle de
"communautés SNMP", c'est que c'est de la v1 ou v2 et que vous devriez
mieux partir en courant.

## Monitoring d'un APC Rack PDU avec Prometheus

Lors du déménagement de Cachan vers Saclay, le Crans a récupéré un [APC
Rack PDU 2G Metered-by-Outlet with
Switching](https://www.apc.com/shop/th/en/products/Rack-PDU-2G-Metered-by-Outlet-with-Switching-ZeroU-20A-208V-16A-230V-21-C13-3-C19/P-AP8659)
venant de l'ancien équipement de la DSI à Cachan. Ce Power Distribution Unit
(PDU) est utilisé pour alimenter notre baie de serveur principale. Cette unité
de distribution de courant est capable de monitorer chaque prise avec plusieurs
métriques utiles. Par exemple, <root@crans.org> reçoit un courrier quand une
prise surcharge la distribution.

Dans les sous-sections on va présenter comment mettre en place le monitoring
en SNMPv3 de ce PDU vers la base de monitoring Prometheus.

### Configuration du SNMPv3 et des droits d'accès

*On a réalisé ces étapes sur le firmware AOS v6.1.3. Si vous vous sentez
aventureux et mettez à jour le PDU, les menus et identifiants risquent de
changer.*

Connectez-vous à l'interface Web du PDU. Par défault il écoute en HTTP sur
son port 80 et on peut s'identifier avec l'identifiant `apc` et le mot de passe
`apc`. *Néanmoins, pour sécuriser l'infrastructure, le mot de passe actuel
est dans la bibliothèque de mots de passe du Crans.*

Après connexion, allez dans `Configuration > Network > SNMPv3 > Access` et
cochez `Enable SNMPv3 access` puis appliquez.

Le PDU va proposer de se redémarrer pour appliquer les changements. Déjà,
redémarrer le PDU ne coupe pas les prises (heureusement!), mais on a d'autres
changements à réaliser avant de le redémarrer.

On change les identifiants pour collecter les métriques en SNMPv3. Allez dans
`Configuration > Network > SNMPv3 > User Profiles` et configurez un utilisateur
Vous devriez utiliser `SHA` pour `Authentication Protocol` et `AES` pour
`Privacy Protocol` (car `MD5` et `DES` sont vraiment faibles).

*Au Crans, le compte s'appelle `crans`.*

Maintenant que le compte est configuré, il faut lui autoriser l'accès. Allez
dans `Configuration > Network > SNMPv3 > Access Control` et choisissez
l'utilisateur, puis activez l'accès et mettez l'adresse IPv4 de la machine qui
contactera le  PDU en SNMP.

### Test de l'accès SNMP

La commande suivante devrait maintenant marcher (changer `PDU_*` avec les
bonnes valeurs). Sur les dérivées de Debian, vous devriez installer `snmp` et
`snmp-mibs-downloader` (optionnel, télécharge une base de données non libre
de correspondance des valeurs SNMP, appelées `MIBs`).

Pour avoir les entrées associées au PDU, il faut télécharger le MIBs d'APC :

```bash
mkdir -p $HOME/.snmp/mibs
echo "mibdirs +$HOME/.snmp/mibs" >> $HOME/.snmp/snmp.conf
curl "https://download.schneider-electric.com/files?p_enDocType=Firmware&p_File_Name=powernet432.mib&p_Doc_Ref=APC_POWERNETMIB_432" -o $HOME/.snmp/mibs/powernet432.mib
```

Une fois cela effectué, la commande suivante devrait afficher les valeurs
intéressantes :

```bash
# -m ALL: try to load all SNMP MIBs to output metrics name
snmpwalk -v3 -a SHA -A PDU_AUTH_PASSPHRASE -x AES -X PDU_PRIVACY_PASSPHRASE \
    -l authPriv -u PDU_SNMP_USERNAME -m ALL PDU_HOSTNAME_OR_IP
```

Si cette étape marche, vous pouvez aller plus loin et configurer Prometheus.
Sinon, c'est le bon moment pour aller pinger <erdnaxe@crans.org> ou une autre
personne habitué au SNMPv3.

### À la recherche des métriques utiles

*Tel un valeureux chevalier, vous partez à l'aventure pour retrouver les
métriques utiles. Vous tombez devant un donjon qui vient de vous spammer plus
de 2000 métriques, que faites-vous ?*

`snmpwalk` devrez vous spammer de métriques toutes plus ou moins utiles. Si
vous avez la base de données de MIBs installée, la sortie devrait être
lisible et vous devriez rapidement trouver votre bonheur. Sinon vous êtes en
quête soit pour deviner les identifiants des métriques intéressantes, soit
pour aller chercher un fichier de définition MIB sur le site du constructeur.

Pour le PDU, après quelques observations, on a gardé :

* `rPDU2DeviceStatusLoadState`

* `rPDU2DeviceStatusPower`

* `rPDU2DeviceStatusPeakPower`

* `rPDU2PhaseStatusPowerFactor`

* `rPDU2OutletMeteredStatusPower`

### Configuration de l'exporteur Prometheus SNMP

*C'est bien beau le SNMPv3, mais je ne vais pas snmpwalk dans un cron tout de
même !*

Prometheus SNMP exporter est livré avec un générateur de configuration.
Cette étape de génération permet de créer la configuration de l'exporteur
et lui permet de fonctionner sans nécessiter la base de données des MIBs du
périphérique. Elle permet aussi de peaufiner la phase de traduction SNMP vers
les gauges ou compteurs Prometheus.

Le generateur écrit la configuration de l'exporteur `snmp.yml` en utilisant
les MIBs et un fichier `generator.yml` tel que :

```YAML
modules:
  apc_pdu:
    walk:
      - rPDU2DeviceStatusLoadState
      - rPDU2DeviceStatusPower
      - rPDU2DeviceStatusPeakPower
      - rPDU2PhaseStatusPowerFactor
      - rPDU2OutletMeteredStatusPower
    version: 3
    auth:
      security_level: authPriv
      username: PDU_SNMP_USERNAME
      password: PDU_AUTH_PASSPHRASE
      auth_protocol: SHA
      priv_protocol: AES
      priv_password: PDU_PRIVACY_PASSPHRASE
```

Sur les dérivées de Debian, vous aurez à installer
`prometheus-snmp-exporter` puis lancer `prometheus-snmp-generator generate`
dans `/etc/prometheus/` qui contient `generator.yml`.

### Collecter l'exporteur avec Prometheus

Maintenant que l'exporteur est configuré, vous pouvez le démarrer. Il
écoutera sur le port `9116` par défaut.

Dans la configuraiton de Prometheus, vous pouvez ajouter une section pour
interroger cet exporteur en utilisant la définition `apc_pdu` définie  dans
la dernière section:

```YAML
scrape_configs:
- job_name: apc_pdu_snmp
  static_configs:
  - targets:
    - PDU_HOSTNAME_OR_IP
  metrics_path: /snmp
  params:
    module:
    - apc_pdu
  relabel_configs:
  - source_labels:
    - __address__
    target_label: __param_target
  - source_labels:
    - __param_target
    target_label: instance
  - replacement: 127.0.0.1:9116
    target_label: __address__
```

La bidouille `relabel_configs` permet à Prometheus de query l'exporter avec le
nom d'hôte ou adresse IP du périphérique afin de collecter les métriques
SNMP tout en gardant le label `instance` propre dans sa base de données.

Vous devriez pouvoir redémarrer Prometheus et le PDU devrait être monitoré.

*Voilà, vous brandissez Excalibur de fierté!*
