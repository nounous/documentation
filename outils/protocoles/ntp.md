# NTP

NTP (Network Time Protocol) est un protocole de synchronisation d'horloge, il
utilise le port 123 en UDP.

Sa version 4 est spécifiée par la [RFC
5905](https://tools.ietf.org/html/rfc5905).
