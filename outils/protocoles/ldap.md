# LDAP

LDAP (Lightweight Directory Access Protocol) est un protocole d'accès à une
base donnée arborescente (directory). Sa dernière version (version 3) est
spécifiée dans la [RFC 4511](https://tools.ietf.org/html/rfc4511).
