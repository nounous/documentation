# IRC

IRC (Internet Relay Chat) est un protocole de communication textuelle. C'est le
protocole privilégié pour les discussions instantanées entre membres actifs.

Sa version 2 est spécifiée par la [RFC
2812](https://tools.ietf.org/html/rfc2812), une [version 3](https://ircv3.net/)
est en cours de développement.

Il utilise 2 ports :

* Le port 6667 en TCP pour la communication en clair.

* Le port 6697 en TCP pour la communication chiffrée avec TLS.

Il existe deux types d'entités dans le protocole IRC : les utilisateurs et les
canaux. Un nom d'utilisateur (nick) commence obligatoirement par une lettre ou
un des caractères spéciaux définis dans la RFC. Un nom de canal commence
obligatoirement par un caractère `#`, `&`, `!` ou `+`, en pratique seul le `#`
est implémenté de manière universelle.

Le protocole IRC utilise des commandes, dans la plupart des clients il est
possible d'envoyer une commande en entrant un `/` suivi de la commande dans le
prompt du client.

## Modes

Il est possible d'attribuer des «flags» aux utilisateurs et aux canaux qui
permettent de modifier leur comportement, ceux-ci s'appellent des modes,
certains modes sont spécifiés dans la RFC mais la quasi-totalité des
serveurs IRC implémentent d'autres modes pour ajouter des fonctionnalités au
protocole.

Ces modes sont manipulables avec la commande `MODE`, par exemple `MODE nick +w`
pour ajouter le mode `w` à l'utilisateur `nick` ou `MODE #channel -t` pour
retirer le mode `t` du canal `#channel`.
