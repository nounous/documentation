# BGP

BGP (Border Gateway Protocol) est un protocole d'échange de routes IP entre AS
sur internet. On le qualifie de protocole de routage à vecteurs de chemins.

BGP utilise le port 179 en TCP.

Sa version 4 est spécifiée par la [RFC
4271](https://datatracker.ietf.org/doc/html/rfc4271).

## Autonomous System

Un système autonome (AS) est un ensemble de préfixes IP sous le contrôle
d'une seule entité ayant une politique de routage cohérente sur l'internet.

Chaque AS est identifié par son numéro (ASN) qui est utilisé dans le
protocole BGP. À l'origine un ASN était un entier représenté sur 16 bits
mais la [RFC 6793](https://datatracker.ietf.org/doc/html/rfc6793) a étendu
cette représentation à 32 bits.

Certains ASN sont réservés à des usages particuliers :

| ASN | Usage |
|-----|-------|
| `64496`-`64511` | Documentation |
| `64512`-`65534` | Utilisation privée |
| `65536`-`65551` | Documentation |
| `4200000000`-`4294967294` | Utilisation privée |
