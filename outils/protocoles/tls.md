# TLS

TLS (Transport Layer Security) est un protocole cryptographique permettant une
communication sécurisée sur un réseau.

## TLS 1.3

La dernière version de TLS est TLS 1.3 spécifiée dans la [RFC
8446](https://tools.ietf.org/html/rfc8446).

## OpenSSL

[OpenSSL](https://www.openssl.org/) est une bibliothèque implémentant le
protocole TLS elle vient avec un binaire `openssl` permettant notamment de
gérer les certificats TLS et d'effectuer des connexions chiffrées.

### s_client

Le binaire `openssl` lancé avec la sous-commande `s_client` permet d'effectuer
une connexion TLS à la manière de netcat. Voici un exemple :

```bash
openssl s_client -connect irc.crans.org:6697
```

Ceci permet de se connecter sur le port 6697 du serveur identifié par le nom
de domaine `irc.crans.org`.

Il est également possible d'utiliser des protocoles utilisant STARTTLS (c'est
à dire passant d'une connexion en clair à une connexion chiffrée a l'aide
d'une commande) :

```bash
openssl s_client -starttls smtp -connect redisdead.crans.org:25
```

### genpkey

La sous-commande `genpkey` permet de générer une clef privée, par exemple
(pour générer une clef privée ED25519) :

```bash
openssl genpkey -algorithm ED25519 -out key.pem
```

Voici les valeurs notables possible de l'option `-algorithm` et les valeurs
associées de l'option `-pkeyopt` permettant d'ajuster les paramètres de la
clef privée:

* `ED25519` (pas d'options)

* `ED448` (pas d'options)

* `RSA`

   * `rsa_keygen_bits`: le nombre de bits de la clef privée

   * `EC`

   * `ec_paramgen_curve`: le nom de la courbe de la clef privée

### x509

La sous-commande `x509` permet d'afficher des informations sur un certificat et
d'effectuer des opérations de signature.

On peut par exemple l'utiliser pour afficher la clef publique d'un certificat :

```bash
openssl x509 -pubkey -in cert.pem -noout
```

Ou encore signer un certificat avec une autorité :

```bash
openssl x509 -req -in cert.csr -CA ca_cert.pem -CAkey ca_key.pem -CAcreateserial -out cert.pem -days 3650
```

Le certificat `cert.pem` est délivrée par l'autorité `ca_cert.pem` pour 10
ans (3650 jours).
