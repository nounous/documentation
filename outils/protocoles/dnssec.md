# DNSSEC

DNSSEC (Domain Name System Security Extensions) est un ensemble d'extension de
sécurité pour DNS. Il permet d'authentifier les données obtenues par le
protocole DNS.

Pour cela, chaque entrée du DNS est signée cryptographiquement avec une clef
privée. La clef publique, elle-même publiée dans le DNS permet de vérifier
les signatures. L'authenticité de la clef publique est assurée par la zone
parente (via les enregistrements DS), dans laquelle elle est publiée.

Ainsi, il suffit de faire confiance à la clef publique de la racine du DNS (et
seulement de la racine) pour pouvoir établir une chaîne de confiance dans
tous le DNS.

Les enregistrements DNS associés au DNSSEC sont les suivants :

* `DNSKEY`: une clef publique servant à signer une zone

* `DS`: un hash de clef publique d'une zone fille (conceptuellement proche
  d'un GLUE record NS)

* `RRSIG`: une signature d'un enregistrement

* `NSEC`: un pointeur vers la prochaine entrée DNS signée (dans l'ordre
  alphabétique), celui sert à prouver l'absence d'un enregistrement

* `NSEC3`: une variante de `NSEC` où les entrées DNS sont hashées

* `NSEC3PARAM`: des informations servant à calculer les hash pour les
  enregistrements `NSEC3`

## Exemples

### DNSKEY

On lance la commande `dig -t DNSKEY @silice.crans.org crans.org` :

```txt
;; QUESTION SECTION:
;crans.org.     IN  DNSKEY

;; ANSWER SECTION:
crans.org.    3600  IN  DNSKEY  257 3 14 Db282t/1HUd5ccAnJ0BSaidrnseZEtG/8Cj9MbzYl/GbgGxA8msR/Dq8 KfIeobLwnZFF5277dhdzAAdfKAb/XclhHBqpDtaV+bvd21n8MLR3yhdI VsFAde/yOBFW28fV
crans.org.    3600  IN  DNSKEY  256 3 14 4gBTKamW1QSNWNSWkR0qVswo+GcIV7cmKf6y4zuqlinDcvN0a5+ppqW2 almA4TF7vh7u9jH2/iwZVmHiikgpTiRzZJzga158/AlYTqE4pTK6zhin YlPz9w/PTSdOXMU6
```

La zone `crans.org` possède deux clefs de signature : une KSK (Key Signing
Key) et une ZSK (Zone Signing Key).

### DS

On lance la commande `dig -t DS @a0.org.afilias-nst.info crans.org`:

```txt
;; QUESTION SECTION:
;crans.org.     IN  DS

;; ANSWER SECTION:
crans.org.    3600  IN  DS  40871 14 2 8E792BB11D64D2F109F4B80DE2E353266671E32BD897E39455701D77 77018EA2
```

La zone `crans.org` a publié dans la zone `org` le hash d'une clef publique
(la KSK).

### RRSIG

On lance la commande `dig -t RRSIG @silice.crans.org zamok.crans.org` :

```txt
;; QUESTION SECTION:
;zamok.crans.org.   IN  RRSIG

;; ANSWER SECTION:
zamok.crans.org.  3600  IN  RRSIG A 14 3 3600 20220711091113 20220611082423 40871 crans.org. U5Ggkg82aIqPdO8HrJ9k793DQv0mykIKoh9PMRdNpwJSjrnOFmACdD4q YuPceZBpllJJHfhX5bsie8g/eYMPS39RZvoFUiMcw3SXdpJiE+/bOf89 5HZe3HkbveX/twx5
zamok.crans.org.  3600  IN  RRSIG AAAA 14 3 3600 20220711100552 20220611093221 40871 crans.org. 2XkmJYJbeKgXK0plIB/d1nnhQ6mXhubZu4k6jASlp9BumPy6WLRPUxR1 QhFV+gidKTVidsWL6faL28HH8j4pmxVGeIC50c5NOfaBa3nVTfeD+pUp MEhC0VwhBYnODuu9
zamok.crans.org.  3600  IN  RRSIG SSHFP 14 3 3600 20220711100552 20220611093221 40871 crans.org. nd/HiLy0qw3NMo2Lwn8P/J2h1UaXjGWAFYPCK5ISQWBILPE5iO+TdaXy uyOy4l4Dc9hdanzvKQFBuE9hG9gkr3KpmtCEFWtHlg6PAu7MIqXTzpbV 4kR2wjiuCLfmPq2W
```

`zamok.crans.org` possède trois enregistrements signés : ils sont de types
`A`, `AAAA` et `SSHFP`.
