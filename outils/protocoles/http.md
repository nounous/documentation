# HTTP

HTTP (Hypertext Transfer Protocol) est un protocole permettant de servir un
site web. Il utilise le port 80.

Sa version sécurisé (HTTPS) consiste en une intégration de
[TLS](tools/tls.md) au protocole et utilise le port 443.
