# NFS

NFS (Network File System) est un système de fichiers pouvant être monté à
travers le réseau : le serveur NFS expose des parties de son arborescence qui
peuvent être montées par les clients.

NFS utilise le port 2049 en TCP et en UDP.

Le système de fichier ZFS supporte nativement l'export NSF de tout ou partie
de son arborescence, vous pouvez vous référer à la section *Partager un
dataset en NFS* de la page ZFS pour savoir comment faire.

## Version 4

La dernière version de NFS est la version 4.2 spécifiée par la [RFC
7862](https://tools.ietf.org/html/rfc7862).
