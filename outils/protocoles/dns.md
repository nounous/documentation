# DNS

DNS (Domain Name System) est un protocole d'annuaire. Il utilises deux ports :

* Le port 53 en UDP pour la plupart des échanges.

* Le port 53 en TCP pour les échanges de grande taille (tels que du transfert
  de zone).

DNS sert principalement à traduire les noms de domaines en adresse IP.

C'est est un service hierarchisé dont le sommet est appelé la racine, et est
représentée par un point (`.`). Chaque nom de domaine est découpé en
plusieurs sous parties :

Par exemple `zamok.crans.org` est un sous-domaine de `crans.org` lui-même
sous-domaine de `org` qui est un sous-domaine de `.` (la racine).

Il existe plusieurs types de serveurs DNS :

* Les serveurs Autoritaires, qui s'occupent de garder à jour le gros
  dictionnaire (ou annuaire) de la zone qu'ils gouvernent (par exemple
  `crans.org`, `crans.eu` ou `crans.fr`)

* Les serveurs Récursifs, qui s'occupent de faire des requêtes successives
  pour obtenir la valeur souhaitée, ils gardent très souvent en cache les
  informations récoltées, ce qui permet d'accélérer la réponse et d'éviter
  de surcharger les serveurs autoritaires.

DNS permet d'associer des enregistrements (records) à des noms, ces
enregistrements ont une classe et un type dont la liste est disponible [sur le
site de
l'IANA](https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml).

La classe utilisée traditionnellement est la classe `IN` (pour internet),
voici quelques types courants :

| Type | Description |
|------|-------------|
| `A` | Adresse IPv4 |
| `AAAA` | Adresse IPv6 |
| `CNAME` | Alias vers un autre nom de domaine |
| `NS` | Nom d'un serveur DNS autoritaire |
| `MX` | Nom d'un serveur mail |

Ainsi on a par exemple les enregistrements suivants dans la zone `crans.org` :

```txt
zamok.crans.org. IN A 185.230.79.1
perso.crans.org. IN CNAME hodaur.crans.org.
crans.org. IN NS silice.crans.org.
crans.org. IN MX 10 redisdead.crans.org.
```

Le 10 dans la valeur du champ MX est la priorité du serveur mail.

## host

Pour résoudre des noms de domaines la commande privilégiée est la commande
`host` :

```bash
benjamin@tiamat ~ $ host zamok.crans.org
zamok.crans.org has address 185.230.79.1
zamok.crans.org has IPv6 address 2a0c:700:2:0:ec4:7aff:fe59:a1ad
```

```bash
benjamin@tiamat ~ $ host crans.org
crans.org has address 185.230.79.10
crans.org has IPv6 address 2a0c:700:2::ff:fe01:4502
crans.org mail is handled by 10 redisdead.crans.org.
crans.org mail is handled by 15 sputnik.crans.org.
crans.org mail is handled by 25 freebox.crans.org.
```

## dig

La commande `dig` permet d'effectuer des requêtes DNS plus complexes que
`host`, la commande `dig -t TYPE @server name` est équivalente à la commande
`host -vt TYPE name server`. `dig` peut prendre des options de requête plus
qui modifient son comportement, en voici quelques exemples :

* `+short`: affiche uniquement la section de réponses de la requête et de
  manière compacte

* `+dnssec`: affiche également les résultats des requêtes pour les champs
  DNSSEC associés à la requête demandée

* `+rrcomments`: affiche des commentaires sur certains enregistrements

* `+https`: effectue une query DNS over HTTPS (DoH)

* `+trace`: fait les requêtes successives récursivement côté client afin
  de déterminer le résultat par exemple :
  `dig +trace @ns0.fdn.fr zamok.crans.org`:

   ```txt
   .      47747 IN  NS  a.root-servers.net.
   .      47747 IN  NS  b.root-servers.net.
   .      47747 IN  NS  c.root-servers.net.
   .      47747 IN  NS  d.root-servers.net.
   .      47747 IN  NS  e.root-servers.net.
   .      47747 IN  NS  f.root-servers.net.
   .      47747 IN  NS  g.root-servers.net.
   .      47747 IN  NS  h.root-servers.net.
   .      47747 IN  NS  i.root-servers.net.
   .      47747 IN  NS  j.root-servers.net.
   .      47747 IN  NS  k.root-servers.net.
   .      47747 IN  NS  l.root-servers.net.
   .      47747 IN  NS  m.root-servers.net.
   .      47747 IN  RRSIG NS 8 0 518400 20220629170000 20220616160000 47671 . WGw6h2UFnKlJdCnApXM7dYkGuSpmRwokdwl0PzQMt0yUUG/Dumwpijn5 MEeOCxhoP29ni62JRMCGxgIirXVOHGfiMHDcx/dnv2gMnsr1OhQjPgwj hOu/NjZVkb612QW3b/i0mwDdjcEwRFQs9wzVZIBc4mPTFjocT2C2GlQc noLupUmliv38lfaPd+knrbhIUdwD8VEHDrzpDfdwR/kws+fWDjBcgUjA 77WzsA+FiwkqNvFIhQfMG+5CSXd0PpD3pPhdA6lCKa3l1AM4Cfnbtvj6 tuejEnNvbxsBaPOkiRz642/ysLV+fhXrP4U9BnWncUwe92NtFlZYRCHe a/D+yw==
   ;; Received 525 bytes from 2001:910:800::12#53(ns0.fdn.fr) in 23 ms

   org.      172800 IN  NS  a0.org.afilias-nst.info.
   org.      172800 IN  NS  a2.org.afilias-nst.info.
   org.      172800 IN  NS  b0.org.afilias-nst.org.
   org.      172800 IN  NS  b2.org.afilias-nst.org.
   org.      172800 IN  NS  c0.org.afilias-nst.info.
   org.      172800 IN  NS  d0.org.afilias-nst.org.
   org.      86400  IN  DS  26974 8 2 4FEDE294C53F438A158C41D39489CD78A86BEB0D8A0AEAFF14745C0D 16E1DE32
   org.      86400  IN  RRSIG DS 8 1 86400 20220630050000 20220617040000 47671 . bKm4Ul/DPpqbqvF0gRS1Gzrmk9N0VU2Qa1dBIb/vbx6N+SEVK1wGpD20 BA6mdyFL2SOOuhEldC44N/cj401aLoli/9Bj71hdYl0XLcW/Hiq1xo2U 6Iqvfu4GwBGb36aLfce8DYEdnTtpbicXrFszx8dg2CAnFvCYLve6sA/r DOzs+K8LCsTi3cdOTuee3mEN1XC6SAfIrZDDtLEwZXiyxRAI7GbuM0i8 LCFHRa7YBlf8EOFNafkFsCi4qM4cHdnavVAxDH+gOeYnjVOd3yhezJb9 /hRBo3aIKGY1orjb+erQAOxhly26f8v2H/h0czKOXriNL7d4id8aa1fN qWiFeA==
   ;; Received 781 bytes from 199.7.83.42#53(l.root-servers.net) in 27 ms

   crans.org.   3600  IN  NS  sputnik.crans.org.
   crans.org.   3600  IN  NS  silice.crans.org.
   crans.org.   3600  IN  DS  40871 14 2 8E792BB11D64D2F109F4B80DE2E353266671E32BD897E39455701D77 77018EA2
   crans.org.   3600  IN  RRSIG DS 8 2 3600 20220630152826 20220609142826 41346 org. EjZHR/kgXopDL/Rqw5wMWQn2AB2stt6eu0TtGHa2Gt1/LhOPraqNbXJq Ufg7VhcCRtY4DaZnp9wnBXzK9vdqUIIRMuNQoA6mEdj8qpjRajsjfPdT D87bU6h1OH5YghQAzi2N90LGgjlDqdB50xWIZHgMeLLH53nePcAcxpxE VA4=
   ;; Received 358 bytes from 2001:500:e::1#53(a0.org.afilias-nst.info) in 243 ms

   zamok.crans.org. 3600  IN  A 185.230.79.1
   zamok.crans.org. 3600  IN  RRSIG A 14 3 3600 20220711091113 20220611082423 40871 crans.org. U5Ggkg82aIqPdO8HrJ9k793DQv0mykIKoh9PMRdNpwJSjrnOFmACdD4q YuPceZBpllJJHfhX5bsie8g/eYMPS39RZvoFUiMcw3SXdpJiE+/bOf89 5HZe3HkbveX/twx5
  crans.org.    3600  IN  NS  silice.crans.org.
   crans.org.   3600  IN  NS  sputnik.crans.org.
   crans.org.   3600  IN  RRSIG NS 14 2 3600 20220711101913 20220611094455 40871 crans.org. +BvWwsxxekrKHDFCXTL2d/pHP/2i75U6qah3Ee15SSDSQhjbnwCkI2+a Q6zGpnkwnSsB6HfRgoCbXp8r/rRzgAV7pGN06wPyejJRzkgRB+euSjbD tUmc8ODVxVWYH7iQ
   ;; Received 1041 bytes from 2a0c:700:2::ff:fe01:4702#53(silice.crans.org) in 23 ms
   ```
