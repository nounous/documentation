# IP

## Généralités

IP (Internet Protocol) est le protocole principal de communication sur
l'internet, actuellement deux versions sont en cours d'utilisation : IPv4 et
IPv6.

IP permet de transmettre des datagrammes d'un point à un autre.

### Adressage

Les adresses IP sont alloués avec la méthode du CIDR (Classless Inter-Domain
Routing). Cette méthode consiste à noter une adresse au format
`adresse/longueur_préfixe`, elle induit une attribution des IP par blocs.

Les `n` bits (où `n` est la longueur du préfixe) les plus significatifs
servent à identifier le réseau auquel l'adresse appartient.

## IPv4

Une adresse IPv4 s'écrit sur 32 bits et utilise une notation décimale
pointée (4 entiers séparés par des `.`), par exemple `192.168.0.1` est une
adresse IPv4.

De même `192.168.0.0/24` est un exemple de notation CIDR pour un sous-réseau
IPv4.

Dans un réseau local les adresses IPv4 peuvent être attribuées par le
protocole DHCP ou configurées statiquement.

À noter que dans un sous-réseau IPv4 la première et la dernière sont
réservées respectivement pour identifier le réseau et effectuer du broadcast
par exemple pour le bloc `192.168.0.0/24`, l'adresse `192.168.0.0` est
réservée et l'adresse `192.168.0.255` est l'adresse de broadcast.

Cette restriction peut être contournée sous Linux, par exemple sous
[Debian](/tools/debian.md) on peut modifier l'interface comme ceci :

```txt
iface eth0 inet static
    broadcast -
```

Ceci permet d'utiliser des blocs d'adresses IPv4 d'une taille de préfixe
égale à 31 afin d'interconnecter deux machines.

### IPv4 privées

Certains blocs d'adresses IPv4 sont réservés à des utilisations privées,
celà signifie qu'ils ne sont pas annoncés sur l'internet. Ils sont
spécifiés par la [RFC 1918](https://tools.ietf.org/html/rfc1918) et sont les
suivants :

* `10.0.0.0/8`

* `172.16.0.0/12`

* `192.168.0.0/16`

### IPv4 de lien local

Un bloc d'IPv4 est réservé pour utilisation sur le réseau local, c'est à
dire que ce bloc n'est normalement jamais routé, il s'agit du bloc
`169.254.0.0/16`.

### IPv4 multicast

un bloc d'IPv4 est réservé pour du multicast, il s'agit du bloc `224.0.0.0/4`.

## IPv6

Une adresse IPv6 s'écrit sur 128 bits et utilise une notation hexadécimale
séparée par des `:` (deux-points), par exemple
`2001:0db8:0000:0000:0000:0000:0000:0000` (on regroupe les octets par groupe de
2).

Il existe également une notation compacte pour les adresses IPv6 : on peut
omettre les 0 en début de bloc et remplacer la plus longue suite de blocs nuls
par `::`, par exemple l'adresse précédente peut s'écrire `2001:db8::`.

Ainsi `2001:db8::/32` est un exemple de notation CIDR pour un sous-réseau IPv6.

Dans un réseau local les adresses IPv6 peuvent être attribuées par le
protocole NDP, par le protocole DHCPv6 ou configurées statiquement.

La liste des IPv6 attribuées est disponible
[ici](https://www.iana.org/assignments/ipv6-address-space/ipv6-address-space.xhtml)
et
[là](https://www.iana.org/assignments/iana-ipv6-special-registry/iana-ipv6-special-registry.xhtml).

Il n'y a pas de broadcast en IPv6 et la première adresse du bloc peut être
utilisée sans risque. Le broadcast est remplacé par du multicast : tous les
nœuds sur le réseau local répondent sur l'adresse `ff02::1`.

### IPv6 uniques locales

Un bloc d'adresses IPv6 est réservé pour des utilisations privées, c'est
l'équivalent des blocs d'IPv4 privées. Il s'agit du bloc `fc00::/7`.

### IPv6 de lien local

Un bloc d'IPv6 est réservé pour utilisation sur le réseau local, il s'agit
du bloc `fe80::/10`.

### IPv6 multicast

Un bloc d'IPv6 est réservé pour du multicast, il s'agit du bloc `ff00::/8`.

## ip

Sous Linux la commande privilégiée pour consulter l'état de la configuration
réseau est `ip` (fournie par la suite `iproute2`) voici quelques exemples de
commandes :

Afficher l'état des interfaces de la machine (adresses IP attribuées et leur
sous-réseau) :

```bash
ip address
```

Afficher l'état des routes IPv4 de la machine :

```bash
ip route
```

Afficher l'état des routes IPv6 de la machine :

```bash
ip -6 route
```
