# Comptes-rendus

## Présentation

Ce dossier contient l'ensemble des comptes-rendus des IN.

Les IN, ou Inter-Nounous, sont les réunions techniques des membres
actifs⋅ves du Crans. Celle-ci sont publiques et sont organisées tous les
mois. Celles-ci sont annoncées sur IRC et sur les ML
apprenti-es@lists.crans.org et nounou@crans.org.

## Organisation

Chaque fichier de ce dossier est le compte-rendu d'une réunion technique. Vous
pouvez trouver un template de compte-rendu dans le fichier
[`TEMPLATE.md`](TEMPLATE.md) si nécessaire. La seule contrainte pour ce
dossier est le nom des fichiers. Les comptes-rendus sont identifiés par la
date de la réunion : pour une réunion du JJ/MM/AAAA, le fichier devra se
nommer `AAAA_MM_JJ.md` (et non `JJ_MM_AAAA.md` car le tri par date se fait plus
simplement).
