# Réunion IN

* Date :
* Lieu(x) :
* Heure de début :
* Heure de fin :

## Présent⋅es

* [Nounou 1](https://wiki.crans.org/CransNounous)
* [Apprentie 1](https://wiki.crans.org/CransTechnique/CransApprentis)
* [Apprenti 2](https://wiki.crans.org/CransTechnique/CransApprentis)
* [Nounou 2](https://wiki.crans.org/CransNounous)
* Personne sans page wiki

## Ordre du jour

* [Nounou 1] Point 1
* [Nounou 2] Point 2
   * Sous-point 1
   * Sous-point 2
* [Apprentie 1] Point 3
* [Nounou 1] Point 4
* [Apprenti 2] Point 5

### Point 1

Détails et résumé des discussions du point 1

### Point 2

Détails et résumé des discussions du point 2

...
